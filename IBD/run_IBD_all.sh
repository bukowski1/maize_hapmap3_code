#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

# Run the IBD evaluation of the genotyping set using the GBS anchor genotypes

JAVASCR='java -Xmx160g -jar $BINDIR/java_code/IBDfilter/dist/IBDfilter.jar'
NCPU=20

# Everything assumed to be split by chromosome. All genotype files assumed to be in RBfmt format

for i in {1..10}
do

# This is the file with genotypes on GBS sites (GBS anchor)
GBSFILE=thr.all_c${i}_bqc10_q30_bwamem_GBS

# This is the file to be filtered
GENOFILE=thr.all_c${i}_bqc10_q30_bwamem

# Run filtering on $NCPU CPUs, using minimum of 200 sites to calculate genetic distance in a window of 2000 GBS sites.
# A pair of taxa is in OBD in the window if genetic distance over that window is <=0.02.
$JAVASCR pfilter $GBSFILE $GENOFILE uuu 200 0.02 2000 $NCPU >& IBDfilt_c${i}_bqc10_q30_withW.log

#The result will be in file IBDfilt_c${i}_bqc10_q30_withW.log - to be parsed by script extract_IBD_filtered_sites.sh
done
