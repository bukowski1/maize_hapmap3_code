#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

# Extract IBD-filtered sites from raw genotype file
# using the the otput of IBD assessment analysis

SCR=$BINDIR/IBD/general_extract_from_geno.pl

# Everything split by chromosome
for i in {1..10}
do
# Result of IBD assessment (obtained using script run_IBD_all.sh)
FILE=IBDfilt_c${i}_bqc10_q30_withW.log

# Parse the file, extracting IBD-passing positions
awk '{if($NF=="R") print $1}' $FILE > postmp_$i
awk '{if($5==0 && $NF=="W") print $1}' $FILE >> postmp_$i
sort -k 1,1n  postmp_$i >  postmp_${i}.sorted
mv postmp_${i}.sorted postmp_${i}
done

# Extract genotypes for the IBD-passing sites from raw genotype files
for i in {1..10}
do
GENOFILE=thr.all_c${i}_bqc10_q30_bwamem
POSFILE=postmp_$i
$SCR $GENOFILE $POSFILE 0 1 1 | gzip -1 -c > ${GENOFILE}.IBD.gz &
done

# The output files will be gzipped, with names thr.all_c${i}_bqc10_q30_bwamem.IBD.gz
