#!/usr/bin/env perl

# Extract certain positions from a genotyping file in RB format

$genofile = $ARGV[0];
$posfile = $ARGV[1];
$negate = $ARGV[2];     # 0 - include the sites from posfile; 1 - include only sites from outside posfile
$posindex= $ARGV[3];    # 0 for LD file, 1 for geno RBfmt file
$stride = $ARGV[4];     # select every stride-th site from among the ones satisfying the conditions

open(in,$posfile);
while($line = <in>)
{
	chomp $line;
	$positions{$line}++;
}
close in;

#$nnn = scalar (keys %positions);
#print "Positions found: $nnn\n";

$counter = 0;
open(in,$genofile);
while($line = <in>)
{
	if($line =~ /#/)
	{
		print $line;
		next;
	}
        chomp $line;
        @aux = split /\t/, $line;
	if($negate)
	{
		if($positions{$aux[$posindex]} ne "") { next; }
	}
	else
	{
		if($positions{$aux[$posindex]} eq "") { next; }
	}

	$counter++;
	if($counter == $stride)
	{
		print "$line\n";
		$counter = 0;
	}
}
close in;
