#!/bin/bash

# Run LD evaluation for a genotype set (typically - this would be 
# an IBD-filtered set).
# This is run for a single chromosome specified as input parameter.
# The input is a (gzipped) IBD-filtered genotype file for the given
# chromosome and the GBS anchor file (concatenated over all chromosomes.
# The parameters file LDparams.xml is also needed.

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

CHR=$1
JAR=$BINDIR/java_code/LDfilter/dist/LDfilter.jar
NPROC=25

FNAMECORE=c${CHR}_bqc10_q30_bwamem

# The IBD-filtered genotype file
GENOFILE=thr.all_${FNAMECORE}.IBD.gz

# Run the LD evaluation in parallel on $NPROC cores
java -Xmx160g -jar $JAR makeld $GENOFILE LDparams.xml $NPROC true >& log.LDthr_all_${FNAMECORE}.IBD.GBSanchor

# Collect the partial thread files
$BINDIR/LD/collect_LD.sh

mv LDthr_all LDthr_all_${FNAMECORE}.IBD.GBSanchor
rm LDthr.*

