#!/usr/bin/env perl

# Filter a VCF file based on proximity to indels
# Assumes the files only contain SNPs for one chromosome
# If the VCFfile present on argument list, replace the VCF header with the new one...

$sprdfile = $ARGV[0];  # collect indel positions from this file - assume they are ready-made
$sprdfile1 = $ARGV[1];  # This file will be filtered
$vcfheader = $ARGV[2];  # VCF header line (optional - if absent, the e4xisting header will be used)

$save_indels = 1;

# If the second file is not given, then the same file will be used
if($sprdfile1 eq "") { $sprdfile1 = $sprdfile; }

if($sprdfile =~ /\.gz/)
{
	$sprdfile = "gzip -d -c $sprdfile |";
}
if($sprdfile1 =~ /\.gz/)
{
        $sprdfile1 = "gzip -d -c $sprdfile1 |";
}

if($vcfheader ne "")
{
	open(in,$vcfheader);
	while($line=<in>)
	{
		print $line;
	}
	close in;
}

$rdlen = 5;  # if an indel is found within this distance from a SNP, the SNP one will be rejected
$FLAG = "NI$rdlen";

# First, collect indels 

@badsnp = ();
open(in,"$sprdfile");
while($line=<in>)
{
	chomp $line;
	if(length($line) == 0 || $line =~ m/^#/)
	{
		next;
	}
	push(@badsnp, $line);
}
close in;


$num_badsnp = $#badsnp + 1;

# Now scan the file again determining the position of the "good" SNPs wrt closest indels
$srch_strt = 0;
%distdist = {};
$tot_rejected = 0;
$tot_good_snps = 0;
open(in,"$sprdfile1");
while($line=<in>)
{
        chomp $line;
	if(length($line) == 0)
        {
                next;
        }
	if($line =~ m/^##/)  # just echo the line
        {
                if($vcfheader eq "") { print "$line\n"; }
		next;
        }
	if($line =~ m/^#CHROM/)  # just echo the line
        {
                print "$line\n";
                next;
        }

        @aux = split "\t", $line;
        if(! ($aux[4] =~ m/[DI]/))
        {
		$tot_good_snps++;
		#print "$aux[0] $aux[1] search from $srch_strt ";
		$num_bad_neighbors = 0;
		for($i=$srch_strt;$i<$num_badsnp;$i++)
		{
			$dist = abs($badsnp[$i] - $aux[1]);
			if($dist <= $rdlen)
			{
				$num_bad_neighbors++;
				if($num_bad_neighbors == 1) { $srch_strt = $i; }
				$distdist{$dist}++;
			}
			else
			{
				if($badsnp[$i] > $aux[1])
				{
					last;
				}
			}
		}

		if($num_bad_neighbors > 0) 
		{ 
			# close to indel - add the flag to INFO field...
			$aux[7] .= ";$FLAG";
			$line_mod = join("\t",@aux);
			$tot_rejected++; 
			print "$line_mod\n";
		}
		else
		{
			# away from indels - just echo the line....
			print "$line\n";
		}
		#print "found $num_bad_neighbors bad neighbors stopped at $i\n";
        }
	else
	{
		# This itself is an indel..
		$aux[7] .= ";$FLAG";
                $line_mod = join("\t",@aux);
                $tot_rejected++;
		$tot_good_snps++;
                print "$line_mod\n";
	}
}
close in;

print STDERR "# SNP refiltering stats for file $sprdfile1:\n";
print STDERR "# Sites marked $FLAG if closer than $rdlen to indel\n";
print STDERR "# Found $num_badsnp indels\n";
print STDERR "# Total sites marked: $tot_rejected out of $tot_good_snps\n\n";

#print "# Distribution of distance from indel:\n\n";
#print "# Distance\tCount\n";
#foreach $key (sort {$a <=> $b} keys %distdist)
#{
#      if($distdist{$key} ne "") {print "# ".$key."\t".$distdist{$key}."\n";}
#}


