#!/usr/bin/env perl

# Takes two lists of strings in format str\n
# and produce another list being the result of the requested
# set operation (union, diff, or cross).
# Empty lines and lines starting with # in the input files
# are ignored.

@files = ();
if($#ARGV != 2)
{
	print "# This program needs two file names and the operation (union, diff, or cross)!!!\n";
	exit;
}
for($i=0;$i<$#ARGV;$i++)
{
	push(@files,$ARGV[$i]);
}

print "# Files supplied:\n";
for($ifile=0;$ifile<=$#files;$ifile++)
{
	print "# $files[$ifile]\n";
}

$oper = $ARGV[2];
print "# Operation trequested: $oper\n";

%SNPs = {};
%SNPcounts = {};

for($ifile=0;$ifile<=$#files;$ifile++)
{
open(in,$files[$ifile]);
while($line=<in>)
{
	chomp $line;
	if(length($line) == 0 || $line =~ m/^#/)  # just skip such lines
        {
                next;
        }

	$snpstr = $line; 
	$snppos = $snpstr;

	#$SNPcounts{$snppos} += $ifile+1;
	if($ifile == 0)
	{
		$SNPcounts{$snppos} = 1;
	}
	else
	{
		if($SNPcounts{$snppos} eq "")
		{
			$SNPcounts{$snppos} = 2;
		}
		else
		{
			$SNPcounts{$snppos} = 3;
		}	
	}
	if($SNPs{$snppos} eq "")
	{
		$SNPs{$snppos} = $snpstr;
	}
	else
	{
		if($SNPs{$snppos} ne $snpstr)
		{
			print "# WARNING: item $snppos should be $SNPs{$snppos} but is $snpstr in file $files[$ifile]\n";
		}	
	}
}
close in;
}

# Analyze and print the collected hash of items
if($oper eq "union")
{
	$s = 1; $e = 3;
}
elsif($oper eq "diff")
{
	$s = 1; $e = 1;
}
elsif($oper eq "cross")
{
	$s = 3; $e = 3;
}
else
{
	print "# Operation $oper not valid\n";
	exit;
}
print "# Resulting item set:\n";
foreach $key ( keys %SNPcounts)
{
	if($SNPcounts{$key} eq "") { next; }
        if($SNPcounts{$key} >= $s && $SNPcounts{$key} <= $e) 
	{
		($ref, $allele) = split "_", $SNPs{$key};
		print "$SNPs{$key}\n";
	}
}

