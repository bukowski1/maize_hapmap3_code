#!/usr/bin/env perl

$infile=$ARGV[0];

$min_pv = 1E-45;
$abs_pv_thr = 0;
$ratio_thr = 10; # if an alternative site has a p-value less that this times
		# the best p-value, it will be checked for distance
$dist_thr = 1000000;  # if distabce to site larger than this, consider
			# the mapping to be wrong...

open(in,$infile);
$line = <in>;

$unclass = 0;
$wrong_best = 0;
$wrong_secondary = 0;
$right = 0;
while($line=<in>)
{
	chomp $line;
	@aux = split /\t/, $line;

	if($aux[2] == -1) 
	{ 
		$unclass++; 
		print "$aux[0]\tU\n";
		next; 
	}

	($pos, $r2_best, $pv_best, $dist_best, $chr_best) = ($aux[0], $aux[1], $aux[2], $aux[3], $aux[5]);
	$dist_best = abs($dist_best);
	$prtline = "$pos\t$r2_best\t$pv_best\t$dist_best";

	if($pv_best == 0) { $pv_best = $min_pv; } # to make division always possible

	if($dist_best > $dist_thr) 
	{ 
		$wrong_best++; 
		print "$prtline\tW\n";
		next; 
	}

	@all_pv_best = split ",", $aux[7];
	@all_ccord_best = split ",", $aux[8];

	# Consider all alternative sites with comparable p-values
	# (they are already listed in order of increasing p-value)
	# and check the distances....
	$wrong_map = 0;
	for($i=1;$i<=$#all_pv_best;$i++)
	{
		if($all_pv_best[$i]/$pv_best < $ratio_thr || $all_pv_best[$i] < $abs_pv_thr)
		{
			$dist = abs($pos - $all_ccord_best[$i]);
			if($dist > $dist_thr) { $wrong_map++; };
		}
		else
		{
			last;
		}
	}
	if($wrong_map > 0)
	{
		print "$prtline\tW\t$wrong_map\n";
		$wrong_secondary++;
	}
	else
	{
		print "$prtline\tR\n";
		$right++;
	}
}
close in;

print "Good LD:\t$right\n";
print "Bad best:\t$wrong_best\n";
print "Bad secondary:\t$wrong_secondary\n";
print "Uncalassifiable:\t$unclass\n";
