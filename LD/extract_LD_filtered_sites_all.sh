#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

SCR=$BINDIR/LD/general_extract_from_geno.pl
SCR1=$BINDIR/LD/filter_LD_info.pl
SCR2=$BINDIR/LD/mark_indel_vicinity_vcf.pl
SCR3=$BINDIR/LD/string_sets_ops.pl

CONVERT="java -Xmx5g -jar $BIDIR/java_code/IBDfilter/dist/IBDfilter.jar convert RBfmt2vcf"
VCFHEADER=$BINDIR/LD/VCFheader

POSPREP=1
MAKEVCF=1

# Extract all sites with local hits (even if the site also hits non-locally)
if [ $POSPREP == 1 ]
then
for i in {1..10}
do
FNAMECORE=c${i}_bqc10_q30_bwamem

LDFILE=LDthr_all_${FNAMECORE}.IBD.GBSanchor
IBDFILE=IBDfilt_c${i}_bqc10_q30_withW.log
# How to distinguish IBD1 positons?
# Will need to extract stuff from IBDflt files here and cross with Rpostmp_IBD_$i and Upostmp_IBD_$i...
# Extract true IBD positions:
awk '{if($NF=="R") print $1}' $IBDFILE | sort -u -k 1,1n > postmp_trueIBD_$i
# Extrat IBD1 positions:
awk '{if($5==0 && $NF=="W") print $1}' $IBDFILE | sort -u -k 1,1n > postmp_IBD1_$i
# These will include both IBD and IBD1 positions:
$SCR1 $LDFILE $i | awk '{if(NF==6 && $6 ~ /[Rr]/) print $1}' >  Rpostmp_allIBD_$i
$SCR1 $LDFILE $i | awk '{if($NF ~ /U/) print $1}' >  Upostmp_allIBD_$i
# Now make the intersections
$SCR3 Rpostmp_allIBD_$i postmp_trueIBD_$i cross | grep -v "#" | sort -u -k 1,1n > Rpostmp_IBD_$i
$SCR3 Upostmp_allIBD_$i postmp_trueIBD_$i cross | grep -v "#" | sort -u -k 1,1n > Upostmp_IBD_$i
$SCR3 Rpostmp_allIBD_$i postmp_IBD1_$i cross | grep -v "#" | sort -u -k 1,1n > Rpostmp_IBD1_$i
$SCR3 Upostmp_allIBD_$i postmp_IBD1_$i cross | grep -v "#" | sort -u -k 1,1n > Upostmp_IBD1_$i
done
fi

if [ $MAKEVCF == 1 ]
then
for i in {1..10}
do
FNAMECORE=c${i}_bqc10_q30_bwamem

GENOFILE=thr.all_${FNAMECORE}.IBD
POSFILE=Rpostmp_IBD_$i
VCFFILE1=${GENOFILE}.trueIBD_LD_R.vcf
gzip -d -c ${GENOFILE}.gz | $SCR - $POSFILE 0 1 1 | $CONVERT - $VCFFILE1 $VCFHEADER "LLD"
POSFILE=Upostmp_IBD_$i
VCFFILE2=${GENOFILE}.trueIBD_LD_U.vcf
gzip -d -c ${GENOFILE}.gz | $SCR - $POSFILE 0 1 1 | $CONVERT - $VCFFILE2 $VCFHEADER 

POSFILE=Rpostmp_IBD1_$i
VCFFILE3=${GENOFILE}.IBD1_LD_R.vcf
gzip -d -c ${GENOFILE}.gz | $SCR - $POSFILE 0 1 1 | $CONVERT - $VCFFILE3 $VCFHEADER "IBD1;LLD"
POSFILE=Upostmp_IBD1_$i
VCFFILE4=${GENOFILE}.IBD1_LD_U.vcf
gzip -d -c ${GENOFILE}.gz | $SCR - $POSFILE 0 1 1 | $CONVERT - $VCFFILE4 $VCFHEADER "IBD1"

vcf-concat $VCFFILE1 $VCFFILE2 $VCFFILE3 $VCFFILE4 > concat_${i}.vcf
cat concat_${i}.vcf | vcf-sort -t ./ > thr.all_${FNAMECORE}.IBDall.LDall.vcf
rm  concat_${i}.vcf $VCFFILE1 $VCFFILE2 $VCFFILE3 $VCFFILE4

# Mark indel proximate positions in the final file
INDPOS=indelpos.$FNAMECORE
GENOFILECORE=thr.all_${FNAMECORE}.IBDall.LDall
$SCR2 $INDPOS ${GENOFILECORE}.vcf > ${GENOFILECORE}_indflt5.vcf
rm ${GENOFILECORE}.vcf
done

fi
