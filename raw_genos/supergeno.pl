#!/usr/bin/env perl

$BINDIR="/home/bukowski/HapMap3/maize_hapmap3_code";

$chr = $ARGV[0];          # chromosome 
$regionstrt = $ARGV[1];   # Start of the region to pile up
$regionend = $ARGV[2];    # End of region to genotype
			  # if start, end not given, run the whole chromosome

$chrstart = 1;
$chrend = 350000000;      # Some large number

if($regionstrt eq "")
{
	$regionstrt = 1;
	$regionend = $chrend;
	$incr = 200000000;
}
else
{
	$incr = $regionend - $regionstrt + 10;
}
# Name of the run
$NAME="c${chr}_bqc10_q30_bwamem_tst";

# List of taxa
$sampfile = "samples_tst"

# If non-empty, only these positions will be genotyped
$posfile="";

# Directory to look for BAM files or H5 files with depth/quality numbers
$h5dir = "/workdir/bukowski/bamFiles";

# Genotyper parameters
$paramfile = "params.xml";

# Geotyper command
$javacmd = "java -Xmx120g -jar $BINDIR/java_code/pileup2hdf5/dist/pileup2hdf5.jar ";

# Number of CPU cores to run genotyping on
$proc = 20;

# Number of CPU cores to run pileup on.
# Set this to 0 to use HDF5 files with extracted depths instead of doing pileup
$ioproc = 12;

# Number of positions genotyped by one thread
$chunk = 1000;


$LOGFILE= "log.$NAME";


$strt = $regionstrt;
while($strt <= $regionend && $strt <= $chrend)
{
	$end = $strt + $incr - 1;
	$cmd = "$javacmd pgenotype $sampfile $paramfile $chr $strt $end $proc $chunk $h5dir $ioproc $posfile >& ${NAME}.$strt-$end";
        `echo $strt-$end started at >> $LOGFILE; date >> $LOGFILE`;
        `echo $cmd >> $LOGFILE`;
	system($cmd);
	$strt = $end + 1;
}

# Collect output files
$cmd = "for i in thr.c${chr}.*; do echo \$i; done | awk '{split(\$1,a,\".\"); split(a[3],b,\"-\"); print b[1], \$0}' | sort -k 1,1n | awk '{print \$2}'";
open(in,"$cmd |");
$line=<in>;
chomp $line;
`cat $line > thr.all_$NAME`;   # first file with header
while($line=<in>)
{
	chomp $line;
	`cat $line | grep -v "#" >> thr.all_$NAME`; # strip header from subsequent files
}
close in;
#`rm thr.c${chr}.*`;   # This will fail if file list too long
$cmd="for i in thr.c${chr}.*; do rm \$i; done";
system($cmd);

# Collect log files
$cmd = "ls -1 ${NAME}.* | grep -v log | awk '{split(\$1,a,\".\"); split(a[2],b,\"-\"); print b[1], \$0}' | sort -k 1,1n | awk '{print \$2}'";
open(in,"$cmd |");
while($line=<in>)
{
        chomp $line;
        `cat $line >> log.$NAME`;
}
close in;
`rm ${NAME}.*`;

