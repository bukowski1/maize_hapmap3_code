#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

# Define the following parameters:

# File with sample names (one name per row):
SAMPFILE=samples_tst

# Parameter file (xml)
# In the parameters file, the only important fields are the mpileup command and the reference genome fasta file, e.g.:
#
# <mpileupmaincmd>/programs/samtools-0.1.19/samtools mpileup -A -Q 10 -q 30 -d 60000 -f maize3.fa</mpileupmaincmd>
# <ref_h5_file>maize3.fa</ref_h5_file>
#
# It is assumed that the file maize3.fa and the corresponding index maize3.fa.fai are in the current directory,
# in which the run is started.
PARFILE=params.xml

# Directpry with BAM files
BAMDIR=/workdir/bukowski/bamFiles

# Directory where the H5 files will be stored (in separate chromosome subdirectories)
H5ROOT=/workdir/bukowski/h5_bqc10_q30

# JAVA comand - adjust memory and jar file location, if needed
JAVACMD="java -Xmx120g -jar $BINDIR/java_code/pileup2hdf5/dist/pileup2hdf5.jar "

# How may CPUs to use (may need some experimentation, but more than 20 may be too much)
NCPU=15

# This is the script proper It will loop over chromosomes and put the reated h5 files in
# separate directories (one directory per chromosome).

for i in {1..10}
do

CHR=$i
echo $CHR Started at
date
$JAVACMD gpwrite $PARFILE $SAMPFILE $CHR 0 0 $NCPU $BAMDIR > pileup_to_h5_c${CHR}.log

mkdir -p $H5ROOT/c${CHR}
mv *.h5 $H5ROOT/c${CHR}
mv pileup_to_h5_c${CHR}.log $H5ROOT/c${CHR}

echo H5 conversion for $CHR ended at
date

done
