/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ibdfilter;

/**
 *
 * @author bukowski
 */
public class IBDfilter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        if(args[0].equals("filter"))
        {
            String hm3gbsSNPsFile = args[1];
            String hm3SNPsFile = args[2];
            String outFile = args[3];
            int minComp = Integer.parseInt(args[4]);
            double maxIBDDist = Double.parseDouble(args[5]);
            int windowSize = Integer.parseInt(args[6]);
            Filter.filterBasedOnIBD(hm3gbsSNPsFile, hm3SNPsFile, outFile, minComp, maxIBDDist, windowSize);
        }
        else if (args[0].equals("pfilter"))
        {
            String hm3gbsSNPsFile = args[1];
            String hm3SNPsFile = args[2];
            String outFile = args[3];
            int minComp = Integer.parseInt(args[4]);
            double maxIBDDist = Double.parseDouble(args[5]);
            int windowSize = Integer.parseInt(args[6]);
            int numthreads = Integer.parseInt(args[7]);
            int bestContrasts = -1;
            if(args.length > 8)
            {
                bestContrasts = Integer.parseInt(args[8]);
            }
            Filter.PfilterBasedOnIBD(hm3gbsSNPsFile, hm3SNPsFile, outFile, minComp, maxIBDDist, windowSize, numthreads, bestContrasts);
            //Filter.metaPfilter(hm3gbsSNPsFile, hm3SNPsFile, outFile, minComp, maxIBDDist, windowSize, numthreads);
        }
        else if(args[0].equals("check"))
        {
            String hm3gbsSNPsFile = args[1];
            int minComp = Integer.parseInt(args[2]);
            int windowSize = Integer.parseInt(args[3]);
            String taxon1 = args[4];
            String taxon2 = args[5];
            Filter.checkGenDist(hm3gbsSNPsFile, minComp, windowSize, taxon1, taxon2);
        }
        else if(args[0].equals("convert"))
        {
            String fromto = args[1];
            String fileIn = args[2];
            String fileOut = args[3];
            if(fromto.equals("RBfmt2hmp") || fromto.equals("vcf2hmp"))
            {
                convert.convertRBfmt2HapMap(fileIn, fileOut);
            }
            else if(fromto.equals("RBfmt2vcf"))
            {
                String VCFheaderFile = null;
                if(args.length > 4) { VCFheaderFile = args[4]; }
                String addInfoFlags = null;
                if(args.length > 5) { addInfoFlags = args[5]; }
                convert.convertRBfmt2VCF(fileIn, fileOut, VCFheaderFile, addInfoFlags);
            }
        }
        else
        {
            System.out.println("Action "+args[0]+" not known");
        }
    }
}
