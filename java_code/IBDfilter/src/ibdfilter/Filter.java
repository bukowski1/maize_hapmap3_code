/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ibdfilter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import static net.maizegenetics.dna.snp.GenotypeTable.*;
import net.maizegenetics.dna.snp.GenotypeTableUtils;
import static net.maizegenetics.dna.snp.GenotypeTableUtils.*;
import net.maizegenetics.dna.snp.GenotypeTableBuilder;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.io.BuilderFromVCF;
import net.maizegenetics.popgen.distance.DistanceMatrix;
import net.maizegenetics.popgen.distance.IBSDistanceMatrix;

/**
 *
 * @author bukowski
 */
public class Filter {


public static void filterBasedOnIBD(String hm3gbsSNPsFile, String hm3SNPsFile, String outFile, int minComp, double maxIBDDist, int windowSize) {
    
        int lowPhysLimit;
        int highPhysLimit;
        String chr;
        
        GenotypeTable hm3gbs=BuilderFromVCF.getBuilder(hm3gbsSNPsFile).build();        
        GenotypeTable hm3=BuilderFromVCF.getBuilder(hm3SNPsFile).build();
        
        if(hm3 == null) { System.out.println(" hm3 is null...."+hm3SNPsFile); }
        if(hm3gbs == null) { System.out.println(" hm3gbs is null...."+hm3gbsSNPsFile); }
        
        // try to determine the chromosome automatically
        chr = hm3gbs.chromosomes()[0].getName();
        if(! chr.equals(hm3.chromosomes()[0].getName()))
        {
            System.out.println("Chromosome names not the same: "+chr+", "+hm3.chromosomes()[0].getName());
            return;
        }
        // Determine the low and high physical limits of the hm3 file
        lowPhysLimit = hm3.physicalPositions()[0];
        highPhysLimit = hm3.physicalPositions()[hm3.numberOfSites()-1];
        System.out.println("Low, High phys limit in hm3: "+ lowPhysLimit+", "+highPhysLimit);
        
        // Normalize taxa names
        List<String> hm3gbsTaxaNames = new ArrayList<String>();
        for(int i=0;i<hm3gbs.numberOfTaxa(); i++)
        {
            hm3gbsTaxaNames.add(hm3gbs.taxaName(i).replaceAll(".h5", ""));
        }
        List<String> hm3TaxaNames = new ArrayList<String>();
        for(int i=0;i<hm3.numberOfTaxa(); i++)
        {
            hm3TaxaNames.add(hm3.taxaName(i).replaceAll(".h5", ""));
        }
        
        int[] aTobTaxa=new int[hm3gbs.numberOfTaxa()];
            for (int i=0; i<hm3gbs.numberOfTaxa(); i++) {
                aTobTaxa[i]=Integer.MIN_VALUE;
                int tind = hm3TaxaNames.indexOf(hm3gbsTaxaNames.get(i));
                if(tind>-1) 
                {
                    aTobTaxa[i]=tind;
                    System.out.println("hm3gbs taxon "+hm3gbsTaxaNames.get(i)+" found in hm3 file at index "+tind+" ("+hm3TaxaNames.get(tind)+")");
                }
                else
                {
                    System.out.println("hm3gbs taxon "+hm3gbsTaxaNames.get(i)+" has no counterpart in hm3 file");
                }
            }
        GenotypeTableBuilder gtb=GenotypeTableBuilder.getSiteIncremental(hm3.taxa());
        for (int start=0; start+windowSize < hm3gbs.numberOfSites(); start+=windowSize) {
            
            int phys_start_gbs = hm3gbs.physicalPositions()[start];
            int phys_end_gbs = hm3gbs.physicalPositions()[start+windowSize];
            
            if(phys_end_gbs < lowPhysLimit || phys_start_gbs > highPhysLimit) { continue; }
            
            int endInstance = phys_end_gbs;
            int startInstance = phys_start_gbs;
            if(endInstance > highPhysLimit)
            {
                endInstance = highPhysLimit;
            }
            if(startInstance<lowPhysLimit)
            {
                startInstance = lowPhysLimit;
            }
          
            System.out.println("phys_start_gbs: "+phys_start_gbs + " phys_end_gbs: "+phys_end_gbs);
            System.out.println("startInstance: "+startInstance + " endInstance: "+endInstance);
            
            //GenotypeTable a=hm3gbs;
            GenotypeTable a=FilterGenotypeTable.getInstance(hm3gbs,start,start+windowSize);
            //GenotypeTable b=hm3;
            GenotypeTable b=FilterGenotypeTable.getInstance(hm3, chr, startInstance, endInstance);
            System.out.println("Taxa in b: " + b.numberOfTaxa());
            System.out.println("Sites in b: " + b.numberOfSites());
            
            /*
            int[] aTobTaxa=new int[a.numberOfTaxa()];
            for (int i=0; i<a.numberOfTaxa(); i++) {
                System.out.println(i+":"+a.taxaName(i)+"="+b.taxaName(i));
                if(a.taxaName(i).equals(b.taxaName(i))) System.out.println(a.taxaName(i));
                List<Integer> hits=b.taxa().indicesMatchingTaxon(a.taxaName(i));
                if(hits.size()==1) {aTobTaxa[i]=hits.get(0);}
                else {aTobTaxa[i]=Integer.MIN_VALUE;}
            }
            */
            
            DistanceMatrix dm=new IBSDistanceMatrix(a, minComp, null);
            int countIBD=0, taxaIBD=0, count=0;
            double mean=0;
            int numTestSites=b.numberOfSites();
            int[] mjSame=new int[numTestSites];
            int[] minorSame=new int[numTestSites];
            int[][] rightNuc=new int[6][numTestSites];
            int[][] wrongNuc=new int[6][numTestSites];
            int[] diff=new int[numTestSites];
            int[] het=new int[numTestSites];
            int[] minorComp=new int[numTestSites];
            byte[][] diploids=new byte[3][numTestSites];//mj:mj,mj:mn,mn:mn
            for (int k=0; k<numTestSites; k++) {
                diploids[0][k]=GenotypeTableUtils.getDiploidValue(b.majorAllele(k),b.majorAllele(k));
                diploids[1][k]=GenotypeTableUtils.getDiploidValue(b.majorAllele(k),b.minorAllele(k));
                diploids[2][k]=GenotypeTableUtils.getDiploidValue(b.minorAllele(k),b.minorAllele(k));
            }
            for (int i=0; i<dm.numberOfTaxa(); i++) {
                int ibd=0;
                for (int j=0; j<dm.numberOfTaxa(); j++) {
                    if(i==j || Double.isNaN(dm.getDistance(i,j))) continue;
                    mean+=dm.getDistance(i,j);
                    count++;
                    if(dm.getDistance(i,j)<maxIBDDist) {
                        // The following does not seem to make sense.....
                        //double[] distB=IBSDistanceMatrix.computeHetBitDistances(b,i,j);
                        ibd++;
                        int bI=aTobTaxa[i];
                        int bJ=aTobTaxa[j];
                        if(bI<0 || bJ<0) continue;
                        //double[] distB=IBSDistanceMatrix.computeHetBitDistances(b,bI,bJ);
                        for (int k=0; k<numTestSites; k++) {
                            byte t1b=b.genotype(bI,k);
                            byte t2b=b.genotype(bJ,k);
                            if(t1b==UNKNOWN_DIPLOID_ALLELE) continue;
                            if(t2b==UNKNOWN_DIPLOID_ALLELE) continue;
                            byte[] t1ba=getDiploidValues(t1b);
                            byte[] t2ba=getDiploidValues(t2b);
                            if(t1ba[0]==t2ba[0] || t1ba[0]==t2ba[1]) {rightNuc[t1ba[0]][k]++;} else {wrongNuc[t1ba[0]][k]++;}
                            if(t1ba[1]==t2ba[0] || t1ba[1]==t2ba[1]) {rightNuc[t1ba[1]][k]++;} else {wrongNuc[t1ba[1]][k]++;}
                            // Uncomment two next lines to ignire hets
                            //if(t1b!=diploids[0][k] && t1b!=diploids[2][k]) continue;
                            //if(t2b!=diploids[0][k] && t2b!=diploids[2][k]) continue;
                            if(isHeterozygous(t1b)  || isHeterozygous(t2b)) het[k]++;
                            if(t1b==diploids[2][k] || t2b==diploids[2][k])  {
                                minorComp[k]++;
                                if(t1b==t2b) minorSame[k]++;
                            }
                            if(t1b==t2b) {mjSame[k]++;} else {diff[k]++;}

                        }
                        //System.out.printf("%d %d %g %g %n",i,j,dm.getDistance(i,j),distB[0]);
                    }
                }
                if(ibd>0) taxaIBD++;
                countIBD+=ibd;
            }
            mean/=(double)count;
            System.out.printf("Window:%d StartSite:%d Taxa:%d maxIBDDist:%g IBDTaxa:%d IBDContrasts:%d MeanDist:%g%n",
                    windowSize, start,hm3gbs.numberOfTaxa(),maxIBDDist,taxaIBD,countIBD,mean);
            for (int k=0; k<numTestSites; k++) {
                double minorRate=(double)minorSame[k]/((double)diff[k]+0.5);
                int countBiased=0;
                int counttries = 0;
                for (int bs=0; bs<6; bs++) {
                    if(((double)rightNuc[bs][k]/(0.5+wrongNuc[bs][k])) >2.0) countBiased++;
                    counttries += rightNuc[bs][k] + wrongNuc[bs][k];
                }
                //if(minorComp[k]>0 && minorRate>1) {
                if(countBiased>1 || counttries == 0) {
                    // "Good" site
                    System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",b.positions().get(k).getPosition(),het[k],
                            mjSame[k],minorSame[k],diff[k],minorComp[k], minorRate);
                    for (int bs=0; bs<6; bs++) {
                        System.out.print(rightNuc[bs][k]+":"+wrongNuc[bs][k]+"\t");
                    }
//                    for (int[] rV : rightNuc) {System.out.print(rV[k]+"\t");}
//                    for (int[] wV : wrongNuc) {System.out.print(wV[k]+"\t");}
                    if(counttries>0)
                    {
                        System.out.print("R");
                    }
                    else
                    {
                        System.out.print("A");   
                    }
                    System.out.println();
                    gtb.addSite(b.positions().get(k),b.genotypeAllTaxa(k));
                }
                /*
                else
                {
                    // "Other" site
                    System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",b.positions().get(k).getPosition(),het[k],
                            mjSame[k],minorSame[k],diff[k],minorComp[k], minorRate);
                    for (int bs=0; bs<6; bs++) {
                        System.out.print(rightNuc[bs][k]+":"+wrongNuc[bs][k]+"\t");
                    }
//                    for (int[] rV : rightNuc) {System.out.print(rV[k]+"\t");}
//                    for (int[] wV : wrongNuc) {System.out.print(wV[k]+"\t");}
                    System.out.print("W");
                    System.out.println();
                    
                }
                */

            }
            //GenotypeTable gt=gtb.build();
            //ExportUtils.writeToHapmap(gt,false,outFile,'\t',null);
        }
        GenotypeTable gt=gtb.build();
        //ExportUtils.writeToHapmap(gt,false,outFile,'\t',null);
        ExportUtils.writeToVCF(gt, outFile, '\t');
    }

public static void PfilterBasedOnIBD(String hm3gbsSNPsFile, String hm3SNPsFile, String outFile, int minComp, double maxIBDDist, 
        int windowSize, int numThreads, int bestContrasts) {
    
        int lowPhysLimit;
        int highPhysLimit;
        String chr;
        
        GenotypeTable hm3gbs=BuilderFromVCF.getBuilder(hm3gbsSNPsFile).build();        
        GenotypeTable hm3=BuilderFromVCF.getBuilder(hm3SNPsFile).build();
        
        if(hm3 == null) { System.out.println(" hm3 is null...."+hm3SNPsFile); }
        if(hm3gbs == null) { System.out.println(" hm3gbs is null...."+hm3gbsSNPsFile); }
        
        // try to determine the chromosome automatically
        chr = hm3gbs.chromosomes()[0].getName();
        if(! chr.equals(hm3.chromosomes()[0].getName()))
        {
            System.out.println("Chromosome names not the same: "+chr+", "+hm3.chromosomes()[0].getName());
            return;
        }
        else
        {
            System.out.println("Processing chromosome "+chr);
        }
        // Determine the low and high physical limits of the hm3 file
        lowPhysLimit = hm3.physicalPositions()[0];
        highPhysLimit = hm3.physicalPositions()[hm3.numberOfSites()-1];
        System.out.println("Low, High phys limit in hm3: "+ lowPhysLimit+", "+highPhysLimit);
        
        // Normalize taxa names
        List<String> hm3gbsTaxaNames = new ArrayList<String>();
        for(int i=0;i<hm3gbs.numberOfTaxa(); i++)
        {
            hm3gbsTaxaNames.add(hm3gbs.taxaName(i).replaceAll(".h5", ""));
        }
        List<String> hm3TaxaNames = new ArrayList<String>();
        for(int i=0;i<hm3.numberOfTaxa(); i++)
        {
            hm3TaxaNames.add(hm3.taxaName(i).replaceAll(".h5", ""));
        }
        
        int[] aTobTaxa=new int[hm3gbs.numberOfTaxa()];
            for (int i=0; i<hm3gbs.numberOfTaxa(); i++) {
                aTobTaxa[i]=Integer.MIN_VALUE;
                int tind = hm3TaxaNames.indexOf(hm3gbsTaxaNames.get(i));
                if(tind>-1) 
                {
                    aTobTaxa[i]=tind;
                    //System.out.println("hm3gbs taxon "+hm3gbsTaxaNames.get(i)+" found in hm3 file at index "+tind+" ("+hm3TaxaNames.get(tind)+")");
                }
                else
                {
                    System.out.println("hm3gbs taxon "+hm3gbsTaxaNames.get(i)+" has no counterpart in hm3 file");
                }
            }
        //GenotypeTableBuilder gtb=GenotypeTableBuilder.getSiteIncremental(hm3.taxa());
        GenotypeTable a;
        GenotypeTable b;
        int lastHm3SiteProcessed = 0;
        int lastHm3SiteProcPos = 0;
        for (int start=0; start < hm3gbs.numberOfSites(); start+=windowSize) {
            
            int phys_start_gbs = hm3gbs.physicalPositions()[start];
            int phys_end_gbs; 
            int lastGBSsite = start+windowSize-1;
            if(lastGBSsite >= hm3gbs.physicalPositions().length)
            {
                lastGBSsite = hm3gbs.physicalPositions().length-1;
            }
            
            phys_end_gbs = hm3gbs.physicalPositions()[lastGBSsite];
            
            if(phys_end_gbs < lowPhysLimit || phys_start_gbs > highPhysLimit) { continue; }
            
            int endInstance = phys_end_gbs;
            int startInstance = phys_start_gbs;
            if(endInstance > highPhysLimit)
            {
                endInstance = highPhysLimit;
            }
            if(startInstance<lowPhysLimit)
            {
                startInstance = lowPhysLimit;
            }
          
            System.out.println("phys_start_gbs: "+phys_start_gbs + " phys_end_gbs: "+phys_end_gbs);
            System.out.println("startInstance: "+startInstance + " endInstance: "+endInstance);
            
            //GenotypeTable a=hm3gbs;
            a=FilterGenotypeTable.getInstance(hm3gbs,start,lastGBSsite);
            //GenotypeTable b=hm3;
            b=FilterGenotypeTable.getInstance(hm3, chr, startInstance, endInstance);
            if(b == null) { continue; }
            System.out.println("Taxa in b: " + b.numberOfTaxa());
            System.out.println("Sites in b: " + b.numberOfSites());
            
            
            DistanceMatrix dm=new IBSDistanceMatrix(a, minComp, null);
            int numTaxa = dm.numberOfTaxa();
            int numTestSites=b.numberOfSites();
            int numTaxaChunk = 150;
            
            int [][] acounts = new int[numTaxaChunk][3]; // countIBD, taxaIBD, count
            double [][] amean = new double[numTaxaChunk][1];
            int[][] amjSame=new int[numTaxaChunk][numTestSites];
            int[][] aminorSame=new int[numTaxaChunk][numTestSites];
            int[][][] arightNuc=new int[numTaxaChunk][6][numTestSites];
            int[][][] awrongNuc=new int[numTaxaChunk][6][numTestSites];
            int[][] adiff=new int[numTaxaChunk][numTestSites];
            int[][] ahet=new int[numTaxaChunk][numTestSites];
            int[][] aminorComp=new int[numTaxaChunk][numTestSites];
            byte[][] diploids=new byte[3][numTestSites];//mj:mj,mj:mn,mn:mn
            String [][] acontrastPairs = new String[numTaxaChunk][1];
            String [][] afailedContrasts = new String[numTaxaChunk][numTestSites];
            for (int k=0; k<numTestSites; k++) {
                diploids[0][k]=GenotypeTableUtils.getDiploidValue(b.majorAllele(k),b.majorAllele(k));
                diploids[1][k]=GenotypeTableUtils.getDiploidValue(b.majorAllele(k),b.minorAllele(k));
                diploids[2][k]=GenotypeTableUtils.getDiploidValue(b.minorAllele(k),b.minorAllele(k));
            }
            // Initialize string arrays holding contrast info
            for(int i=0;i<numTaxaChunk;i++)
            {
                acontrastPairs[i][0] = "";
                for(int j=0;j<numTestSites;j++)
                {
                    afailedContrasts[i][j] = "";
                }
            }
             
            // If requested, collect the distribution of genetic distances - we will then pick threshold to
            // to use 50 or so best contrasts. Do not parallelize this for now...
            double maxIBDDistAdj = maxIBDDist;
            if(bestContrasts > 0)
            {
                double [] dstmat2sort = new double[numTaxa*(numTaxa-1)/2];
                int counter = 0;
                for (int i=0; i<dm.numberOfTaxa(); i++) 
                {
                    for(int j=i+1;j<dm.numberOfTaxa(); j++)
                    {
                        if(Double.isNaN(dm.getDistance(i,j))) continue;
                        dstmat2sort[counter] = dm.getDistance(i,j);
                        counter++;
                    }
                }
                // Sort the array (in ascending order) and pick, say 50th value from the end as threshold
                // This will ensure we count only 50 best IBD pairs.
                // NOTE: numTaxa*(numTaxa-1)/2 - counter last elements (first after sorting) will be zero!
                Arrays.sort(dstmat2sort);
                // Debug print
                System.out.println("Sorted dstmat2sort matrix:");
                for(int i=0;i<dstmat2sort.length;i++)
                {
                    System.out.print(dstmat2sort[i]+",");
                }
                System.out.println();
            
                maxIBDDistAdj = dstmat2sort[Math.min(dstmat2sort.length-1,bestContrasts-1+dstmat2sort.length-counter)];
                maxIBDDistAdj = Math.min(maxIBDDistAdj,maxIBDDist);
            }
 
            // We will parallelize over i loop
            java.util.concurrent.ExecutorService executor = Executors.newFixedThreadPool(numThreads);
            
            for (int i=0; i<dm.numberOfTaxa(); i++) {
                
                int ir = i % numTaxaChunk;
                
                pFilter pflt = new pFilter(b, dm, i, maxIBDDistAdj, aTobTaxa, numTestSites, arightNuc[ir], awrongNuc[ir], 
                        diploids, ahet[ir], aminorComp[ir], aminorSame[ir], amjSame[ir], adiff[ir], acounts[ir], amean[ir],
                        acontrastPairs[ir], afailedContrasts[ir]);
                
                executor.execute(pflt);
                
                // Re-create the thread pool 
                if(ir == numTaxaChunk-1)
                {
                    executor.shutdown();
                    try
                    {
                        executor.awaitTermination(100, TimeUnit.DAYS);
                    }
                    catch(Exception e)
                    {
                        System.out.println("executor termination problem: "+e);
                    }
                    executor = Executors.newFixedThreadPool(numThreads);
                }
            }
            
            executor.shutdown();
            try
            {
                executor.awaitTermination(100, TimeUnit.DAYS);
            }
            catch(Exception e)
            {
                System.out.println("executor termination problem: "+e);
            }
            
            // Collect cummmulatives over threads
            int countIBD=0, taxaIBD=0, count=0;
            double mean=0;
            int[] mjSame=new int[numTestSites];
            int[] minorSame=new int[numTestSites];
            int[][] rightNuc=new int[6][numTestSites];
            int[][] wrongNuc=new int[6][numTestSites];
            int[] diff=new int[numTestSites];
            int[] het=new int[numTestSites];
            int[] minorComp=new int[numTestSites];
            String contrastPairs = "";
            String [] failedContrasts = new String[numTestSites];
            for(int i=0;i<numTestSites;i++)
            {
                failedContrasts[i] = "";
            }
            for(int i=0;i<numTaxaChunk;i++)
            {
                countIBD += acounts[i][0];
                taxaIBD += acounts[i][1];
                count += acounts[i][2];
                mean += amean[i][0];
                contrastPairs += acontrastPairs[i][0];
                //int cntrslen = acontrastPairs[i][0].split("\n").length;
                //System.out.println("cntrst len for i " + i + ":" + cntrslen);
                for(int k=0;k<numTestSites;k++)
                {
                    mjSame[k] += amjSame[i][k];
                    minorSame[k] += aminorSame[i][k];
                    diff[k] += adiff[i][k];
                    het[k] += ahet[i][k];
                    minorComp[k] += aminorComp[i][k];
                    failedContrasts[k] += afailedContrasts[i][k];
                }
                for(int bs=0;bs<6;bs++)
                {
                    for(int k=0;k<numTestSites;k++)
                    {
                        rightNuc[bs][k] += arightNuc[i][bs][k];
                        wrongNuc[bs][k] += awrongNuc[i][bs][k];
                    }   
                }
            }
            
            mean/=(double)count;
            System.out.printf("Window:%d StartSite:%d Taxa:%d maxIBDDist:%g IBDTaxa:%d IBDContrasts:%d MeanDist:%g%n",
                    windowSize, start,hm3gbs.numberOfTaxa(),maxIBDDistAdj,taxaIBD,countIBD,mean);
            //System.out.println("Contrast pairs:");
            //System.out.print(contrastPairs);
            for (int k=0; k<numTestSites; k++) {
                double minorRate=(double)minorSame[k]/((double)diff[k]+0.5);
                int countBiased=0;
                int counttries = 0;
                for (int bs=0; bs<6; bs++) {
                    if(((double)rightNuc[bs][k]/(0.5+wrongNuc[bs][k])) >2.0) countBiased++;
                    counttries += rightNuc[bs][k] + wrongNuc[bs][k];
                }
                //if(minorComp[k]>0 && minorRate>1) {
                if(countBiased>1 || counttries == 0) {
                    // "Good" site
                    System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",b.positions().get(k).getPosition(),het[k],
                            mjSame[k],minorSame[k],diff[k],minorComp[k], minorRate);
                    for (int bs=0; bs<6; bs++) {
                        System.out.print(rightNuc[bs][k]+":"+wrongNuc[bs][k]+"\t");
                    }
//                    for (int[] rV : rightNuc) {System.out.print(rV[k]+"\t");}
//                    for (int[] wV : wrongNuc) {System.out.print(wV[k]+"\t");}
                    if(counttries>0)
                    {
                        System.out.print("R");
                    }
                    else
                    {
                        System.out.print("A");   
                    }
                    System.out.println();
                    //System.out.print(failedContrasts[k]);
                    //gtb.addSite(b.positions().get(k),b.genotypeAllTaxa(k));
                    lastHm3SiteProcPos = b.positions().get(k).getPosition();
                }
                else // Comment this out after test
                {
                    System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",b.positions().get(k).getPosition(),het[k],
                            mjSame[k],minorSame[k],diff[k],minorComp[k], minorRate);
                    for (int bs=0; bs<6; bs++) {
                        System.out.print(rightNuc[bs][k]+":"+wrongNuc[bs][k]+"\t");
                    }
                    System.out.print("W");
                    System.out.println();
                    //System.out.print(failedContrasts[k]);
                    lastHm3SiteProcPos = b.positions().get(k).getPosition();
                }
                
            }
            lastHm3SiteProcessed += numTestSites;
        }
        // Print the remaining hm3 sites as ambiguous
        System.out.println("Last HM3 position processed: "+ lastHm3SiteProcPos);
        // Remember that lastHm3SiteProcessed does not correspond to the last position on original VCF file
        // because positions numTestSites are only those within the GBS window limit. So the following prinout
        // does not make too much sense anyway...
        /*
        for(int k=lastHm3SiteProcessed;k<hm3.numberOfSites();k++)
        {
            //System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",hm3.positions().get(k).getPosition(),0,
            //                0,0,0,0, 0.0);
            System.out.printf("%d\t%d\t%d\t%d\t%d\t%d\t%g\t",hm3.physicalPositions()[k],0,
                            0,0,0,0, 0.0);
            for (int bs=0; bs<6; bs++) {
                   System.out.print("0:0\t");
            }
            System.out.print("A");
            System.out.println();
        }
         */
        //GenotypeTable gt=gtb.build();
        //ExportUtils.writeToHapmap(gt,false,outFile,'\t',null);
        //ExportUtils.writeToVCF(gt, outFile, '\t');
    }

public static void metaPfilter(String hm3gbsSNPsFile, String hm3SNPsFile, String outFile, int minComp, double maxIBDDist, 
        int windowSize, int numThreads)
{
    String line;
    int incr = 5000000;
    
    long startTime, endTime;
    double duration;
    
    try
    {
        startTime = System.currentTimeMillis();
        
        BufferedReader fileIn = new BufferedReader(new FileReader(hm3SNPsFile), 1000000);
        String header = fileIn.readLine();
        
        int fcounter = 1;
        String genofile = "fle_" + fcounter;
        BufferedWriter fileOut = new BufferedWriter(new FileWriter(genofile), 1000000);
        fileOut.write(header + "\n");
        
        int counter = 0;
        while((line=fileIn.readLine()) != null)
        {
           if(counter > incr)
           {
               //close the increment file and run IBD filtering on it, then remove the increment file
               // update/reset counters and open the next inceremnt file
               fileOut.close();
               
               endTime = System.currentTimeMillis();
               duration = 0.001*(endTime-startTime)/60;
               startTime = endTime;
               System.out.println("File chunk "+fcounter + " written in " + duration + " minutes");
               
               // Run filtering here and then rmeove the file....
               (new File(genofile)).delete();
               
               endTime = System.currentTimeMillis();
               duration = 0.001*(endTime-startTime)/60;
               startTime = endTime;
               System.out.println("File chunk "+fcounter + " IBD-filtered in " + duration + " minutes");
               
               counter = 0;
               fcounter++;
               genofile = "fle_" + fcounter;
               fileOut = new BufferedWriter(new FileWriter(genofile), 1000000);
               fileOut.write(header + "\n");
           }
           
           fileOut.write(line+"\n");
           counter++;
        }
        fileIn.close();
        fileOut.close();
        
        endTime = System.currentTimeMillis();
        duration = 0.001*(endTime-startTime)/60;
        startTime = endTime;
        System.out.println("File chunk "+fcounter + " written in " + duration + " minutes");
        
        // Filter and remove the last file
        (new File(genofile)).delete();
        
        endTime = System.currentTimeMillis();
        duration = 0.001*(endTime-startTime)/60;
        startTime = endTime;
        System.out.println("File chunk "+fcounter + " IBD-filtered in " + duration + " minutes");
        
    }
    catch(Exception e)
    {
        System.out.println("Error processing file "+hm3SNPsFile+": "+e);
    }
    
}

    public static void checkGenDist(String hm3gbsSNPsFile, int minComp, int windowSize, String taxon1, String taxon2)
    {
        int indI, indJ;
        GenotypeTable hm3gbs=BuilderFromVCF.getBuilder(hm3gbsSNPsFile).build(); 
        
        List<Integer> hits1=hm3gbs.taxa().indicesMatchingTaxon(taxon1);
        if(! hits1.isEmpty())
        {
            indI = hits1.get(0);
        }
        else
        {
            System.out.println("Taxon "+taxon1 +" not found");
            return;
        }
        
        List<Integer> hits2=hm3gbs.taxa().indicesMatchingTaxon(taxon2);
        if(! hits2.isEmpty())
        {
            indJ = hits2.get(0);
        }
        else
        {
            System.out.println("Taxon "+taxon2 +" not found");
            return;
        }
        System.out.println("Checking genetic distance for taxa "+taxon1 + " ("+indI+") and "+taxon2+" ("+indJ+")");
        
        
        System.out.println("Window_start\tPhysical_start\tPhysical_end\tdistance");
        for (int start=0; start+windowSize < hm3gbs.numberOfSites(); start+=windowSize)
        {
            GenotypeTable a=FilterGenotypeTable.getInstance(hm3gbs,start,start+windowSize);
            DistanceMatrix dm=new IBSDistanceMatrix(a, minComp, null);
            
            double dist = dm.getDistance(indI, indJ);
            int end = start + windowSize-1;
            int phys_start = hm3gbs.physicalPositions()[start];
            int phys_end = hm3gbs.physicalPositions()[end];
            System.out.println(start+"\t"+phys_start + "\t"+phys_end + "\t"+dm.getDistance(indI, indJ));
            
        }
        
    }
}
