/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ibdfilter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.io.BuilderFromVCF;
import net.maizegenetics.dna.snp.ExportUtils;

/**
 * Convert form RBfmt (presumably VCF reading methods of TASSEL5 handle this)
 * into HapMap format
 * @author bukowski
 */
public class convert {
    
    public static void convertRBfmt2HapMap(String fileIn, String fileOut)
    {
        GenotypeTable gt=BuilderFromVCF.getBuilder(fileIn).build();
        ExportUtils.writeToHapmap(gt,false,fileOut,'\t',null);
    }
    
    public static void convertRBfmt2VCF(String infile, String outfile, String VCFheaderFile, String addInfoFlags)
    {
        int linecount = 0;
        String line;
        String [] tokens;
        int numsamples;
        int NONTAXA_COLUMNS = 5;
        int INFO_POSITION = 4;
        StringBuilder outstrbld;
        int curpos = 0;
        String outstr;
        String allele_str;
        String info;
        String [] aux;
        long startTime;
        long startTime0;
        long endTime;
        double duration;
        boolean includeGL = true;
        boolean compute_maf = false;
        
        String FORMATSTR="GT:AD"; // we will skip genotype likelihoods
        if(includeGL) { FORMATSTR="GT:AD:GL"; }
        //String addInfoFlags = "IBD1;LLD";
                
        
        try
        {
            startTime = System.currentTimeMillis();
            startTime0 = startTime;
            
            BufferedReader fileIn;
            InputStreamReader isreader = null;
            if ( ! infile.equals("-"))
            {
                fileIn = new BufferedReader(new FileReader(infile), 1000000);
            }
            else
            {
                isreader = new InputStreamReader(System.in);
                fileIn = new BufferedReader(isreader);
            }
            BufferedWriter fileOut = new BufferedWriter(new FileWriter(outfile), 1000000);
            
            line=null;
            
            // read/write header line first
            line=fileIn.readLine();
            tokens = line.split("\t");
            // Detect if the file misses INFO field
            if(!tokens[INFO_POSITION].equals("INFO"))
            {
                NONTAXA_COLUMNS--;
                INFO_POSITION = -1;
            }
            numsamples = tokens.length - NONTAXA_COLUMNS;
            
            // Construct a VCF-stype header line
            if(VCFheaderFile == null)
            {
                line = VCFheader();
            }
            else
            {
                line = VCFheaderFromFile(VCFheaderFile);
            }
            line += "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
            outstrbld = new StringBuilder();
            
            outstrbld.append(line);
            for(int i=NONTAXA_COLUMNS;i<tokens.length;i++)
            {
                tokens[i] = tokens[i].replaceAll("\\.h5", "");
                outstrbld.append('\t').append(tokens[i]);
            }
            outstr = outstrbld.toString();
            fileOut.write(outstr+"\n");
            
            System.out.println("Input (RBfmt format): "+infile);
            System.out.println("Output (VCF format): "+outfile);
            System.out.println(numsamples + " taxa detected in input file");
            
            // Loop over the rest of lines;
            while((line=fileIn.readLine()) != null)
            {
                if(line.startsWith("#")) { continue; }
                linecount++;
                
                tokens = line.split("\t");
                
                curpos = Integer.parseInt(tokens[1]);
                //if(curpos < startpos) { continue; }
                
                // convert alternative allele string to VCF conventions
                allele_str = tokens[3];
                if(allele_str.equals("N")) { allele_str = "."; }
                allele_str = allele_str.replaceAll("D", "<DEL>").replaceAll("I", "<INS>");
                if(INFO_POSITION > -1)
                {
                    // fix the info string, if needed
                    info = tokens[INFO_POSITION];
                    // This should not happen, but early version of 822-826 taxa fixing
                    // script had a small error leading to ",," instead of ",0,", etc.
                    info = info.replaceAll(",,", ",0,").replaceAll("=,", "=0,").replaceAll(",;", ",0;");
                    // Remove irrelevant fields of INFO that we do not want transmitted to VCF
                    info = info.replaceAll("SR=\\d+;", "").replaceAll("IN=[\\d,]+;", "");
                    // Add any additional flags to info string
                    if(addInfoFlags != null)
                    {
                        info += ";"+addInfoFlags;
                    }
                    // Maybe compute and add MAF?
                    if(compute_maf) { info = addMAFtoInfo(info); }
                }
                else
                {
                    info = ".";
                }
                outstr = tokens[0] + "\t" + tokens[1] + "\t.\t" + tokens[2] + "\t" +allele_str + "\t.\t.\t" + info +"\t" + FORMATSTR;
                outstrbld = new StringBuilder();
                outstrbld.append(outstr);
                
                // Process all samples
                for(int i=NONTAXA_COLUMNS;i<tokens.length;i++)
                {
                    if(tokens[i].equals("./.") || includeGL)
                    {
                        outstrbld.append('\t').append(tokens[i]);
                    }
                    else
                    {
                        aux = tokens[i].split(":");
                        outstr = aux[0] + ":" + aux[1];
                        outstrbld.append('\t').append(outstr);
                    }
                }
                outstr = outstrbld.toString();
                fileOut.write(outstr+"\n");
                
                if(linecount % 100000 == 0)
                {
                    endTime = System.currentTimeMillis();
                    duration = 0.001*(endTime - startTime)/60;
                    startTime = endTime;
                    System.out.println(linecount + ": 100K lines converted in "+duration+" minutes");
                }
            }
            
            // Everything processed - close files
            fileIn.close();
            fileOut.close();
            
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime - startTime0)/60;
            System.out.println(linecount + " lines converted in "+duration+" minutes");
            duration = 60000*duration/linecount;
            System.out.println("Rate: "+duration+" sec per 1000 lines");
        }
        catch(Exception e)
        {
            System.out.println("Error processing line " + linecount + ":" + e);
        }
                
    }
    
    private static String VCFheader()
    {
        String header = "";
        header += "##fileformat=VCFv4.1\n";
        header += "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n";
        header += "##INFO=<ID=NZ,Number=1,Type=Integer,Description=\"Number of taxa with data\">\n";
        header += "##INFO=<ID=AD,Number=.,Type=Integer,Description=\"Total allelelic depths in order listed\">\n";
        header += "##INFO=<ID=AN,Number=.,Type=Integer,Description=\"Total number of alleles in order listed\">\n";
        header += "##INFO=<ID=AQ,Number=.,Type=Integer,Description=\"Average phred base quality for alleles in order listed\">\n";
        header += "##INFO=<ID=GN,Number=.,Type=Integer,Description=\"Number of taxa with genotypes AA,AB,BB or AA,AB,AC,BB,BC,CC if 2 alt alleles\">\n";
        header += "##INFO=<ID=HT,Number=1,Type=Integer,Description=\"Number of heterozygotes\">\n";
        header += "##INFO=<ID=EF,Number=1,Type=Float,Description=\"Ed factor\">\n";
        header += "##INFO=<ID=PV,Number=.,Type=Float,Description=\"p-value from segregation test between AB or AB, AC, BC if 2 alt alleles\">\n";
        
        header += "##ALT=<ID=DEL,Description=\"Deletion\">\n";
        header += "##ALT=<ID=INS,Description=\"Insertion\">\n";
        
        header += "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
        header += "##FORMAT=<ID=AD,Number=.,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n";
 
        //header += "##FILTER=<ID=LowQual,Description=\"Low quality\">\n";
        //header += "##FORMAT=<ID=PL,Number=3,Type=Float,Description=\"Normalized, Phred-scaled likelihoods for AA,AB,BB genotypes where A=ref and B=alt; not applicable if site is not biallelic\">\n";
        //header += "##INFO=<ID=AF,Number=.,Type=Float,Description=\"Allele Frequency, for each ALT allele, in the same order as listed\">\n";
        
        return header;
    }
    
    private static String VCFheaderFromFile(String VCFfileName)
    {
        String header = "";
        String line;
        try
        {
            BufferedReader headerfile = new BufferedReader(new FileReader(VCFfileName), 1000000);
            while((line=headerfile.readLine()) != null)
            {
                header += line + "\n";
            }
            headerfile.close();
        }
        catch(Exception e)
        {
            System.out.println("Error reading VCF header file "+VCFfileName+":" + e);
        }
        
        return header;
    }
    
    private static String addMAFtoInfo(String InfoIn)
    {
        String result = "";
        int i, j, aux;
        Pattern ptrn = Pattern.compile("GN=([\\d+,]+)");
        Matcher mchr = ptrn.matcher(InfoIn);
        mchr.find();
        //System.out.println("Group: "+mchr.group());
        String [] genonums = mchr.group().replaceAll("GN=", "").split(",");
        int allnum = genonums.length == 3 ? 2:3;  // Note: it is limited to 3 alleles
        int [] allcounts = new int[allnum];
        //Arrays.fill(allcounts, 0);
        int count = 0;
        int all_alleles_num = 0;
        for(i=0;i<allnum;i++)
        {
            for(j=i;j<allnum;j++)
            {
                aux = Integer.parseInt(genonums[count]);
                allcounts[i]+=aux;
                allcounts[j]+=aux;
                all_alleles_num += aux;
                count++;
            }
        }
        all_alleles_num *= 2;
        Arrays.sort(allcounts);
        //System.out.println("al_allele_nums "+all_alleles_num+" allcouts: "+allcounts[allnum-1]+ " "+ allcounts[allnum-2]);
        double maf = (double)(all_alleles_num-allcounts[allnum-1])/all_alleles_num;
        result = InfoIn + ";MAF=" + (double)Math.round(10000*maf)/10000;

        return result;
    }
    
}
