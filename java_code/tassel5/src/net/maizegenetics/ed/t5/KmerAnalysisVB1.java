package net.maizegenetics.ed.t5;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.primitives.UnsignedInts;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import net.maizegenetics.analysis.gbs.v2.GBSUtils;
import net.maizegenetics.dna.BaseEncoder;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.util.Utils;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.SubstitutionMatrixHelper;
import org.biojava.nbio.alignment.template.GapPenalty;
import org.biojava.nbio.alignment.template.SequencePair;
import org.biojava.nbio.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by edbuckler on 4/14/15.
 */
public class KmerAnalysisVB1 {

    public static final String b73GenomeFile = "/data1/HapMap4/assemblies/CML247_140810/tst2.fasta"; // "/data/genomes/maizev3/maize3.fa";
    public static final String[] genomeFiles = {
            b73GenomeFile,
           // "/data1/HapMap4/assemblies/CML247_140810/CML247_maize.fasta.gz",
            "/data1/HapMap4/assemblies/CML247_140810/tst2.fasta",
//        "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/W22_5.genome.fasta.gz",
//        "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/PH207_pseudochrom_140622.fasta.gz",
//            "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/Mo17_scaffolds_201403.fa.gz",
//        "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/B47_scaffolds.fa.gz",
//        "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/TIL01_MaSuRCA_contigs_v1.0.fasta.gz",
//        "/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/TIL11_MaSuRCA_contigs_v1.0.fasta.gz"
    };

    public static final String uniqueKmerMapFileName = "uniq32mer2TaxaMapX.ser";


    public static final String[] fastqFileName = {
            "4881_6665_17269_HFFGKADXX_CML247_ATCACG_R1.fastq.gz",
            "4881_6665_17269_HFFGKADXX_CML247_ATCACG_R2.fastq.gz"
            //"/Users/edbuckler/SolexaAnal/AGPv3/wholegenome/4881_6665_17270_HFFGKADXX_Va35_CGATGT_R1.fastq.gz"
    };


    //public static final int kmerLength = 32;
    public static int kmerLength = 25;


    //This is used to only create maps with a subset of all kmers
    public static final BiPredicate<byte[], Integer> rightKmerPrefix = (bb, i) -> {
        //if ((bb[i] == 0) && (bb[i + 1] == 2) && (bb[i + 2] == 3)) return true;  /AGT
        //if ((bb[i] == 2) && (bb[i + 1] == 1) && (bb[i + 2] == 3)) return true; // GCT
        if ((bb[i] == 0)) return true;
        return false;
    };
    
    public static final boolean rightLeftKmerPrefix( byte[] bb, int i, int kmerl)
    {
        // GCT in the beginning, or AGC at the end
        //boolean cond = (bb[i] == 2) && (bb[i + 1] == 1) && (bb[i + 2] == 3);
        //cond = cond || (bb[i + kmerl-3] == 0) && (bb[i + kmerl-2] == 2) && (bb[i + kmerl-1] ==1);
        
        boolean cond = (bb[i] == 2);
        cond = cond || (bb[i + kmerl-1] == 1);
        
        return cond;
    };
    
    // Test the first and the last element of subsequence for "N" allele
    // Return true if N found
    public static final boolean findNinByteSeq(byte [] bb, int i, int kmerl)
    {
        for(int j=i;j<i+kmerl;j++)
        {
            if(bb[j] == GenotypeTable.UNKNOWN_ALLELE) return true;
        }
        return false;
        //if(Arrays.binarySearch(bb, i, i+kmerl, GenotypeTable.UNKNOWN_ALLELE)>=0)
        //if(bb[i] == GenotypeTable.UNKNOWN_ALLELE || bb[i+kmerl-1] == GenotypeTable.UNKNOWN_ALLELE)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }
    
    public static int getCanonicalKmer(byte [] bb, int i, int kmerl)
    {
        byte baux;
        for(int j=i;j<i+kmerl;j++)
        {
            baux = (byte)(3-bb[2*i+kmerl-1-j]); // rhs is reverse complement of bb[j]
            if(bb[j] < baux)
            {
                // Return kmer on forward strand
                // return Arrays.copyOfRange(bb, i, i + kmerl);
                return 1;
            }
            else if(bb[j] > baux)
            {
                //return NucleotideAlignmentConstants.reverseComplementAlleleByteArray(Arrays.copyOfRange(bb, i, i + kmerl));
                return -1;
            }
        }
        // If not ended yet, return the forward kmer
        // return Arrays.copyOfRange(bb, i, i + kmerl);
        return 1;
    }
    
    public static final boolean rightKmerPrefixGeneral( byte[] bb, int i, byte [] prefs)
    {
        if(prefs[0] == -1) { return true; }
        
        boolean cond = (bb[i] == prefs[0]);
        for(int j=1;j<prefs.length;j++)
        {
            cond = cond && (bb[i + j] == prefs[j]);
        }
        return cond;
    };
    
    public static final byte [] decodePrefix(String prefix)
    {
        char [] cprefix = prefix.toCharArray();
        byte [] bprefix = new byte[cprefix.length];
        for(int i=0;i<cprefix.length;i++)
        {
            switch (cprefix[i]) 
            {
                case 'A':
                    bprefix[i] = 0;
                    break;
                case 'C':
                    bprefix[i] = 1;
                    break;
                case 'G':
                    bprefix[i] = 2;
                    break;
                case 'T':
                    bprefix[i] = 3;
                    break;
                case 'N':
                    bprefix[i] = -1;
            }
        }
        //return BaseEncoder.getByteSeqFromLong(BaseEncoder.getLongFromSeq(prefix));
        return bprefix;
    }


    public static void createUniqueKmerMap() {
        final int window = 200_000_000;
        //map is key  =long (upto 32 of sequence with 2 bit encoding)
        //value = int array of genome positions in each reference genome (unsigned ints are used - to get to 4 billion)
        //position 0 means not present in that genome
        TLongObjectMap<int[]> kmerMap = new TLongObjectHashMap<>(70_000_000);
        final int[] dupKmerArray = new int[0]; //shared object for duplicated kmers
        final int numberOfTaxa = genomeFiles.length;
        for (int taxa = 0; taxa < numberOfTaxa; taxa++) {
            System.out.printf("Taxa %d File: %s %n", taxa, genomeFiles[taxa]);
            GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(genomeFiles[taxa]);
            for (long start = 0; start < genomeSequence.genomeSize(); start += window) {
                System.out.printf("Subseq start %,d %n", start);
                byte[] subSeq = genomeSequence.genomeSequence(start, Math.min(genomeSequence.genomeSize(), start + window - 1));
                for (int i = 0; i < subSeq.length - kmerLength; i++) {
                    if (!rightKmerPrefix.test(subSeq, i)) continue;
                    //System.out.println("i = " + i);
                    long seqL = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(subSeq, i, i + kmerLength));
                    if (kmerMap.containsKey(seqL)) {
                        int[] positions = kmerMap.get(seqL);
                        if (positions == dupKmerArray) continue;  //already duplicated ignore
                        if (positions[taxa] != 0) {
                            positions = dupKmerArray; //duplicated tag set key-value to duplicated
                        } else {
                            positions[taxa] = (int) (start + (long) i);  //unique tag found in more than one taxa
                        }
                        kmerMap.put(seqL, positions);
                    } else {
                        int[] newPosition = new int[numberOfTaxa];
                        newPosition[taxa] = (int) (start + (long) i);
                        kmerMap.put(seqL, newPosition);
                    }
                }
            }
            System.out.printf("Taxa %d File: %s Complete %n", taxa, genomeFiles[taxa]);
            System.out.printf("kmerMap.size() = %,d%n", kmerMap.size());
            int dupCnt = 0, multiSingle = 0;
            for (long kmers : kmerMap.keys()) {
                int[] positions = kmerMap.get(kmers);
                if (kmerMap.get(kmers) == dupKmerArray) {
                    dupCnt++;
                } else if (IntStream.of(positions).filter(p -> p != 0).count() > 1) {
                    multiSingle++;
                }
            }
            System.out.printf("Duplicates %,d MultiSingle %,d%n", dupCnt, multiSingle);
        }
        for (long kmers : kmerMap.keys()) {
            if (kmerMap.get(kmers) == dupKmerArray) {
                kmerMap.remove(kmers);
            }
        }
        System.out.printf("Removed dups and now kmerMap.size() = %,d%n", kmerMap.size());
        saveGZipObject(kmerMap, uniqueKmerMapFileName);
    }

    public static void createUniqueKmerMapBothStrands() {
        final int window = 200_000_000;
        //final int window = 200;
        int strand;
        //map is key  =long (upto 32 of sequence with 2 bit encoding)
        //value = int array of genome positions in each reference genome (unsigned ints are used - to get to 4 billion)
        //position 0 means not present in that genome
        TLongObjectMap<int[]> kmerMap = new TLongObjectHashMap<>(70_000_000);
        final int[] dupKmerArray = new int[0]; //shared object for duplicated kmers
        final int numberOfTaxa = genomeFiles.length;
        
        ArrayList<Set<Chromosome>> chromsets = new ArrayList<Set<Chromosome>>();
        
        ArrayList<long []> cumChrLens = new ArrayList<long []>();
        ArrayList<String []> chrNamesL = new ArrayList<String []>();
        
        for (int taxa = 0; taxa < numberOfTaxa; taxa++) {
            System.out.printf("Taxa %d File: %s %n", taxa, genomeFiles[taxa]);
            GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(genomeFiles[taxa]);
            
            // Save set of chromosomes for this taxon
            chromsets.add(genomeSequence.chromosomes());
            
            Object [] chrs = genomeSequence.chromosomes().stream().sorted().toArray();
            long [] ltmp = new long[chrs.length];
            String [] chrNames = new String[chrs.length];;
            long incrlen = 0;
            for(int jj=0;jj<chrs.length;jj++)
            {
                 Chromosome cc = (Chromosome)chrs[jj];
                 incrlen += cc.getLength();
                 chrNames[jj] = cc.getName();
                 ltmp[jj] = incrlen;
            }
            cumChrLens.add(ltmp);
            chrNamesL.add(chrNames);
            
            for (long start = 0; start < genomeSequence.genomeSize(); start += window) {
                System.out.printf("Subseq start %,d %n", start);
                byte[] subSeq = genomeSequence.genomeSequence(start, Math.min(genomeSequence.genomeSize(), start + window - 1));
                for (int i = 0; i < subSeq.length - kmerLength; i++) {
                    if (!rightKmerPrefix.test(subSeq, i)) continue;
                    //System.out.println("i = " + i);
                    
                    // We need to create a reverse coplement of seqL and save the one that has smaller integer value
                    // Smaller integer from encoding defines the canonical order
                    byte [] aseqL = Arrays.copyOfRange(subSeq, i, i + kmerLength);
                    byte[] aseqLrc = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(aseqL);
                    long seqL = BaseEncoder.getLongSeqFromByteArray(aseqL);
                    long seqLrc = BaseEncoder.getLongSeqFromByteArray(aseqLrc);
         
                    //System.out.println("aseqL: "+ nucleotideBytetoString(aseqL) + ", aseqLrc: "+nucleotideBytetoString(aseqLrc));
                    //System.out.println("seqL, seqLrc: "+seqL + ", " + seqLrc);
                    strand = 1;
                    if(seqLrc < seqL) { seqL = seqLrc; strand = -1;}
                    if (kmerMap.containsKey(seqL)) {
                        int[] positions = kmerMap.get(seqL);
                        if (positions == dupKmerArray) continue;  //already duplicated ignore
                        if (positions[taxa] != 0) {
                            positions = dupKmerArray; //duplicated tag set key-value to duplicated
                        } else {
                            positions[taxa] = strand * (int) (start + (long) i);  //unique tag found in more than one taxa
                        }
                        kmerMap.put(seqL, positions);
                    } else {
                        int[] newPosition = new int[numberOfTaxa];
                        newPosition[taxa] = strand * (int) (start + (long) i);
                        kmerMap.put(seqL, newPosition);
                    }
                }
            }
            System.out.printf("Taxa %d File: %s Complete %n", taxa, genomeFiles[taxa]);
            System.out.printf("kmerMap.size() = %,d%n", kmerMap.size());
            int dupCnt = 0, multiSingle = 0;
            for (long kmers : kmerMap.keys()) {
                int[] positions = kmerMap.get(kmers);
                if (kmerMap.get(kmers) == dupKmerArray) {
                    dupCnt++;
                } else if (IntStream.of(positions).filter(p -> p != 0).count() > 1) {
                    multiSingle++;
                }
            }
            System.out.printf("Duplicates %,d MultiSingle %,d%n", dupCnt, multiSingle);
        }
        for (long kmers : kmerMap.keys()) {
            if (kmerMap.get(kmers) == dupKmerArray) {
                kmerMap.remove(kmers);
            }
        }
        System.out.printf("Removed dups and now kmerMap.size() = %,d%n", kmerMap.size());
        //saveGZipObject(kmerMap, uniqueKmerMapFileName);
        
        // Stupid print out of all common unique k-mers
        for (long kmers : kmerMap.keys()) 
        {
            //System.out.println("Processing kmer "+kmers);
            int [] positions = kmerMap.get(kmers);
            // Save only common k-mers
            if(positions[0] != 0 && positions[1] != 0)
            {
                //System.out.println("Kmer " + kmers + " is common");
                // Need to convert long representation of k-mer into string...
                String thiskmer = BaseEncoder.getSequenceFromLong(kmers);
                //System.out.println(thiskmer+"\t"+positions[0]+"\t"+ positions[1]);
                
                // Try to retrieve chromosomes and positions on the chromosomes for first taxon.
                
                long pos0 = 0;
                long pos1 = 0;
                int c0 = 0;
                int c1 = 0;
                String sc0="";
                String sc1="";
                long absp  = Math.abs(positions[0]);
                for(int jj=0;jj<cumChrLens.get(0).length;jj++)
                {
                    if(absp<=cumChrLens.get(0)[jj])
                    {
                        pos0 = absp;
                        if(jj>0)
                        {
                            pos0 = absp - cumChrLens.get(0)[jj-1];
                        }
                        c0 = jj;
                        sc0 = chrNamesL.get(0)[jj];
                        break;
                    }
                }
                        
                absp  = Math.abs(positions[1]);
                for(int jj=0;jj<cumChrLens.get(1).length;jj++)
                {
                    if(absp<=cumChrLens.get(1)[jj])
                    {
                        pos1 = absp;
                        if(jj>0)
                        {
                            pos1 = absp - cumChrLens.get(1)[jj-1];
                        }
                        c1 = jj;
                        sc1 = chrNamesL.get(1)[jj];
                        break;
                    }
                }
                
                
                System.out.println(thiskmer+"\t"+positions[0]+"\t"+ positions[1]+"\t"+sc0+"_"+pos0+"\t"+sc1+"_"+pos1);
          
       
            }
        }
    }
    
    public static void createUniqueKmerMapBothStrands1(String [] gfiles, int kmerl, String prefix, String outfilecore) {
        final int window = 200_000_000;
        //final int window = 200;
        int strand;
        //map is key  =long (upto 32 of sequence with 2 bit encoding)
        //value = int array of genome positions in each reference genome (unsigned ints are used - to get to 4 billion)
        //position 0 means not present in that genome
        TLongObjectMap<String[]> kmerMap = new TLongObjectHashMap<>(70_000_000);
        final String[] dupKmerArray = new String[0]; //shared object for duplicated kmers
        final int numberOfTaxa = gfiles.length;
        final String posSeparator = ":";
        kmerLength = kmerl;
        final byte [] bprefix = decodePrefix(prefix);
        
        System.out.println("Prefix length detected: " + bprefix.length + " "+prefix);
        
        ArrayList<HashMap<String,Integer>> achrLengths = new ArrayList<HashMap<String,Integer>>();
        for (int taxa = 0; taxa < numberOfTaxa; taxa++) {
            System.out.printf("Taxa %d File: %s %n", taxa, gfiles[taxa]);
            GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(gfiles[taxa]);
            
            Object [] chrs = genomeSequence.chromosomes().stream().sorted().toArray();
            String [] chrNames = new String[chrs.length];
            for(int jj=0;jj<chrs.length;jj++)
            {
                 chrNames[jj] = ((Chromosome)chrs[jj]).getName();
            }
            
            HashMap<String, Integer> chrLengths = new HashMap<String,Integer>();
            // Loop over chromosomes first
            for(int ichr=0;ichr<genomeSequence.numberOfChromosomes();ichr++)
            {
            long chrlen = genomeSequence.chromosomeSequence((Chromosome)chrs[ichr]).length;
            chrLengths.put(chrNames[ichr], (int)chrlen);
            //System.out.println("Doing chromosome " + ichr + " of length "+chrlen);
            // Note: chromosome positions start from 1
            for (long start = 1; start <= chrlen; start += window) {
                //System.out.printf("Subseq start %,d %n", start);
                //byte[] subSeq = genomeSequence.genomeSequence(start, Math.min(genomeSequence.genomeSize(), start + window - 1));
                byte[] subSeq = genomeSequence.chromosomeSequence((Chromosome)chrs[ichr], (int)start, Math.min((int)chrlen,(int)(start + window-1)));
                for (int i = 0; i <= subSeq.length - kmerLength; i++) {
                    
                    // Select only kmers with certain starting bases
                    //if (!rightKmerPrefix.test(subSeq, i)) continue;
                    
                    // Select only kmers with certain starting bases or complementary ending bases
                    //if(!rightLeftKmerPrefix(subSeq,i,kmerLength)) continue;
                    
                    // Skip kmers containing stretches of N alleles
                    if(findNinByteSeq(subSeq,i,kmerLength)) continue;
                    
                    // is kmer of its rvc the canonical one?
                    strand = getCanonicalKmer(subSeq,i,kmerLength);
                    //strand = 1;
                   
                    // We need to create a reverse complement of seqL and save the one that has smaller integer value
                    // Smaller integer from encoding defines the canonical order
                    byte [] aseqL;
                    if(strand==1)
                    {
                        aseqL = Arrays.copyOfRange(subSeq, i, i + kmerLength);
                    }
                    else
                    {
                        aseqL = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(Arrays.copyOfRange(subSeq, i, i + kmerLength));
                    }
                    
                    // Debug:
                    /*
                    String strKmer = nucleotideBytetoString(aseqL);
                    if(strKmer.equals("CGGCCGACGGGCCGAGCAAGAGG"))
                    {
                        System.out.println("DEBUG kemr strKmer found");
                    }
                    */
                    // end debug
                    
                    // Skip if restriction not satisifed; otherwise continue with encoding and analysis
                    //if (!rightKmerPrefix.test(aseqL, 0)) continue;
                    if (!rightKmerPrefixGeneral(aseqL, 0, bprefix)) continue;
                    
                    long seqL = BaseEncoder.getLongSeqFromByteArray(aseqL);
                    
                    //byte[] aseqLrc = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(aseqL);
                    //long seqLrc = BaseEncoder.getLongSeqFromByteArray(aseqLrc);
                   
                    //System.out.println("aseqL: "+ nucleotideBytetoString(aseqL) + ", aseqLrc: "+nucleotideBytetoString(aseqLrc));
                    //System.out.println("seqL, seqLrc: "+seqL + ", " + seqLrc);
                    /*
                    strand = 1;
                    if(seqLrc < seqL) 
                    { 
                        seqL = seqLrc; strand = -1;
                        if (!rightKmerPrefix.test(aseqLrc, 0)) continue;
                    }
                    else
                    {
                        if (!rightKmerPrefix.test(aseqL, 0)) continue;
                    }
                    */
                    
                    if (kmerMap.containsKey(seqL)) {
                        String[] positions = kmerMap.get(seqL);
                        if (positions == dupKmerArray) continue;  //already duplicated ignore
                        if (!positions[taxa].isEmpty()) {
                            positions = dupKmerArray; //duplicated tag set key-value to duplicated
                        } else {
                            positions[taxa] = chrNames[ichr] + posSeparator + String.valueOf(strand * (int) (start + (long) i));  //unique tag found in more than one taxa
                        }
                        kmerMap.put(seqL, positions);
                    } else {
                        String[] newPosition = new String[numberOfTaxa];
                        Arrays.fill(newPosition, "");
                        newPosition[taxa] = chrNames[ichr] + posSeparator + String.valueOf(strand * (int) (start + (long) i));
                        kmerMap.put(seqL, newPosition);
                    }
                }
            }
            } // end loop over chromosomes
            achrLengths.add(chrLengths);
            
            System.out.printf("Taxa %d File: %s Complete %n", taxa, gfiles[taxa]);
            System.out.printf("kmerMap.size() = %,d%n", kmerMap.size());
            int dupCnt = 0, multiSingle = 0;
            for (long kmers : kmerMap.keys()) {
                String [] positions = kmerMap.get(kmers);
                if (kmerMap.get(kmers) == dupKmerArray) {
                    dupCnt++;
                } 
                else 
                {
                    int kmerintaxa = 0;
                    for(int it=0;it<numberOfTaxa;it++)
                    {
                        if(!positions[it].isEmpty()) { kmerintaxa++; }
                    }
                    if(kmerintaxa>1) { multiSingle++; }
                }
            }
            System.out.printf("Duplicates %,d MultiSingle %,d%n", dupCnt, multiSingle);
        }
        for (long kmers : kmerMap.keys()) {
            if (kmerMap.get(kmers) == dupKmerArray) {
                kmerMap.remove(kmers);
            }
        }
        System.out.printf("Removed dups and now kmerMap.size() = %,d%n", kmerMap.size());
        //saveGZipObject(kmerMap, uniqueKmerMapFileName);
        
        // Stupid print out of all unique (over all assemblies) k-mers common to the first and second assembly
        BufferedWriter bw=null;
        try
        {
        bw = new BufferedWriter(new FileWriter(outfilecore+".seg"));
        
        String [][] posPrint = new String [numberOfTaxa][2];
        int [] strands = new int[numberOfTaxa];
        String relStrand;
        int spos1,spos2,epos1,epos2;
        for (long kmers : kmerMap.keys()) 
        {
            //System.out.println("Processing kmer "+kmers);
            String [] positions = kmerMap.get(kmers);
            // Save only common k-mers
            if(!positions[0].isEmpty() && !positions[1].isEmpty())
            {
                // Convert long representation of k-mer into string...
                String thiskmer = BaseEncoder.getSequenceFromLong(kmers);
                
                // Beautify the printout
                for(int it=0;it<2;it++)
                {
                    posPrint[it] = positions[it].split(posSeparator);
                    strands[it] = 1;
                    if(posPrint[it][1].contains("-"))
                    {
                        strands[it] = -1;
                        posPrint[it][1] = posPrint[it][1].replaceAll("-", "");
                    }
                }
                relStrand = strands[0] != strands[1]?"-":"+";
                //relStrand = strands[1] == 1?"+":"-";
                
                spos1 = Integer.parseInt(posPrint[0][1]);
                epos1 = spos1 + kmerLength -1;
                // If kmer present in query on opposite strand, position needs to be counted from
                // 5' end of the opposite strand
                spos2 = Integer.parseInt(posPrint[1][1]);
                if(relStrand.equals("-"))
                {
                    spos2 = achrLengths.get(1).get(posPrint[1][0]) - spos2- kmerLength + 2;
                }
                epos2 = spos2 + kmerLength -1;
                
                //System.out.println(thiskmer.substring(0,kmerLength-1)+"\t"+positions[0]+"\t"+ positions[1]);
               
                bw.write(posPrint[0][0]+" "+ spos1 + " " + epos1 + 
                        " "+ posPrint[1][0] + " " + spos2 + " " + epos2 + " " + relStrand + " #" +
                        thiskmer.substring(0,kmerLength) + " " + strands[0] + " " + strands[1] + "\n");
       
       
            }
        }
        bw.close();
        
        // Dump all K-mers for all assemblies to different files
        // Not too optimal, but clear
        /*
        for(int it=0;it<numberOfTaxa;it++)
        {
            bw = new BufferedWriter(new FileWriter(outfilecore+"."+String.valueOf(it)+".seg"));
            for (long kmers : kmerMap.keys()) 
            {
                String [] positions = kmerMap.get(kmers);
                if(!positions[it].isEmpty())
                {
                    posPrint[0] = positions[it].split(posSeparator);
                    if(posPrint[0][1].contains("-"))
                    {
                        posPrint[0][1] = posPrint[0][1].replaceAll("-", "");
                    }
                    spos1 = Integer.parseInt(posPrint[0][1]);
                    epos1 = spos1 + kmerLength -1;
                    bw.write(posPrint[0][0]+" "+ spos1 + " " + epos1 + "\n");
                }
            }
            bw.close();
        }
        // End of per taxon K-mer dumping
        */
        }
        catch(Exception e)
        {
            System.out.println("output file could not be open or written"+e);
        }
    }
    
    // scan the target (taxa=0) on forward strand, then look for matches on query (taxa=1)
    // on both strands
    public static void createUniqueKmerMapBothStrands2(String [] gfiles, int kmerl, String prefix, String outfilecore) {
        final int window = 200_000_000;
        //final int window = 200;
        int strand;
        //map is key  =long (upto 32 of sequence with 2 bit encoding)
        //value = int array of genome positions in each reference genome (unsigned ints are used - to get to 4 billion)
        //position 0 means not present in that genome
        TLongObjectMap<String[]> kmerMap = new TLongObjectHashMap<>(70_000_000);
        final String[] dupKmerArray = new String[0]; //shared object for duplicated kmers
        final int numberOfTaxa = gfiles.length;
        final String posSeparator = "_";
        kmerLength = kmerl;
        final byte [] bprefix = decodePrefix(prefix);
        
        System.out.println("Prefix length detected: " + bprefix.length + " "+prefix);
        
        ArrayList<HashMap<String,Integer>> achrLengths = new ArrayList<HashMap<String,Integer>>();
        for (int taxa = 0; taxa < numberOfTaxa; taxa++) {
            System.out.printf("Taxa %d File: %s %n", taxa, gfiles[taxa]);
            GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(gfiles[taxa]);
            
            Object [] chrs = genomeSequence.chromosomes().stream().sorted().toArray();
            String [] chrNames = new String[chrs.length];
            for(int jj=0;jj<chrs.length;jj++)
            {
                 chrNames[jj] = ((Chromosome)chrs[jj]).getName();
            }
            
            HashMap<String, Integer> chrLengths = new HashMap<String,Integer>();
            // Loop over chromosomes first
            for(int ichr=0;ichr<genomeSequence.numberOfChromosomes();ichr++)
            {
            long chrlen = genomeSequence.chromosomeSequence((Chromosome)chrs[ichr]).length;
            chrLengths.put(chrNames[ichr], (int)chrlen);
            //System.out.println("Doing chromosome " + ichr + " of length "+chrlen);
            // Note: chromosome positions start from 1
            for (long start = 1; start <= chrlen; start += window) {
                //System.out.printf("Subseq start %,d %n", start);
                //byte[] subSeq = genomeSequence.genomeSequence(start, Math.min(genomeSequence.genomeSize(), start + window - 1));
                byte[] subSeq = genomeSequence.chromosomeSequence((Chromosome)chrs[ichr], (int)start, Math.min((int)chrlen,(int)(start + window-1)));
                for (int i = 0; i < subSeq.length - kmerLength; i++) {
                    
                    // Skip kmers containing stretches of N alleles
                    if(findNinByteSeq(subSeq,i,kmerLength)) continue;
                    
                    // is kmer of its rvc the canonical one?
                    //strand = getCanonicalKmer(subSeq,i,kmerLength);
                    strand = 1;
                   
                    // Forward strand - done for both taxa, unless kmer not from requested subset
                    byte [] aseqL;
                    long seqL;
                    aseqL = Arrays.copyOfRange(subSeq, i, i + kmerLength);
                    if (rightKmerPrefixGeneral(aseqL, 0, bprefix))
                    {
                         seqL = BaseEncoder.getLongSeqFromByteArray(aseqL);
                        if (kmerMap.containsKey(seqL)) {
                        String[] positions = kmerMap.get(seqL);
                        if (positions != dupKmerArray)  //if already duplicated - ignore
                        {
                            if (!positions[taxa].isEmpty()) {
                                positions = dupKmerArray; //duplicated tag set key-value to duplicated
                            } else {
                                positions[taxa] = chrNames[ichr] + posSeparator + String.valueOf(strand * (int) (start + (long) i));  //unique tag found in more than one taxa
                            }
                            kmerMap.put(seqL, positions);
                        }
                        } else {
                            if(taxa == 0)
                            {
                                String[] newPosition = new String[numberOfTaxa];
                                Arrays.fill(newPosition, "");
                                newPosition[taxa] = chrNames[ichr] + posSeparator + String.valueOf(strand * (int) (start + (long) i));
                                kmerMap.put(seqL, newPosition);
                            }
                        }
                    }
                    
                    if(taxa == 0) { continue; }
                    
                    // For the second taxon, proceed checking the reverse complement
                    byte [] aseqLrc;
                    aseqLrc = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(aseqL);
                    strand = -1;
                    
                    // Skip if restriction not satisifed; otherwise continue with encoding and analysis
                    
                    if (rightKmerPrefixGeneral(aseqLrc, 0, bprefix))
                    {
                        seqL = BaseEncoder.getLongSeqFromByteArray(aseqLrc);
 
                        if (kmerMap.containsKey(seqL)) {
                            String[] positions = kmerMap.get(seqL);
                            if (positions != dupKmerArray)
                            {//already duplicated ignore
                                if (!positions[taxa].isEmpty()) {
                                    positions = dupKmerArray; //duplicated tag set key-value to duplicated
                                } else {
                                    positions[taxa] = chrNames[ichr] + posSeparator + String.valueOf(strand * (int) (start + (long) i));  //unique tag found in more than one taxa
                                }
                                kmerMap.put(seqL, positions);
                            }
                        } 
                    }
                }
            }
            } // end loop over chromosomes
            achrLengths.add(chrLengths);
            
            System.out.printf("Taxa %d File: %s Complete %n", taxa, gfiles[taxa]);
            System.out.printf("kmerMap.size() = %,d%n", kmerMap.size());
            int dupCnt = 0, multiSingle = 0;
            for (long kmers : kmerMap.keys()) {
                String [] positions = kmerMap.get(kmers);
                if (kmerMap.get(kmers) == dupKmerArray) {
                    dupCnt++;
                } 
                else 
                {
                    int kmerintaxa = 0;
                    for(int it=0;it<numberOfTaxa;it++)
                    {
                        if(!positions[it].isEmpty()) { kmerintaxa++; }
                    }
                    if(kmerintaxa>1) { multiSingle++; }
                }
            }
            System.out.printf("Duplicates %,d MultiSingle %,d%n", dupCnt, multiSingle);
        }
        for (long kmers : kmerMap.keys()) {
            if (kmerMap.get(kmers) == dupKmerArray) {
                kmerMap.remove(kmers);
            }
        }
        System.out.printf("Removed dups and now kmerMap.size() = %,d%n", kmerMap.size());
        //saveGZipObject(kmerMap, uniqueKmerMapFileName);
        
        // Stupid print out of all common unique k-mers
        BufferedWriter bw=null;
        try
        {
        bw = new BufferedWriter(new FileWriter(outfilecore+".seg"));
        
        String [][] posPrint = new String [numberOfTaxa][2];
        int [] strands = new int[numberOfTaxa];
        String relStrand;
        int spos1,spos2,epos1,epos2;
        for (long kmers : kmerMap.keys()) 
        {
            //System.out.println("Processing kmer "+kmers);
            String [] positions = kmerMap.get(kmers);
            // Save only common k-mers
            if(!positions[0].isEmpty() && !positions[1].isEmpty())
            {
                // Convert long representation of k-mer into string...
                String thiskmer = BaseEncoder.getSequenceFromLong(kmers);
                
                // Beautify the printout
                for(int it=0;it<numberOfTaxa;it++)
                {
                    posPrint[it] = positions[it].split(posSeparator);
                    strands[it] = 1;
                    if(posPrint[it][1].contains("-"))
                    {
                        strands[it] = -1;
                        posPrint[it][1] = posPrint[it][1].replaceAll("-", "");
                    }
                }
                relStrand = strands[0] != strands[1]?"-":"+";
                //relStrand = strands[1] == 1?"+":"-";
                
                spos1 = Integer.parseInt(posPrint[0][1]);
                epos1 = spos1 + kmerLength -1;
                // If kmer present in query on opposite strand, position needs to be counted from
                // 5' end of the opposite strand
                spos2 = Integer.parseInt(posPrint[1][1]);
                if(relStrand.equals("-"))
                {
                    spos2 = achrLengths.get(1).get(posPrint[1][0]) - spos2- kmerLength + 2;
                }
                epos2 = spos2 + kmerLength -1;
                
                //System.out.println(thiskmer.substring(0,kmerLength-1)+"\t"+positions[0]+"\t"+ positions[1]);
               
                bw.write(posPrint[0][0]+" "+ spos1 + " " + epos1 + 
                        " "+ posPrint[1][0] + " " + spos2 + " " + epos2 + " " + relStrand + " #" +
                        thiskmer.substring(0,kmerLength) + " " + strands[0] + " " + strands[1] + "\n");
       
       
            }
        }
        bw.close();
        }
        catch(Exception e)
        {
            System.out.println("output file could not be open or written"+e);
        }
    }
    
    private static void processFastQ(String fastqFileString, TLongObjectMap uniqueKmers, GenomeSequence genSeq) {
        boolean debug = false;
        Path fastqFile = Paths.get(fastqFileString);
        int allReads = 0, lowQualityReads = 0;
        try {
            int qualityScoreBase = GBSUtils.determineQualityScoreBase(fastqFile);
            int[] bins = new int[102];
            BufferedReader br = Utils.getBufferedReader(fastqFile.toString(), 1 << 22);
            long time = System.nanoTime();
            String[] seqAndQual;
            LongAdder totalHits = new LongAdder();
            LongAdder readsHit = new LongAdder();
            LongAdder totalReads = new LongAdder();
            double totalIdentity = 0;
            long totalAlignments = 0;
            int b73 = 0, cml247 = 0;
            GapPenalty penalty = new SimpleGapPenalty();
            penalty.setOpenPenalty((short) 10);
            penalty.setExtensionPenalty((short) 2);
            SubstitutionMatrix<NucleotideCompound> matrix = SubstitutionMatrixHelper.getNuc4_4();
            while ((seqAndQual = GBSUtils.readFastQBlock(br, allReads)) != null) {
                totalReads.increment();
                byte[] stringToByte = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(seqAndQual[0]);
                //stringToByte=Arrays.copyOf(stringToByte,125);

                Multimap<Integer, Long> hits = HashMultimap.create();
                hits.putAll(scanStringForHit(stringToByte, uniqueKmers));

                //reverse complement hits are currently excluded as code below needs to swap direction
                byte[] revComp = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(stringToByte);
                //hits.putAll(scanStringForHit(revComp,uniqueKmers));

                totalHits.add(hits.size());
                if (hits.size() > 0) {
                    readsHit.increment();
                    b73 += hits.get(0).size();
                    cml247 += hits.get(1).size();
                    //System.out.println("hits = " + hits.toString());
                    if (hits.get(0).size() < 1) continue;//
                    if (debug) System.out.println("hits = " + hits.toString());
                    Long genStart = hits.get(0).stream().mapToLong(l -> l).min().getAsLong() - 150;
                    Long genEnd = genStart + 300;
                    //Long genEnd=hits.get(0).stream().mapToLong(l -> l).max().getAsLong()+100;


                    DNASequence genDNASeq = new DNASequence(nucleotideBytetoString(genSeq.genomeSequence(genStart, genEnd)));
                    DNASequence readSeq = new DNASequence(seqAndQual[0]);
                    SequencePair<DNASequence, NucleotideCompound> pair = Alignments.getPairwiseAlignment(genDNASeq, readSeq,
                            Alignments.PairwiseSequenceAlignerType.LOCAL, penalty, matrix);
                    //System.out.println(pair.getLength());
                    long gaps = IntStream.rangeClosed(1, pair.getLength()).filter(pair::hasGap).count();
                    double identity = (double) pair.getNumIdenticals() / (double) pair.getLength();
                    double snpIdentity = (double) pair.getNumIdenticals() / (double) (1 + pair.getLength() - gaps);
                    bins[(int) (snpIdentity * 100)]++;
                    totalIdentity += identity;
                    totalAlignments += 1.0;
                    if (totalReads.intValue() % 1_000 == 0) debug = true;
                    if (debug) System.out.println("B73 :" + genDNASeq.getSequenceAsString());
                    if (debug) System.out.println("Read:" + readSeq.getSequenceAsString());
                    if (debug) System.out.println("Alignment\n" + pair.toString());
                    if (debug) System.out.println("pair.getNumIdenticals() = " + pair.getNumIdenticals());
                    if (debug) System.out.println("pair.getLength() = " + pair.getLength());
                    if (debug) System.out.printf("identity:%.3g snpidentity: %.3g %n", identity, snpIdentity);
                    if (debug) System.out.println("-----------------------");
                    debug = false;

                }

                if (totalReads.intValue() % 1_000_000 == 0) {
                    System.out.printf("totalReads %,d totalHits:%,d totalReadsHit:%,d \n",
                            totalReads.intValue(), totalHits.intValue(), readsHit.intValue());
                }

                if (totalReads.intValue() % 100_000 == 0) {
                    double avgIdentity = totalIdentity / totalAlignments;
                    System.out.printf("totalReads %,d totalHits:%,d totalReadsHit:%,d avgIdentity:%,g \n",
                            totalReads.intValue(), totalHits.intValue(), readsHit.intValue(), avgIdentity);
                    System.out.println("bins = " + Arrays.toString(bins));
                }
            }
            System.out.printf("totalReads %,d totalHits:%,d totalReadsHit:%,d \n",
                    totalReads.intValue(), totalHits.intValue(), readsHit.intValue());
            System.out.printf("B73: %,d CML247:%,d \n", b73, cml247);
            System.out.println("Summary for " + fastqFile.toString() + "\n" +
                    "Total number of reads in lane=" + totalReads.intValue() + "\n" +
                    "Total number of low quality reads=" + lowQualityReads + "\n" +
                    "Timing process (sorting, collapsing, and writing TagCount to file)." + "\n" +
                    "Process took " + (System.nanoTime() - time) / 1e6 + " milliseconds.");
            br.close();
        } catch (StringIndexOutOfBoundsException oobe) {
            throw oobe; // pass it up to print error and stop processing
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private static String nucleotideBytetoString(byte[] b) {
        StringBuilder result = new StringBuilder();
        for (int index = 0; index < b.length; index++) {
            result.append(NucleotideAlignmentConstants.getNucleotideAlleleValue(b[index]));
        }
        return result.toString();
    }

    private static Multimap<Integer, Long> scanStringForHit(byte[] seqBytes, TLongObjectMap kmerSet) {
        Multimap<Integer, Long> hits = HashMultimap.create();
        for (int i = 0; i < seqBytes.length - kmerLength; i++) {
            if (!rightKmerPrefix.test(seqBytes, i)) continue;
            long seqL = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(seqBytes, i, i + kmerLength));
            int[] positions = (int[]) kmerSet.get(seqL);
            if (positions != null) {
                for (int taxon = 0; taxon < positions.length; taxon++) {
                    if (positions[taxon] != 0) hits.put(taxon, UnsignedInts.toLong(positions[taxon]));
                }
            }
        }
        return hits;
    }

    public static void main(String[] args) {

        // Assumer arguments: kmer lenght, genomefile1, genomefile2, genomefile3, ...
        int kmerl = Integer.parseInt(args[0]);
        String prefix = args[1];
        String outfilecore = args[2];
        // The rest of argements are genome fasta files
        createUniqueKmerMapBothStrands1(Arrays.copyOfRange(args, 3, args.length),kmerl,prefix,outfilecore);
        
              //create the map of unique kmers
        //only needs to be run once
        //createUniqueKmerMap();  
        //System.out.println("Loading map");
        //TLongObjectMap<int[]> kmerMap = (TLongObjectHashMap<int[]>) loadGZipObject(uniqueKmerMapFileName);
        //System.out.println("Loading sequences");
        //GenomeSequence gs = GenomeSequenceBuilder.instance(genomeFiles[0]);
        //System.out.println("Processing the reads");
        //currently only testing against on genome, but eventually this would be all references genomes.
        //processFastQ(fastqFileName[0], kmerMap, gs);


    }


    /**
     * Serialize an Object through a GZIPOutputStream .
     *
     * @param vlo      The object to be serialized .
     * @param fileName file where the object will be serialized .
     */
    public static void saveGZipObject(Object vlo, String fileName) {
        FileOutputStream fos = null;
        GZIPOutputStream gos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(fileName);
            gos = new GZIPOutputStream(fos);
            oos = new ObjectOutputStream(gos);
            oos.writeObject(vlo);
            oos.flush();
            oos.close();
            gos.close();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static Object loadGZipObject(String fileName) {
        Object obj = null;
        FileInputStream fis = null;
        GZIPInputStream gis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(fileName);
            gis = new GZIPInputStream(fis);
            ois = new ObjectInputStream(gis);
            obj = ois.readObject();
            ois.close();
            gis.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }
        return obj;
    }
}
