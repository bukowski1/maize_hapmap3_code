/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.maizegenetics.ed.t5;

import net.maizegenetics.analysis.imputation.LDKNNiImputationHetV2Plugin;
import net.maizegenetics.dna.snp.*;
import net.maizegenetics.plugindef.*;


/**
 *
 * @author bukowski
 */
public class Imputation {
    
    private static void imputeAllKnownSitesV2(String highCovFile, String outfile, int maxCores, String stableSitesFile) {
        System.out.println("File read - starting imputation");
        DataSet inputDataSet=ImportUtils.readDataSet(highCovFile);
        GenotypeTable impGenotype = new LDKNNiImputationHetV2Plugin()
                .highLDSSites(70)
                .knnTaxa(30)
                .maxDistance(600_000)
                .duplicateHetsThreshold(0.03)
                .maxDistanceFromNN(0.1)
                .maxCores(maxCores)
                .comparSiteFile(stableSitesFile)
                .runPlugin(inputDataSet)
                ;
        ExportUtils.writeToVCF(impGenotype,outfile,true);
    }

    
    public static void main(String[] args) {

        String stableSitesFile = "";
        if(args.length > 3) { stableSitesFile = args[3]; }
        imputeAllKnownSitesV2(args[0],args[1],Integer.parseInt(args[2]), stableSitesFile);


    }
}
