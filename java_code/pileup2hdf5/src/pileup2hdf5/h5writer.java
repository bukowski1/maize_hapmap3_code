/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.util.Hashtable;
import java.util.regex.Pattern;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;

/**
 *
 * @author rb299
 */
public class h5writer implements Runnable {
    private int fileID;
    private int sampleID;
    private int chrGID;
    Integer write_block_size=100000;  //write write_block_size number of sites at a time. As java limit the writing block size to 2g
    private Hashtable <Integer, Hashtable<String, Integer>>dataset2status = new Hashtable <Integer, Hashtable<String, Integer>>();
    //private byte[][] seq;
    private int [][] seq;
    private static Pattern WHITESPACE_PATTERN = Pattern.compile("\t");   
    private int indexinblock;
    private int chrlen;
    private BufferedReader isr = null;
    private boolean isbyte = true;
    private double logbase = 1.0746;  // logbase^128 = 10,000
    private double logconv;
    
    public h5writer(PipedInputStream pis, String outfilename, String samplename, String chrom, int chrlenIN, int gversion)
    {
        // chrlen = h5writereference.chromosome_lengths(gversion).get(chrom);
        chrlen = chrlenIN;
        try {
            java.io.File f = new java.io.File(outfilename);
            if(f.exists())
            {
               IHDF5Reader reader = HDF5Factory.openForReading(outfilename);
               
            //get/confirm sample name
            java.util.List<java.lang.String> groups = reader.getGroupMembers("/");
            String sampleinfile = groups.get(0);
            if(! sampleinfile.equals(samplename))
            {
                System.out.println("WARNING: sample names NOT the same: requested: " + samplename + " in H5 file: " + sampleinfile);
            }
            groups = reader.getGroupMembers("/"+sampleinfile);
            reader.close();
            
            // Now open the file for writing
                fileID = H5.H5Fopen(outfilename, HDF5Constants.H5F_ACC_RDWR,HDF5Constants.H5P_DEFAULT);
                sampleID = H5.H5Gopen(fileID,samplename);
                if(groups.indexOf(chrom) >= 0)
                {
                    System.out.println("WARNING: chromosome group: "+chrom + " already exists - will be overwritten");
                    H5.H5Ldelete(sampleID, chrom, HDF5Constants.H5P_DEFAULT);
                }
                this.create_chrgroup(chrom);
                //chrGID = H5.H5Gopen(sampleID, chrom);
                System.out.println("New GID: " + chrGID);
            }
            else
            {
                System.out.println("Creating a new H5 file "+outfilename + " with sample "+ samplename
                         + " chromosome " + chrom + " of length " + chrlen);
                fileID = H5.H5Fcreate(outfilename, HDF5Constants.H5F_ACC_TRUNC,
                    HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
                this.create_sample_group(samplename, samplename);
                this.create_chrgroup(chrom);
            }
        }
                    catch (HDF5Exception ex) {
        }
        
        // If input stream is null, it will read directly from the file
        // using first overload of loadMpileupFile
        if(pis != null)
        {
            isr = new BufferedReader(new InputStreamReader(pis));
        }
        
        logconv = 1.0/Math.log(logbase);
    }
    
    @Override
    public void run()
    {
        loadMpileupFile();
        closeFile();
    }
    
    public void loadMpileupFile(String infileName)
    {
        InputStreamReader isreader = null;
        try
        {
            if(! infileName.equals("-"))
            {
                isr = new BufferedReader(new FileReader(infileName), 1000000);
            }
            else
            {
                isreader = new InputStreamReader(System.in);
                isr = new BufferedReader(isreader);
            }   
        }
        catch(Exception e)
        {
            
        }
        loadMpileupFile();
    }
    
    public void loadMpileupFile() 
     {
         // length of the chromosome. Will keep alls ites even if coverage zero.
         int numsites = chrlen;
         int numalleles = 6;
         int curpos = 0;
        
        try
        {
            int datasetID = this.create_dataset(chrGID, numsites, numalleles);
            seq = new int[numalleles][write_block_size];
            System.out.println("waiting for first input");
            String fileInLine = isr.readLine();
            System.out.println("got first input");
            String[] s;
            
            // write the matrix in chunks, padding missing positions with zeroes
            indexinblock = 0;
            int startpos = 1;
            curpos = startpos;
            int endpos = numsites;
            int pos = 0;
            while (fileInLine != null && curpos <= numsites) 
            {
                s = WHITESPACE_PATTERN.split(fileInLine);
                //pos = Integer.parseInt(s[1]);
                pos = Integer.parseInt(s[0]);
                if(pos < startpos) { continue; }
                if(pos > endpos) { break; }
                if(pos > curpos)
                {
                    for(int j=0;j< pos - curpos;j++)
                    {
                        //for(int i=3; i< numalleles+3; i++)
                        for(int i=1; i< numalleles+1; i++)
                        {
                            //seq[i-3][indexinblock] = 0;
                            seq[i-1][indexinblock] = 0;
                        }
                        flush_matrix(datasetID,numalleles);
                    }
                }
                //for (int i = 3; i < numalleles+3; i++) 
                for (int i = 1; i < numalleles+1; i++) 
                {
                        //seq[i-3][indexinblock] = Integer.parseInt(s[i]);
                        seq[i-1][indexinblock] = Integer.parseInt(s[i]);
                }
                curpos = pos + 1;
                flush_matrix(datasetID,numalleles);
                fileInLine = isr.readLine();
            }
            isr.close();
            
            // Pad the rest of the matrix with zeroes, if needed
            //System.out.println("curpos, endpos, indexinblock: " + curpos + " " + endpos + " " + indexinblock);
            if(curpos <= endpos)
            {
                for(int j=0;j<= endpos - curpos;j++)
                {
                   //for(int i=3; i< numalleles+3; i++)
                   for(int i=1; i< numalleles+1; i++)
                   {
                        //seq[i-3][indexinblock] = 0;
                        seq[i-1][indexinblock] = 0;
                   }
                   flush_matrix(datasetID,numalleles);
                   //System.out.println("indexinblock after flush in last loop: " + indexinblock);
                }
            }
            
            // Write the rest of the matrix, if any
            //System.out.println("indexinblock before final flush: " + indexinblock);
            if(indexinblock >0)
            {
                this.write_dataset_chunk(datasetID, indexinblock);
            }
            
            
            this.close_dataset(datasetID);
        }
        catch(Exception e)
        {
            System.err.println("Error in processing mpileup extract file at position "+curpos+": " + e);
            e.printStackTrace();
        }
     }
    
    private void flush_matrix(int datasetID, int numAlleles)
    {
        if(indexinblock >= (write_block_size-1))
        {
            this.write_dataset_chunk(datasetID, write_block_size);
            indexinblock = 0;
            seq= null;
            System.gc();
            seq = new int[numAlleles][write_block_size];
         }
         else
         {
            indexinblock++;
         }
    }
    
    public void write_dataset_chunk(int datasetID, int numSitesInBlock)
    {
        //get dataset status of the tf dataset
        
        int numAlleles = 6;
        int lastupboundary = dataset2status.get(datasetID).get("lastupboundary");
        
        
        int  DataTypeID, FileSpaceID,  MemorySpaceID;
        
        FileSpaceID = H5.H5Dget_space (datasetID);
        long[] count= new long[] {numAlleles, numSitesInBlock};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, lastupboundary}, null, count, null);
        
        if(isbyte)
        {
            DataTypeID = HDF5Constants.H5T_NATIVE_SCHAR;
        
            //seq array might have some empty rows in the ends
            // not sure how this copyOf works (and Favadoc is confusing)
            //int[][] seq_pf = Arrays.copyOf(seq, numAlleles);
            byte [][] seq_pf_byte = new byte[numAlleles][numSitesInBlock];
            //seq_pf = Arrays.copyOf(seq,numSitesInBlock);
            // the only way I can get this to work is via a loop
            for(int i=0;i<numSitesInBlock;i++)
            {
                for(int j=0;j<6;j++)
                {
                    if(seq[j][i] <= 127)
                    {
                        seq_pf_byte[j][i] = (byte)seq[j][i];
                    }
                    else
                    {
                        int aux = (int)(-logconv*Math.log(seq[j][i]));
                        if(aux < -128) { aux = -127; }
                        seq_pf_byte[j][i] = (byte)aux;
                    }
                }
            }
   
            //write the position-fast orientation
            H5.H5Dwrite(datasetID, DataTypeID, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, seq_pf_byte);
            seq_pf_byte = null;
        }
        else
        {
            DataTypeID = HDF5Constants.H5T_NATIVE_INT;
        
            //seq array might have some empty rows in the ends
            // not sure how this copyOf works (and Favadoc is confusing)
            //int[][] seq_pf = Arrays.copyOf(seq, numAlleles);
            int [][] seq_pf = new int[numAlleles][numSitesInBlock];
            //seq_pf = Arrays.copyOf(seq,numSitesInBlock);
            // the only way I can get this to work is via a loop
            for(int i=0;i<numSitesInBlock;i++)
            {
                for(int j=0;j<6;j++)
                {
                    seq_pf[j][i] = seq[j][i];
                }
            }
        
            //write the position-fast orientation
            H5.H5Dwrite(datasetID, DataTypeID, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, seq_pf);
            seq_pf = null;
        }
        System.gc();
        seq = null;
        
        H5.H5Sclose(MemorySpaceID);
        H5.H5Sclose(FileSpaceID);
        
        dataset2status.get(datasetID).put("lastupboundary", lastupboundary + numSitesInBlock);

    }
    
    public void write_dataset_chunk_old(int datasetID, int numSitesInBlock)
    {
        //get dataset status of the tf dataset
        
        int numAlleles = 6;
        int lastupboundary = dataset2status.get(datasetID).get("lastupboundary");
        
        
        int  DataTypeID, FileSpaceID,  MemorySpaceID;
        
        DataTypeID = HDF5Constants.H5T_NATIVE_INT;
        


        //seq array might have some empty rows in the ends
        // not sure how this copyOf works (and Favadoc is confusing)
        //int[][] seq_pf = Arrays.copyOf(seq, numAlleles);
        int [][] seq_pf = new int[numAlleles][numSitesInBlock];
        //seq_pf = Arrays.copyOf(seq,numSitesInBlock);
        // the only way I can get this to work is via a loop
        for(int i=0;i<numSitesInBlock;i++)
        {
            for(int j=0;j<6;j++)
            {
                seq_pf[j][i] = seq[j][i];
            }
        }
        seq=null;
        System.gc();
                
        // Test printout
        /*
        System.out.println("numSitesInBlock: " + numSitesInBlock + ", lastupboundary: " + lastupboundary);
        for(int i=0;i<numSitesInBlock;i++)
        {
            for(int j=0;j<6;j++)
            {
                System.out.print(seq_pf[j][i]+"\t");
            }
            System.out.print("\n");
        }
         * 
         */
        
        //write the position-fast orientation
        FileSpaceID = H5.H5Dget_space (datasetID);
        long[] count= new long[] {numAlleles, numSitesInBlock};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, lastupboundary}, null, count, null);
        H5.H5Dwrite(datasetID, DataTypeID, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, seq_pf);
        H5.H5Sclose(MemorySpaceID);
        H5.H5Sclose(FileSpaceID);
        
        seq_pf = null;
        
        dataset2status.get(datasetID).put("lastupboundary", lastupboundary + numSitesInBlock);

    }
    public int create_dataset (int chrGroupID, int numSites, int numAlleles)
    {
            //encoding = setEncoding;
            //only the sites dimension is chunked, chunksize refer to the sites dimension
            //if chunk size==0, no chunking, then numer of sites is required
            
                     
                        
            //a block cannot be larger than 2g, as java cannot read/write a selection bigger than 2g 
            int maxbytes = (int)2E9;
            if ((numAlleles * write_block_size) > maxbytes)
            {
                write_block_size = maxbytes/numAlleles;
            }

                        
            //create a data set
            int DataTypeID=-1, FileSpaceID=-1, datasetPropertylist=-1, datasetID=-1;
            
            datasetPropertylist = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
            long[] DIMS3=null;
            long[] MAX_DIMS = null;
            
            
            // Should change this to byte once it works...
            if(isbyte)
            {
                DataTypeID = HDF5Constants.H5T_NATIVE_SCHAR;
                System.out.println("Data type is SCHAR" + DataTypeID);
            }
            else
            {
                DataTypeID = HDF5Constants.H5T_NATIVE_INT;
                System.out.println("Data type is INT" + DataTypeID);
            }
            DIMS3 = new long[]{numAlleles, numSites };
            MAX_DIMS = null;
            H5.H5Pset_layout(datasetPropertylist , HDF5Constants.H5D_CONTIGUOUS);
            FileSpaceID = H5.H5Screate_simple(2, DIMS3, MAX_DIMS);
          
            datasetID = H5.H5Dcreate(chrGroupID, "data_array", DataTypeID, FileSpaceID, datasetPropertylist);
            Hashtable <String, Integer> status = new Hashtable<String, Integer>();
            status.put("siteCountInDataSet", 0);
            status.put("numAlleles", numAlleles);
            status.put("numSites", numSites);
            status.put("lastupboundary", 0);
            dataset2status.put(datasetID, status);
            
            if(isbyte)
            {
                add_attribute(datasetID,"logbase",String.valueOf(logbase));
            }
            
            H5.H5Sclose(FileSpaceID);
            return datasetID;
     }
    
    public void close_dataset (int datasetID)
    {
        H5.H5Dclose(datasetID);
    }
    
    public void create_sample_group(String SampGroupName, String SampleName)
    {
        try {
            sampleID = H5.H5Gcreate(fileID, SampGroupName, SampGroupName.length());
            } 
        catch (HDF5Exception ex) 
        {
        }
        add_attribute (sampleID, "name", SampleName);
          
    }
    
    public void create_chrgroup (String chrname)
    {
        int chrGroupID = -1;
        chrGroupID = H5.H5Gcreate(sampleID, chrname, chrname.length());
        if (chrlen>0)
        {
            add_attribute(chrGroupID, "length", String.valueOf(chrlen));
        }

        add_attribute(chrGroupID, "name", chrname);
        
        chrGID = chrGroupID;
    }
    
    public void closeFile()
    {
          H5.H5Gclose(chrGID);
          H5.H5Gclose(sampleID);
          H5.H5Fclose(fileID);
    }
    
    private  void add_attribute (int locid, String name, String value)
    {
        int DataTypeID=-1, attrid=-1, FileSpaceID=-1;
        try {
        DataTypeID = H5.H5Tcopy(HDF5Constants.H5T_C_S1);   
        H5.H5Tset_size(DataTypeID, value.length());
        long[] DIMS1 = {1};
        FileSpaceID = H5.H5Screate_simple(1, DIMS1, null);
            //int datasetPropertylist = H5.H5Pcreate(HDF5Constants.H5P_ATTRIBUTE_CREATE);
        attrid = H5.H5Acreate(locid, name, DataTypeID, FileSpaceID, HDF5Constants.H5P_DEFAULT);
             byte[] projnamebyte = value.getBytes();
             H5.H5Awrite (attrid, DataTypeID,  projnamebyte); 
             H5.H5Aclose(attrid);
             H5.H5Sclose(FileSpaceID);
        }
        catch (HDF5Exception ex) 
        {
        }
    }
}
