/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

/**
 *
 * @author bukowski
 */
public class encoder {
    private double logbase;
    private double logconv;
    private double rlogconv;
    private int offset;
    private double adj;
    private int max_acc_depth;
    private int min_acc_byte;
    public static int [] decoded;
    
    
    public encoder()
    {
        //double logbaseIn = 1.0746;
        //int offsetIn = 126;
        //int max_acc_depthIn = 182;
        //double adjIn = 0.5;
        
        this(1.0746, 126, 0.5, 182);
        
    }
    
    // set max_acc_depthIn=127 if purely exponential formula is to be used
    // for all negative bytes
    public encoder(double logbaseIn, int offsetIn, double adjIn, int max_acc_depthIn)
    {
        logbase = logbaseIn;
        offset = offsetIn;
        rlogconv = 1.0/Math.log(logbase);
        logconv = 1.0/rlogconv;
        adj = adjIn;
        
        // These are good for offset=126, logbase 1.0746, and adj=0.5
        max_acc_depth = max_acc_depthIn;
        min_acc_byte = 127 - max_acc_depth;
        
        predecode();
    }
    
    public byte Encode(int tempdepth)
    {
        byte bdepth;
        int itd;
        
        if(tempdepth <= 127)
        {
            itd = tempdepth;
        }
        else if(tempdepth <= max_acc_depth)
        {
            itd = 127 - tempdepth;
        }
        else
        {
            itd = (int)(-rlogconv*Math.log(tempdepth-offset));
            if(itd < -128) { itd = -128; }
        }
        bdepth = (byte)itd;
        
        return bdepth;
    }
    
    public int Decode(byte bdepth)
    {
        int depth;
        if(bdepth >=0)
        {
                depth = bdepth;
        }
        else if(bdepth >= min_acc_byte)
        {
            depth = 127 - bdepth;
        }
        else
        {
                depth = offset + (int)(Math.exp(-logconv*(bdepth-adj)));
        }
        
        return depth;
        
    }
    
    public int quickDecode(byte bdepth)
    {
        return decoded[bdepth+128];
    }
    
    private void predecode()
    {
        decoded = new int[256];
        for(int i=-128;i<128;i++)
        {
            decoded[i+128] = Decode((byte)i);
            //System.out.println("Pre-decoding "+i+" "+decoded[i+128]);
        }
    }
    
    public double getLogbase()
    {
       return logbase; 
    }
    public int getOffset()
    {
        return offset;
    }
    public double getAdj()
    {
        return adj;
    }
    public int getMaxAccDepth()
    {
        return max_acc_depth;
    }
}
