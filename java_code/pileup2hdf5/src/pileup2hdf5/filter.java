/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.zip.GZIPInputStream;
import static pileup2hdf5.pgenotyper.processAfterGenotyping;

/**
 *
 * @author bukowski
 */
public class filter {
    private double pvthr; 
    private String genofile;
    private String outfile;
    private boolean output_all_sites; 
    private int startpos;
    
    public filter(String genofileIn, String genofileOut, double pvthrIn, boolean output_all_sitesIn, int startposIn)
    {
        pvthr = pvthrIn;
        genofile = genofileIn;
        outfile = genofileOut;
        output_all_sites = output_all_sitesIn;
        startpos = startposIn;
    }
    
    public void filterOnPv()
    {
        /*
        public static void processAfterGenotyping(int [] genallele_nums, int [] depth_alt, int [] qc_mat, int [][]gnum_mat,
            int numaltall, boolean output_all_sites, int nall_after_genotyping_thr, int numsamples,
            String [] genotype_mat, int [][] num_mat, int [][] lkhd_mat, StringBuilder outstrbld,
            String allele_strIn, double [][] PVmat, String infostrIn, String outstrIn, boolean output_genotypes)
            */
        
        // Format-dependent constants
        int NONTAXA_COLUMNS = 5;
        int INFO_POSITION = 4;
        boolean isVCF = false;
        int allele_str_index = 3;
        //int PVindex = 10;
        //int GNindex = 6;
        
        int DPindex = -1;
        int NZindex = -1;
        int ADindex = -1;
        int ANindex = -1;
        int AQindex = -1;
        int GNindex = -1;
        int HTindex = -1;
        int EFindex = -1;
        int MAFindex = -1;
        int PVindex = -1;
        int IBD1index = -1;
        int FHindex = -1;
        int FH2index = -1;
        int LLDindex = -1;
        int NI5index = -1;
        
        // needed for the call to processAfterGenotyping
        int [] genallele_nums = new int[7];
        int [] depth_alt = new int[7];
        int [] qc_mat = new int[7];
        int [][] gnum_mat = new int[7][7];
        int numaltall;
        int nall_after_genotyping_thr=2;
        double edfactor_reject_thr = 1000.0;
        int numsamples;
        String [] genotype_mat;
        int [][] num_mat;
        int [][] lkhd_mat;
        StringBuilder outstrbld;
        String allele_str;
        double [][] PVmat = new double[7][7];
        String infostr;
        String outstr;
        
        // Other variables
        String [] tokens;
        String [] info_tokens;
        String line;
        String info;
        String [] aux;
        String [] aux1;
        int im1, im2;
        int numalleles;
        int count;
        int linecount=0;
        int curpos=0;
        
        try
        {
            BufferedReader fileIn = null;
            if(genofile.endsWith(".gz"))
            {
                fileIn=new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(genofile))));
            }
            else
            {
                fileIn = new BufferedReader(new FileReader(genofile), 1000000);
            }
            BufferedWriter fileOut = null;
            if(!outfile.equals("-"))
            {
                fileOut = new BufferedWriter(new FileWriter(outfile), 1000000);
            }
            else
            {
                fileOut = new BufferedWriter(new OutputStreamWriter(System.out));
            }
            line=null;
            
            // read/write header line first
            line=fileIn.readLine();
            fileOut.write(line+"\n");
            // Deterime if this is a VCF format
            if(line.toLowerCase().contains("vcf"))
            {
                isVCF = true;
                NONTAXA_COLUMNS = 9;
                INFO_POSITION = 7;
                allele_str_index = 4;
            }
            // Read and echo VCF header
            if(isVCF)
            {
                while((line=fileIn.readLine()).startsWith("##"))
                {
                    fileOut.write(line+"\n");
                }
                fileOut.write(line+"\n"); // this should be th header line starting with #CHROM
            }
            
            tokens = line.split("\t");
            numsamples = tokens.length - NONTAXA_COLUMNS;
            genotype_mat = new String[numsamples];
            num_mat = new int[numsamples][7];
            lkhd_mat = new int [numsamples][28];
            
            // proces the rest of the file
            linecount = 0;
            while((line=fileIn.readLine()) != null)
            {
                if(line.startsWith("#")) { continue; }
                linecount++;
                
                tokens = line.split("\t");
                
                curpos = Integer.parseInt(tokens[1]);
                if(curpos < startpos) { continue; }
                
                if(tokens.length - NONTAXA_COLUMNS != numsamples)
                {
                    int detected_samples = tokens.length - NONTAXA_COLUMNS;
                    System.out.println("Number of samples inconsistent in line "+linecount + " pos "+curpos);
                    System.out.println("Detected: "+detected_samples + ", expacted: "+numsamples);
                    System.out.println("Skipping position");
                    continue;
                }
                
                allele_str = tokens[allele_str_index];
                
                //System.out.println("DEBUG1");
                
                if(isVCF)
                {
                    outstr = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t" + tokens[3] + "\t";
                    allele_str = allele_str.replaceAll("<INS>", "I").replaceAll("<DEL>", "D");
                }
                else
                {
                    outstr = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t";
                }
                
                // decipher the info string
                info = tokens[INFO_POSITION];
                // This should not happen, but early version of 822-826 taxa fixing
                // script had a small error leading to ",," instead of ",0,", etc.
                info = info.replaceAll(",,", ",0,").replaceAll("=,", "=0,").replaceAll(",;", ",0;");
                info_tokens = info.split(";");
                
                // Determine the indexes of various INFO fields
                // Typical INFO string:
                // DP=3139;NZ=686;AD=3135,3;AN=1370,2;AQ=33,26;GN=685,0,1;HT=0;EF=0.00;MAF=0.001;PV=0.007;IBD1;FH=0.000;FH2;LLD;NI5
                for(int i=info_tokens.length-1;i>=0;i--)
                {
                    if(info_tokens[i].contains("DP")) { DPindex = i; continue; }
                    if(info_tokens[i].contains("NZ")) { NZindex = i; continue; }
                    if(info_tokens[i].contains("AD")) { ADindex = i; continue; }
                    if(info_tokens[i].contains("AN")) { ANindex = i; continue; }
                    if(info_tokens[i].contains("AQ")) { AQindex = i; continue; }
                    if(info_tokens[i].contains("GN")) { GNindex = i; continue; }
                    if(info_tokens[i].contains("HT")) { HTindex = i; continue; }
                    if(info_tokens[i].contains("EF")) { EFindex = i; continue; }
                    if(info_tokens[i].contains("MAF")) { MAFindex = i; continue; }
                    if(info_tokens[i].contains("PV")) { PVindex = i; continue; }
                    if(info_tokens[i].contains("IBD1")) { IBD1index = i; continue; }
                    if(info_tokens[i].contains("FH")) { FHindex = i; continue; }
                    if(info_tokens[i].contains("FH2")) { FH2index = i; continue; }
                    if(info_tokens[i].contains("LLD")) { LLDindex = i; continue; }
                    if(info_tokens[i].contains("NI5")) { NI5index = i; continue; }
                }
                
                 
                Arrays.fill(depth_alt,0);
                Arrays.fill(genallele_nums,0);
                Arrays.fill(qc_mat,0);
                
                // !!!! all thre arrays below (except maybe qc_mat)  must be re-generated from genotypes, because the number of
                // taxa may have changed....
                
                //System.out.println("AFter info_tokens1");
                aux = allele_str.split(",");
                //convertStringArrayToInt(aux, depth_alt);
                //System.out.println("AFter info_tokens2");
                //convertStringArrayToInt(info_tokens[ANindex].split("=")[1].split(","),genallele_nums);
                //System.out.println("AFter info_tokens3");
                if(AQindex>-1) { convertStringArrayToInt(info_tokens[AQindex].split("=")[1].split(","),qc_mat); }
                numalleles = aux.length + 1;  // +1 because need to count reference
                numaltall = numalleles - 1;
                
                //System.out.println("GNindex: "+GNindex);
                pgenotyper.zero2Dmat(gnum_mat);
                // gnum_mat needs to be re-generated if number of taxa has changed
                /*
                aux = info_tokens[GNindex].split("=")[1].split(",");
                count = 0;
                for(int i=0;i<numalleles;i++)
                {
                    for(int j=i;j<numalleles;j++)
                    {
                        gnum_mat[i][j] = Integer.parseInt(aux[count]);
                        gnum_mat[j][i] = gnum_mat[i][j];
                        count++;
                    }
                }
                */
                //System.out.println("Before PV stuff");
                // PV stuff - we will leave this unchanged, even if number of taxa
                // changed since PV was generated.Otherwise it would be too much work
                for(int i=0;i<7;i++)
                {
                    Arrays.fill(PVmat[i], 1.0);
                }
                if(PVindex > -1)
                {
                    aux = info_tokens[PVindex].split("=")[1].split(",");
                    count = 0;
                    for(int i=0;i<numalleles;i++)
                    {
                        for(int j=i+1;j<numalleles;j++)
                        {
                            PVmat[i][j] = Double.parseDouble(aux[count]);
                            PVmat[i][j] = Math.min(1.0, PVmat[i][j]);
                            PVmat[j][i] = PVmat[i][j];
                            count++;
                        }
                    }
                }
                
                //System.out.println("Before sample loop");
                // This much can be extracted from the info string.
                // Other arrays must be extracted from genotype data, i.e., loop over taxa
                int samplesnonzerocov = 0;
                for(int i=0;i<numsamples;i++)
                {
                    //System.out.println("Before sample "+i + ", non-taxa columns: "+NONTAXA_COLUMNS);
                    aux = tokens[i+NONTAXA_COLUMNS].split(":");
                    genotype_mat[i] = aux[0];
                    if(genotype_mat[i].equals(".")) { genotype_mat[i] = "./."; }
                    if(aux.length > 1)
                    {
                        //System.out.println("ater aux: "+aux[0]);
                        Arrays.fill(num_mat[i],0);
                        convertStringArrayToInt(aux[1].split(","),num_mat[i]);
                        //System.out.println("ater num_mat");
                        if(aux.length >2)
                        {
                            convertStringArrayToInt(aux[2].split(","),lkhd_mat[i]);
                            //System.out.println("ater lkhd_mat");
                        }
                        else
                        {
                            Arrays.fill(lkhd_mat[i],0);
                        }
                    }
                    else
                    {
                        Arrays.fill(num_mat[i],0);
                        Arrays.fill(lkhd_mat[i],0);
                    }
                    //System.out.println("After sample "+i);
                    // Update allele depths
                    for(int ia=0;ia<numalleles;ia++)
                    {
                        depth_alt[ia] += num_mat[i][ia];
                    }
                    // Update allele numbers
                    if(genotype_mat[i].equals("./.")) { continue; }
                    aux1 = genotype_mat[i].split("/");
                    im1 = Integer.valueOf(aux1[0]);
                    im2 = Integer.valueOf(aux1[1]);
                    genallele_nums[im1]++;
                    genallele_nums[im2]++;
                    gnum_mat[im1][im2]++;
                    gnum_mat[im2][im1]++;
                    samplesnonzerocov++;
                }
                
                //System.out.println("DEBUG4");
                
                // What is the major allele?
                // Careful: genallele_nums must be first updated in the loop above
                int almajor = 0;
                int ntmp = 0;
                for(int ia=0;ia<numalleles;ia++)
                {
                    if(genallele_nums[ia] > ntmp) { ntmp = genallele_nums[ia]; almajor = ia; }
                }
                // almajor is the index of the major allele
                
                // Do the filtering over PVmat
                // For each alternative allele, check if at least one P-value is acceptable; if not - zero
                // out this allele'depth to exclude from sorting
                // The search for minimum should be restricted to major-nimor alleles only...
                double [] aminpv = new double[7];
                double globmin= 100;
                int iaglobmin = 100;
                int ibglobmin = 100;
                for(int ia=1;ia<7;ia++)
                {
                    if(genallele_nums[ia] == 0) { continue; }
                    double minpv = 100.0;
                    int ibmin = 100;
                    for(int ib=0;ib<7;ib++)
                    {
                        if(ib == ia) { continue; }
                        if(ia != almajor && ib != almajor) { continue; }
                        if(PVmat[ia][ib] < minpv) { minpv = PVmat[ia][ib]; ibmin = ib;}
                    }
                    aminpv[ia] = minpv;
                    if(minpv < globmin) { globmin = minpv; iaglobmin = ia; ibglobmin = ibmin;} 
                }
                int totDP = depth_alt[0];
                for(int ia=1;ia<7;ia++)
                {
                    // eliminate allele if p-value too large, but always keep the
                    // allele with the smallest p-value if all sites output requested
                    // This way, pvalthr can always be kept reasonably small
                    boolean skipallele = aminpv[ia] > pvthr || !(depth_alt[ia]>2 && isAlleleDeepEnoughInOneTaxon(num_mat,ia)); 
                    skipallele = skipallele && ! ((ia == iaglobmin || ia == ibglobmin) && output_all_sites);
                    if(skipallele)
                    {
                        genallele_nums[ia] = 0;
                        depth_alt[ia] = 0;
                        qc_mat[ia] = 0;
                    }
                    // We can calculate DP here from the updated depth_alt
                    totDP += depth_alt[ia];
                }
                
                infostr = "DP="+totDP+";NZ="+samplesnonzerocov+";";
                if(LLDindex > -1) { infostr += info_tokens[LLDindex] + ";"; }
                if(IBD1index > -1) { infostr += info_tokens[IBD1index] + ";"; }
                if(NI5index > -1) { infostr += info_tokens[NI5index] + ";"; }
                if(FHindex > -1) { infostr += info_tokens[FHindex] + ";"; }
                if(FH2index > -1) { infostr += info_tokens[FH2index] + ";"; }
                
                // Almost there!
                outstrbld = new StringBuilder();
                int filtstat = pgenotyper.processAfterGenotyping(genallele_nums, depth_alt, qc_mat, gnum_mat,
                numaltall, output_all_sites, nall_after_genotyping_thr, edfactor_reject_thr, numsamples,
                genotype_mat, num_mat, lkhd_mat, outstrbld,
                allele_str, PVmat, infostr, outstr, true);
                
                //System.out.println("After processAfterGenotyping");
                if(filtstat >0)
                {
                    // This site has been skipped b/c of filtering
                    continue;
                }
                // Otherwise, print out the site
                outstr = outstrbld.toString();
                try
                {
                    fileOut.write(outstr);
                }
                catch(Exception e)
                {
                    System.out.println("Thread could not write to file"+e);
                }
            }
            fileIn.close();
            fileOut.close();
        }
        catch(Exception e)
        {
            System.out.println("Error processing line " + linecount + ":" + e);
        }
        
    }
    
    private static void convertStringArrayToInt(String [] a, int [] b)
    {
        for(int i=0;i<a.length;i++)
        {
            b[i] = Integer.parseInt(a[i]);
        }
    }
    
    private static boolean isAlleleDeepEnoughInOneTaxon(int [][] num_mat, int ia)
    {
        for(int i=0;i<num_mat.length;i++)
        {
            if(num_mat[i][ia]>1) { return true; }
        }
        return false;
    }
    
}
