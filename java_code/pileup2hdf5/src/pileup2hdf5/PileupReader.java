/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.util.List;

/**
 *
 * @author bukowski
 */
public class PileupReader extends GeneralPileupReader {
    protected int blocksize;
    private int baseQCaverageType;
    
   public PileupReader(List<String> samplistIn, List<Integer> poslistIn, String chromIn, Integer gversionIn, 
            int chrlenIN, int startRangeIn, int endRangeIn, int blocksizeIn,
            int[][][] dataIn, int[][] qcdataIn)
   {
            super(samplistIn, poslistIn, chromIn, gversionIn, 
            chrlenIN, startRangeIn, endRangeIn,
            dataIn, qcdataIn);
            
            blocksize = blocksizeIn;
            baseQCaverageType = 2; // 1: equal weigths (1/N_samples); 2: weights propportional to fraction of ellele reads coming from this sample
   }
    
    public void read_pileup()
    {
        int numSamples = samplist.size();
        boolean useQC=true;
        long startTime, endTime;
        double duration;
        
        for(int i=0;i<numSamples;i++)
        {
            startTime = System.currentTimeMillis();
            h5reader h5rdr = new h5reader(samplist.get(i),chrom, gversion, chrlen);
            h5rdr.get_range(startRange, endRange, false);  // tnis will cap endRange, if needed
            //data[i] = Arrays.copyOf(h5rdr.outmat, 6); // gets slow every 4th sample when samples 370 in total - maybe there was not enogh memory
            // Try naive copy instead
            for(int j=0;j<6;j++)
            {
                for(int k=0;k<blocksize;k++)
                {
                    data[poslist.get(i)][j][k] = h5rdr.outmat[j][k]; 
                }
            }
            //h5rdr.close();
            // If needed, read the allele QC daata and average over samples....
            if(useQC)
            {
                // Modify the QC file name!
                //h5rdr = new h5reader(SampleQcList.get(i),chrom, gparams.getGversion());
                //h5rdr.get_range(startRange, endRange, false);  // tnis will cap endRange, if needed
                for(int j=0;j<6;j++)
                {
                    for(int k=0;k<blocksize;k++)
                    {
                        //qcdata[j][k] += h5rdr.outmat[j][k]; 
                        if(baseQCaverageType ==1)
                        {
                            qcdata[j][k] += h5rdr.outmatqc[j][k];
                        }
                        else
                        {
                            qcdata[j][k] += h5rdr.outmatqc[j][k]*h5rdr.outmat[j][k];;
                        }
                    }
                }
                //h5rdr.close();
                // Normalize the qcdata to averages over samples - divide by number of samples where this allele is present
                // or by allele depth, depending on baseQCaverageType
                // this nees to be done in the pgenotyper....
            }
            h5rdr.close();
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime-startTime);
            System.out.println("Sample "+poslist.get(i)+" slice loaded in "+duration+" sec");
        }
    }
    
    @Override
    public void run()
    {
        read_pileup();
    }
    
}
