/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import java.io.BufferedReader;
import java.io.Reader;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author bukowski
 */
public class IOthread {
    ArrayList<String> SampleList = new ArrayList<String>(); // will keep the names of sample h5 mpileup files
    ArrayList<String> SampleQcList = new ArrayList<String>(); // will keep the names of sample QC h5 mpileup files
    private int[][][] data = null;
    private int[][]   qcdata = null;
    private byte[] refsequence = null;
    private genotyper_params gparams = null;
    private boolean useQC = true;
    private ArrayList<Integer> pos2geno = null;
    private ArrayList<String> altall2geno = null;
    private boolean h5input = true;
    
    public IOthread(String sampleFile, String paramFile, String chrom, int startRange, int endRange, int numThreads, int threadPiece, int numIOthreadsIn, String h5Dir, String posFile)
    {
        // Load the list of sample h5 files to process
        try
        {
            BufferedReader fileIn = new BufferedReader(new FileReader(sampleFile), 1000000);
            String line=null;
            while((line=fileIn.readLine()) != null)
            {
                // Names in sample list may or may not contain ".h5" suffix - we can now keep uniform sample lists
                String dfile = line.replace(".h5", "");
                //String qcfile = line.replace(".h5", "_QC.h5");
                String qcfile = dfile + "_QC.h5";
                dfile = dfile + ".h5";
                SampleList.add(h5Dir + "/" + dfile);
                SampleQcList.add(h5Dir + "/" + qcfile);
            }
        }
        catch(Exception e)
        {
            System.out.println("Could not read the sample file" + e);
        }
        // Load the parameters
        gparams = new genotyper_params();
        try
        {
            JAXBContext context = JAXBContext.newInstance(genotyper_params.class);
            Unmarshaller um = context.createUnmarshaller();
            try
            {
                Reader r = new FileReader(paramFile);
                gparams = (genotyper_params) um.unmarshal(r);
                if(gparams.getOutput_all_sites())
                {
                    // If all sites are to be output (like in GBS anchor calculation),
                    // relax all filtering parameters accordingly
                    relax_genotyping_params();
                }
                print_genotyping_params();
            }
            catch(IOException i)
            {
                System.out.println("Could not read the parameter file");
                 i.printStackTrace();
            }
        }
        catch(JAXBException i)
        {
            System.out.println("Could not de-serialize the parameter class");
            i.printStackTrace();
        }
        
        // If the list of sites to genotype is given, read it in
        // The format of posFIle is position\talt1,alt2,...
        // If alterntaive alleles not listed, the discovery genotyping will be run on the sites provided
        // Assume positions are sorted in ascending order
        if(posFile != null)
        {
            try
            {
                pos2geno = new ArrayList<Integer>();
                altall2geno = new ArrayList<String>();
                BufferedReader fileIn = new BufferedReader(new FileReader(posFile), 1000000);
                String line=null;
                while((line=fileIn.readLine()) != null)
                {
                    String [] aux = line.split("\t");
                    pos2geno.add(Integer.parseInt(aux[0]));
                    if(aux.length>1) { altall2geno.add(aux[1]); }
                }
                if(altall2geno.size() == 0) 
                { 
                    altall2geno = null; // signal that the discovery path should be taken
                } 
                else
                {
                    // Not sure if this is really a good idea to modify this...
                    //startRange = pos2geno.get(0);
                    //endRange = pos2geno.get(pos2geno.size()-1);
                }
            }
            catch(Exception e)
            {
                System.out.println("Could not read the positions file" + e);
            }
        }
        // Load the approproate chunk of reference sequence - assumes the file maizev2.h5 or maizev1.h5 is in current dir
        // First, normalize the end of range
        long startTime = System.currentTimeMillis();
        int chromlen;
        int [] chrinfo = null;
        
        if(gparams.getRef_h5_file().endsWith(".h5"))
        {
            chromlen = h5writereference.chromosome_lengths(gparams.getGversion()).get(chrom);
        }
        else  // i.e, if  reference is fasta file
        {
            // Retrieve the chromosom's infro from the fai file
            chrinfo = getChromInfoFromFai(gparams.getRef_h5_file() + ".fai",chrom);
            chromlen = chrinfo[0];
        }
        
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int blocksize = endRangeNorm - startRange + 1;
        if(blocksize < 1)
        {
            System.out.println("Start position beyond the end of chromosome - nothing to do");
            return;
        }
        
        if(!(gparams.getRef_h5_file().isEmpty() || gparams.getRef_h5_file().endsWith(".h5")))
        {
            // Instead of reading reference from H5 file. we will read it directoly from fasta
            // first executing seek to start from the indexed byte position
            System.out.println("Loading reference chunk from fasta file "+gparams.getRef_h5_file());
            refsequence = getRefFromFasta(gparams.getRef_h5_file(),chrinfo, startRange,blocksize);
        
        }
        else
        {
            // The H5 path to be deprecated, but let's keep it for now
            IHDF5Reader rdr;
            if(gparams.getRef_h5_file().isEmpty())
            {
                System.out.println("Loading reference chunk from H5 file "+"maizev"+gparams.getGversion()+".h5");
                rdr = HDF5Factory.openForReading("maizev"+gparams.getGversion()+".h5");
            }
            else
            {
                System.out.println("Loading reference chunk from H5 file "+ gparams.getRef_h5_file());
                rdr = HDF5Factory.openForReading(gparams.getRef_h5_file());
            }
            int offset = startRange - 1;
            refsequence = rdr.readByteArrayBlockWithOffset("/"+chrom+"/sequence",blocksize,offset);
            rdr.close();
        // this much will be relaced by direct fasta reading
        }
        
        long endTime = System.currentTimeMillis();
        double duration = 0.001*(endTime-startTime);
        System.out.println("Reference slice loaded in "+duration+" sec");
        
        // Read in the actual mpileup data
        long startReadTime = System.currentTimeMillis();
        int numSamples= SampleList.size();
        data = new int[numSamples][6][blocksize];
        //data = new int[blocksize][numSamples][6];
        qcdata = new int[6][blocksize];
        pgenotyper.zero2Dmat(qcdata); // becuase it will by added to
        pgenotyper.zero3Dmat(data); // Important for directc samtools case, where no all positions are returned
        
        // This is the path with reading parallelized by taxa.
        // Probably not the best idea when HDF5 files are read, but may be useful
        // when samtoool mpileup is run directly.....
        int numIOthreads = numIOthreadsIn;
        if(numIOthreads >0) // shopuld be an input parameter
        {
            h5input = false;
            System.out.println("Will read from samtools with "+numIOthreads+" threads");
        }
        else // if numIOthreads inout as 0, we just read HDF5 files purely sequentialy
        {
            numIOthreads = 1;
            h5input = true;
            System.out.println("Will read from H5 files with "+numIOthreads+" threads");
            
        }
        // First, distribute the taxa (names/h5 files and positions in original list) over threads
        List<List<String>> taxalists = new ArrayList<List<String>>();
        List<List<Integer>> taxapos = new ArrayList<List<Integer>>();
        //for(int i=0;i<numIOthreads;i++)
        for(int i=0;i<numSamples;i++)
        {
            taxalists.add(new ArrayList<String>());
            taxapos.add(new ArrayList<Integer>());
        }
        for(int i=0;i<numSamples;i++)
        {
            //taxalists.get(i % numIOthreads).add(SampleList.get(i));
            //taxapos.get(i % numIOthreads).add(i);
            taxalists.get(i).add(SampleList.get(i));
            taxapos.get(i).add(i);
        }
        
        java.util.concurrent.ExecutorService IOexecutor = Executors.newFixedThreadPool(numIOthreads);
        
        if(h5input)
        {
            //for(int i=0;i<numIOthreads;i++)
            for(int i=0;i<numSamples;i++)
            {
                PileupReader preader = new PileupReader(taxalists.get(i), taxapos.get(i),chrom, gparams.getGversion(), chromlen, startRange, endRangeNorm, blocksize,data, qcdata);
                IOexecutor.execute(preader);
            }
        }
        else
        {
            //for(int i=0;i<numIOthreads;i++)
            for(int i=0;i<numSamples;i++)
            {
                // This eader which directly calls samtools......
                SamtoolsPileupReader preader = new SamtoolsPileupReader(taxalists.get(i), taxapos.get(i),chrom, 
                        chromlen, (Integer)gparams.getGversion(), startRange, endRangeNorm, 
                        data, qcdata, pos2geno,
                        gparams.getMpileupmaincmd());
                IOexecutor.execute(preader);
            
            }
        }
        IOexecutor.shutdown();
        try
        {
            IOexecutor.awaitTermination(100, TimeUnit.DAYS);
        }
        catch(Exception e)
        {
            System.out.println("IOexecutor termination problem: "+e);
        }
        endTime = System.currentTimeMillis();
        // End of parallel reading path
        
        // This is the path with the current thread reading mpileup file for each taxon one by one.
        /*
        for(int i=0;i<numSamples;i++)
        {
            startTime = System.currentTimeMillis();
            h5reader h5rdr = new h5reader(SampleList.get(i),chrom, gparams.getGversion());
            h5rdr.get_range(startRange, endRange, false);  // tnis will cap endRange, if needed
            //data[i] = Arrays.copyOf(h5rdr.outmat, 6); // gets slow every 4th sample when samples 370 in total - maybe there was not enogh memory
            // Try naive copy instead
            for(int j=0;j<6;j++)
            {
                for(int k=0;k<blocksize;k++)
                {
                    data[i][j][k] = h5rdr.outmat[j][k]; 
                }
            }
            //h5rdr.close();
            // If needed, read the allele QC daata and average over samples....
            if(useQC)
            {
                // Modify the QC file name!
                //h5rdr = new h5reader(SampleQcList.get(i),chrom, gparams.getGversion());
                //h5rdr.get_range(startRange, endRange, false);  // tnis will cap endRange, if needed
                for(int j=0;j<6;j++)
                {
                    for(int k=0;k<blocksize;k++)
                    {
                        //qcdata[j][k] += h5rdr.outmat[j][k]; 
                        qcdata[j][k] += h5rdr.outmatqc[j][k];
                    }
                }
                //h5rdr.close();
                // Normalize the qcdata to averages over samples - divide by number of samples where this allele is present
                // this nees to be done in the pgenotyper....
            }
            h5rdr.close();
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime-startTime);
            System.out.println("Sample "+i+" slice loaded in "+duration+" sec");
        }
        */
        // End of sequential reading path
        duration = 0.001*(endTime - startReadTime)/60;
        System.out.println("All samples loaded in "+duration+" min");
        /*
        System.out.println("Transposing data matrix");
        startTime = System.currentTimeMillis();
        data = new int[blocksize][numSamples][6];
        endTime = System.currentTimeMillis();
        duration = 0.001*(endTime - startReadTime)/60;
        System.out.println("data memory allocated in "+duration+" min");
        for(int i=0;i<numSamples;i++)
        {
            startTime = System.currentTimeMillis();
            for(int j=0;j<6;j++)
            {
                for(int k=0;k<blocksize;k++)
                {
                    data[k][i][j] = data1[i][j][k]; 
                }
            }
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime-startTime);
            System.out.println("Sample "+i+" transposed in "+duration+" sec");
        }
         * 
         */
        
        java.util.concurrent.ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        
        // Distribute the work among threads
        ArrayList<Thread> my_threads = new ArrayList<Thread>();
        int numsites_all = blocksize;
        int numsites_thread = threadPiece; //numsites_all/(10*numThreads); // chp it to 10 pieces for test
        // For now, we just assume this is > 1
        int start_thr = startRange;
        int end_thr;
        while(start_thr <= endRangeNorm)
        {
            end_thr = Math.min(start_thr + numsites_thread - 1, endRangeNorm);
            System.out.println("Main thread submitting subrange " + start_thr + "-" + end_thr);
            pgenotyper pgnt = new pgenotyper(chrom, chromlen, start_thr, end_thr, startRange, SampleList, refsequence, data, qcdata, gparams, pos2geno, altall2geno);
            //Thread thr = new Thread(pgnt);
            //thr.start();
            executor.execute(pgnt);
            //my_threads.add(thr);
            start_thr = end_thr + 1;
        }
        
        executor.shutdown();
        try
        {
            executor.awaitTermination(100, TimeUnit.DAYS);
        }
        catch(Exception e)
        {
            System.out.println("executir termination problem: "+e);
        }
        //while(! executor.isTerminated())
        //{
            // just wait, but it takes one full CPU to do this...
        //}
        
        // Wait for all threads to finish
        /*
        for(int i=0;i<my_threads.size();i++)
        {
            try
            {
                my_threads.get(i).join();
            }
            catch(Exception e)
            {
                System.out.println("thread cannot be joined");
            }
                    
        }
         * 
         */
    }
    
    private void print_genotyping_params()
    {
        System.out.println("Outoput format: "+gparams.getOutfmt());
        System.out.println("Output all requested sites: "+gparams.getOutput_all_sites());
        // Genome version
        System.out.println("Genome version: " + gparams.getGversion());
        // Genotyper parameters
        System.out.println("Genotyper parameters");
        System.out.println("Overall mismatch rate: " + gparams.getE());
        // depth-based thresholds
        System.out.println("Depth-based filters");
        System.out.println("sample considered to have nonzero coverage if its total depth is > "+gparams.getNonzero_cov_thr());
        System.out.println("site skipped if number of samples with coverage (defined as bove) is < "+gparams.getMinnumsampcov());
        System.out.println("allele is considered to occur in a sample if its depth is > "+gparams.getNonzero_all_cov_thr());
        System.out.println("maximum number of retained alternative alleles "+gparams.getNumaltall_max());
        System.out.println("minimum combined (over all samples) depth to consider allele for genotyping "+gparams.getAllele_retension_thr());
        System.out.println("allele depth must be at least this in at least one sample "+gparams.getSamp_allele_retension_thr());
        System.out.println("The two above thresholds must BOTH be met for alt allele to be considered for genotyping");
        
        System.out.println("allele rejected if pvalue of contingency table larger than this "+gparams.getPvalthr());
        System.out.println("contingency matrix version: "+gparams.getContmatver());
        System.out.println("If Cochran condition satisfied, no simulation even for small chisq p-values " + gparams.getUseCochran());
        if(gparams.getCmatMaxThr() > 0)
        {
            System.out.println("Contingency matrix elements capped at " + gparams.getCmatMaxThr());
        }
        //System.out.println("sample will be part of contingency table test if reference+1st_minor allele depth is greater than "+gparams.getCntg_thr() + "(not used)");    
        System.out.println("sample is NOT used in Ed's factor tests if depth over all retained alleles is < "+gparams.getDepth_retained_alleles_thr());     
        System.out.println("site skipped if combined depth of alternative alleles over all samples is < "+gparams.getDepth_alt_thr());
        
        // genotype-based thresholds
        System.out.println("Genotype-based filters");
        System.out.println("site skipped if number of samples with retained alleles is < "+gparams.getSamps_with_ret_all_thr());
        System.out.println("site skipped if number of non-ref alleles in all samples less or equal than "+gparams.getAlt_num_thr() +" ....");
        System.out.println("... and depth in alt genotype sample (maximized over samples) is less than " + gparams.getMax_depth_alt_thr());
        System.out.println("keep up to this number of alternative alleles after genotyping and re-sorting: "+gparams.getNall_after_genotyping_thr());
        System.out.println("site skipped if Ed's factor larger than "+gparams.getEdfactor_reject_thr());
        
    }
    
    private void relax_genotyping_params()
    {
        gparams.setNonzero_cov_thr(0);
        gparams.setNonzero_all_cov_thr(0);
        gparams.setNumaltall_max(6);
        gparams.setAllele_retension_thr(1);
        gparams.setMinnumsampcov(1.0);
        //gparams.setPvalthr(1.1);
        gparams.setDepth_retained_alleles_thr(1);
        gparams.setDepth_alt_thr(-1);
        gparams.setMax_depth_alt_thr(-1);
        gparams.setSamps_with_ret_all_thr(1);
        gparams.setAlt_num_thr(-1);
        gparams.setEdfactor_reject_thr(1000.0);
    }
    
    public static int [] getChromInfoFromFai(String fainame, String chrom)
    {
        int [] result = null;
        String [] aux = null;
        try
        {
            BufferedReader fileIn = new BufferedReader(new FileReader(fainame), 1000000);
            System.out.println("Reading fai file "+fainame);
            String line=null;
            while((line=fileIn.readLine()) != null)
            {
                aux = line.split("\t");
                if(aux[0].equals(chrom))
                {
                    result = new int [4];
                    result[0] = Integer.parseInt(aux[1]);
                    result[1] = Integer.parseInt(aux[2]);
                    result[2] = Integer.parseInt(aux[3]);
                    result[3] = Integer.parseInt(aux[4]);
                    break;
                }
            }
            fileIn.close();
        }
        catch(Exception e)
        {
            System.out.println("Could not read the fai file:" + e);
        }
        
        return result;
    }
    
    public static byte [] getRefFromFasta(String fastafile, int [] chrinfo, int startRange, int blocksize)
    {
        byte [] result = new byte[blocksize];
        byte [] bytes;
        int nbytes = 0;
        int ii;
        
        System.out.println("fastafile: "+fastafile+",  chrinfo: "+chrinfo[0]+" "+chrinfo[1]);
        try
        {
            RandomAccessFile refFasta = new RandomAccessFile(fastafile,"r");
            // Set the pointer at the right position
            // chrinfo[1] is the beginning of the sequence. To start at startRange,
            // we need to move the pointer by startRange plus the number of line endings
            // occurring before srartRange. Number of letters in a line is in chrinfo[2].
            int numlines2start = (int)(startRange/chrinfo[2]);  
            int offset = chrinfo[1] + numlines2start + startRange - 1;
            refFasta.seek(offset); 
            // Read line by line until the requested number of bytes is completed
            System.out.println("After seek");
            while(nbytes < blocksize)
            {  
                bytes = refFasta.readLine().getBytes();
                for(int i=0;i<bytes.length;i++)
                {
                    ii = nbytes + i;
                    if(ii==blocksize)
                    {
                        break;
                    }
                    result[nbytes+i] = bytes[i];
                }
                nbytes += bytes.length;
            }
            refFasta.close();
        }
        catch(Exception e)
        {
            System.out.println("Cannot open reference fasta: " + e);
        }
        
        return result;
    }
    
}
