/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Writer;

/**
 *
 * @author bukowski
 */
public class h5writereference {
    String fastaFile;
    String outH5;
    IHDF5Writer wrtr = null;
    private Hashtable<String, Integer> chromlens = new Hashtable<String,Integer>();
    
    public h5writereference(String fastafile, String outh5, int gversion)
    {
        fastaFile = fastafile;
        outH5 = outh5;
        wrtr = HDF5Factory.open(outh5);
        
        // Initialize chromosome lengths
        chromlens = chromosome_lengths(gversion);
        
    }
    
    public static Hashtable<String, Integer> chromosome_lengths(int version)
    {
        Hashtable<String, Integer> lll = new Hashtable<String,Integer>();
        
        // Test case
        //lll.put("1", 19);
        //lll.put("2", 40);
        if(version == 1)
        {
            // Define this for maize v1
            lll.put("1",300239041);
            lll.put("2",234752839); 
            lll.put("3",230558137); 
            lll.put("4",247095508);
            lll.put("5",216915529); 
            lll.put("6",169254300); 
            lll.put("7",170974187); 
            lll.put("8",174515299); 
            lll.put("9",152350485); 
            lll.put("10",149686045);
            lll.put("0",14680007);
        }
        else if (version == 2)
        {
            // Define this for maize v2
            lll.put("1",301354135);
            lll.put("2",237068873);
            lll.put("3",232140174);
            lll.put("4",241473504);
            lll.put("5",217872852);
            lll.put("6",169174353);
            lll.put("7",176764762);
            lll.put("8",175793759);
            lll.put("9",156750706);
            lll.put("10",150189435); // For test make a very short chromosome
            //lll.put("10",5);
            lll.put("UNKNOWN",7140151);
            lll.put("Pt",140384);
            lll.put("Mt",569630);
        }
        else if (version == 3)
        {
            // Define this for maize v3
            
            lll.put("10",149632204);
            lll.put("9",157038028);
            lll.put("6",169407836);
            lll.put("8",175377492);
            lll.put("7",176826311);
            lll.put("5",217959525);
            lll.put("3",232245527);
            lll.put("2",237917468);
            lll.put("4",242062272);
            lll.put("1",301476924);
            lll.put("Pt",140384);
            lll.put("Mt",569630);
        }
        else // All other integers
        {
            // Define lengths for cassava
 
            lll.put("1",20421248);
            lll.put("2",19846287);
            lll.put("3",17481451);
            lll.put("4",17400302);
            lll.put("5",19772569);
            lll.put("6",19272237);
            lll.put("7",24028438);
            lll.put("8",21591237);
            lll.put("9",25121365);
            lll.put("10",23452318);
            lll.put("11",21832491);
            lll.put("12",20687738);
            lll.put("13",20905276);
            lll.put("14",23388673);
            lll.put("15",22007488);
            lll.put("16",19164983);
            lll.put("17",16278965);
            lll.put("18",17307199);
            lll.put("19",164895180);
        }

        return lll;
    }
    
    public void covert_fasta()
    {
        try
        {
            BufferedReader fileIn = new BufferedReader(new FileReader(fastaFile), 1000000);
            String line = null;
            String seqname = null;
            String seq = "";
            long offset = 0;
            float percdone = 0;
            int lines_processed = 0;
            boolean convertThis = true;
            while((line = fileIn.readLine()) != null)
            {
                if(line.indexOf(">") > -1)
                {
                    // Found another sequence. Remember its name
                    seqname = line.substring(1).split("\\s+")[0];
                    // Do not convert sequences for which there is no length specified
                    convertThis = chromlens.containsKey(seqname) ? true : false;
                    if(convertThis)
                    {
                        wrtr.createGroup(seqname);
                        wrtr.createByteArray("/"+seqname +"/sequence", chromlens.get(seqname)); // adjust length to chromosome
                        System.out.println("Converting sequence "+seqname);
                    }
                    else
                    {
                        System.out.println("Skipping sequence "+seqname);
                    }
                    offset = 0;
                    lines_processed = 0;
                    continue;
                }
                byte [] data = line.getBytes();
                if(! convertThis) { continue; }
                wrtr.writeByteArrayBlockWithOffset("/" + seqname +"/sequence", data, data.length, offset);
                offset += data.length;
                data = null;
                lines_processed++;
                
                if(lines_processed % 100000 == 0)
                {
                    percdone = 100*offset/chromlens.get(seqname);
                    System.out.println("% done: " + percdone);
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("Could not handle fasta conversion: " + e);
        }
    }
    
    public void close()
    {
        wrtr.close();
        System.gc();
    }
    
}
