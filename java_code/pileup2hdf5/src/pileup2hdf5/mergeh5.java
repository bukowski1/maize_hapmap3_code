/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pileup2hdf5;

import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import ch.systemsx.cisd.hdf5.HDF5DataSetInformation;
import java.io.PipedInputStream;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;

/**
 *
 * @author bukowski
 */
public class mergeh5 {
    
    /*
    * Merge two  HDF5 files in compressed format (index+depth+qual) into
    * the third file.
    */
    public static void merge_compr_h5(String infile1, String infile2, String outfile)
    {
        // Open readers and retrieve dimension information from the two files to be merged
        
        IHDF5Reader reader1 = HDF5Factory.openForReading(infile1);
        //get sample name
        java.util.List<java.lang.String> groups = reader1.getGroupMembers("/");
        String sampleName1 = groups.get(0);  
        groups = reader1.getGroupMembers("/"+sampleName1);
        String chromName1 = groups.get(0);
 
        HDF5DataSetInformation ddd =  reader1.getDataSetInformation("/" + sampleName1 + "/" + chromName1 + "/Index");
        long [] dims = ddd.getDimensions();
        long index_length1 = dims[1];
        ddd = reader1.getDataSetInformation("/" + sampleName1 + "/" + chromName1 + "/Depth");
        dims = ddd.getDimensions();
        long depth_length1 =  dims[1];
        
        IHDF5Reader reader2 = HDF5Factory.openForReading(infile2);
        //get sample name
        groups = reader2.getGroupMembers("/");
        String sampleName2 = groups.get(0);  
        groups = reader1.getGroupMembers("/"+sampleName1);
        String chromName2 = groups.get(0);
 
        ddd =  reader2.getDataSetInformation("/" + sampleName2 + "/" + chromName2 + "/Index");
        dims = ddd.getDimensions();
        long index_length2 = dims[1];
        ddd = reader2.getDataSetInformation("/" + sampleName2 + "/" + chromName2 + "/Depth");
        dims = ddd.getDimensions();
        long depth_length2 =  dims[1];
        
        System.out.println("First file: "+infile1 + ", Sample: "+sampleName1+", chromosome: "+chromName1);
        System.out.println("Index dimension: "+index_length1 + ", depth dimension: " + depth_length1);
        System.out.println("Second file: "+infile2 + ", Sample: "+sampleName2+", chromosome: "+chromName2);
        System.out.println("Index dimension: "+index_length2 + ", depth dimension: " + depth_length2);
        
        if(! sampleName1.equals(sampleName2) || ! chromName1.equals(chromName2))
        {
            System.out.println("ERROR: taxa and/or chromosomes in the two files are inconsistent - exiting");
            reader1.close();
            reader2.close();
            return;
        }
        
        long index_length_tot = index_length1 + index_length2;
        long depth_length_tot = depth_length1 + depth_length2;
        
        // Open the merged file for writing with proper dimensions
        
        PipedInputStream pis=null; // needed only to instantiate h5CompressedWriter
        int chrlen = h5writereference.chromosome_lengths(3).get(chromName1);
        h5CompressedWriter h5cwrt = new h5CompressedWriter(pis,outfile,sampleName1,chromName1,chrlen,1,(int)index_length_tot,3);
        
        // Time to read and save in merged HDF5 datasets
        int chrGID = h5cwrt.getChrGID();
        int indexDatasetID = h5cwrt.create_dataset("Index",chrGID, (int)index_length_tot, 1);
        int depthDatasetID = h5cwrt.create_dataset("Depth",chrGID, (int)depth_length_tot, 1);
        int qualDatasetID = h5cwrt.create_dataset("Quals",chrGID, (int)depth_length_tot, 1);
        int FileSpaceID,  MemorySpaceID;
        
        // Read and write index from the first file
        byte [][] outmat_byte= reader1.readByteMatrixBlockWithOffset("/" + sampleName1 + "/" + chromName1 +"/Index", 1, (int)index_length1, 0, 0);
        FileSpaceID = H5.H5Dget_space (indexDatasetID);
        long[] count= new long[] {1, index_length1};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
        H5.H5Dwrite(indexDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
        H5.H5Sclose(MemorySpaceID);
        // Read and write index from the second file
        outmat_byte= null;
        outmat_byte= reader2.readByteMatrixBlockWithOffset("/" + sampleName2 + "/" + chromName2 +"/Index", 1, (int)index_length2, 0, 0);
        count= new long[] {1, index_length2};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, index_length1}, null, count, null);
        H5.H5Dwrite(indexDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
            
        System.gc();
        outmat_byte = null;
        
        H5.H5Sclose(MemorySpaceID);
        H5.H5Sclose(FileSpaceID);
        H5.H5Dclose(indexDatasetID);
            
        // Read and write depths from the first file
        outmat_byte= reader1.readByteMatrixBlockWithOffset("/" + sampleName1 + "/" + chromName1 +"/Depth", 1, (int)depth_length1, 0, 0);
        FileSpaceID = H5.H5Dget_space (depthDatasetID);
        count= new long[] {1, depth_length1};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
        H5.H5Dwrite(depthDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
        H5.H5Sclose(MemorySpaceID);
        // Read and write depths from the second file
        outmat_byte= reader2.readByteMatrixBlockWithOffset("/" + sampleName2 + "/" + chromName2 +"/Depth", 1, (int)depth_length2, 0, 0);
        count= new long[] {1, depth_length2};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, depth_length1}, null, count, null);
        H5.H5Dwrite(depthDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
        
        System.gc();
        H5.H5Sclose(MemorySpaceID);
        H5.H5Sclose(FileSpaceID);
        H5.H5Dclose(depthDatasetID);
            
       // Read and write quals from the first file
        outmat_byte= reader1.readByteMatrixBlockWithOffset("/" + sampleName1 + "/" + chromName1 +"/Quals", 1, (int)depth_length1, 0, 0);
        FileSpaceID = H5.H5Dget_space (qualDatasetID);
        count= new long[] {1, depth_length1};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
        H5.H5Dwrite(qualDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
        H5.H5Sclose(MemorySpaceID);
        // Read and write quals from the second file
        outmat_byte= reader2.readByteMatrixBlockWithOffset("/" + sampleName2 + "/" + chromName2 +"/Quals", 1, (int)depth_length2, 0, 0);
        count= new long[] {1, depth_length2};
        MemorySpaceID = H5.H5Screate_simple(2, count, null);
        H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, depth_length1}, null, count, null);
        H5.H5Dwrite(qualDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, outmat_byte);
        H5.H5Sclose(MemorySpaceID);
        
        System.gc();
        H5.H5Sclose(FileSpaceID);
        H5.H5Dclose(qualDatasetID);
           
        h5cwrt.closeFile();
        
        reader1.close();
        reader2.close();
    }
    
}
