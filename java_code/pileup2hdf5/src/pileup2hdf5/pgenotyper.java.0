/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.util.Arrays;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.*;
import org.apache.commons.lang.ArrayUtils;
import net.sf.javaml.utils.*;

/**
 *
 * @author bukowski
 */
public class pgenotyper implements Runnable {
    ArrayList<String> SampleList = new ArrayList<String>(); // will keep the names of sample h5 mpileup files
    int numSamples;
    private int[][][] data = null;
    private String [] alleles = {"A", "C", "G", "T", "I", "D"};
    private Hashtable<String, Integer> allelepos = new Hashtable<String,Integer>(); 
    private String chrom;
    private int chromlen;
    private int startRange;
    private int endRange;
    private int startMain;
    private byte[] refsequence;
    private BufferedWriter bw;
    private genotyper_params gpars;
    double e = 0.05;
    double p_homo = -0.018483406; // log10(1 - 5*e/6);
    double p_het = -0.315753252;  //  log10(0.5 -e/3);
    double p_rest = -2.079181246;
    private int[] filter_count = new int[10];
    
    public pgenotyper(String chromIn, int startRangeIn, int endRangeIn, int startMainIn, ArrayList<String> SampleListIn, byte[] refsequenceIn, int[][][] dataIn, genotyper_params gparams)
    {
        chrom = chromIn;
        // Initialize the allele positions in extracted mpileup file
        allelepos.put("A", 0);
        allelepos.put("C", 1);
        allelepos.put("G", 2);
        allelepos.put("T", 3);
        allelepos.put("I", 4);
        allelepos.put("D", 5);
        
        // Load chromosome length
        chromlen = h5writereference.chromosome_lengths().get(chrom);
        
        SampleList = SampleListIn;
        
        numSamples = SampleList.size();
        
        refsequence = refsequenceIn;
        data = dataIn;
        
        startRange = startRangeIn;
        endRange = endRangeIn;
        if(endRange > chromlen)
        {
            endRange = chromlen;
        }
        startMain = startMainIn;
        
        gpars = gparams;
        
        // Set the probability logs for multinomial genotyper
        p_homo = Math.log10(1-5*gpars.getE()/6);
        p_het = Math.log10(0.5 - gpars.getE()/3);
        p_rest = Math.log10(gpars.getE()/6);
        
        // Initialize filter count
        Arrays.fill(filter_count, 0);
                
    }
    
    public void run()
    {
        long startTime;
        long endTime;
        // Open output file for thread
        try
        {
            bw = new BufferedWriter(new FileWriter("thr.c"+chrom+"."+startRange+"-"+endRange));
        }
        catch(Exception e)
        {
            System.out.println("output file could not be open"+e);
        }
        System.out.println("Thread " + startRange + "-" + endRange + " starting");
        startTime = System.currentTimeMillis();
        genotype_range();
        endTime = System.currentTimeMillis();
        double duration = 0.001*(endTime - startTime)/60;
        System.out.println("Thread " + startRange + "-" + endRange + " took "+ duration +" minutes");
        try
        {
            bw.close();
        }
        catch(Exception e)
        {
            System.out.println("File could not be closed" + e);
        }
        
        // Print filter stats
        print_filter_stats();
    }
    
    public void genotype_range()
    {
        // depth-based thresholds
        int nonzero_cov_thr = gpars.getNozero_cov_thr(); // sample considered to have nonzero coverage if its total depth is larger that this
        int nonzero_all_cov_thr = gpars.getNozero_all_cov_thr(); //allele is considered to occur in a sample if its depth is larger than this
        int numaltall_max = gpars.getNumaltall_max();  // maximum number of retained alternative alleles
        int all_retension_thr = gpars.getAllele_retension_thr(); // keep allele if its combined depth over samples is at least this
        double minfracsampcov = gpars.getMinfracsampcov(); // minimum fraction of samples with coverage to include the site
        double pvalthr = gpars.getPvalthr();    // site rejected if pvalue of contingency table larger than this (not used)
        int cntg_thr = gpars.getCntg_thr();        // sample will be part of contingency table test if reference+1st_minor allele depth is greater than this
        int depth_retained_alleles_thr = gpars.getDepth_retained_alleles_thr();    // sample will NOT be used in Ed's factor tests if depth over all retained alleles is smaller than this
        int depth_alt_thr = gpars.getDepth_alt_thr(); // overall depth of minor alleles has to be bigger than this to keep the position
        int max_depth_alt_thr =gpars.getMax_depth_alt_thr(); // total depth must be at least this in at least one sample with alt genotype to keep the position even if number of alt alleles too low
        // genotype-based thresholds
        int samps_with_ret_all_thr = gpars.getSamps_with_ret_all_thr(); // site rejected if number of samples with genotypes 0/0 + 0/1 + 1/1 smaller than this
        int nall_after_genotyping_thr = gpars.getNall_after_genotyping_thr(); // retain at most this many alleles after genotyping
        double minor_num_thr = gpars.getMinor_num_thr(); // site kept if number of minor allele more than this
        double edfactor_reject_thr = gpars.getEdfactor_reject_thr();  // site will be rejected if Ed's factor greater than this (0.1 value is  arbitrary) 
        
        boolean ishet = false;   // to be used in counting hets for Ed's factor
        //int[][] gnum_mat = new int[6][6]; // to hold the number of samples of different genotypes
        
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int numsites = endRangeNorm - startRange + 1;
        int [][] totdepth = new int[numsites][numSamples];
        Hashtable<String,Integer> sampcount = new Hashtable<String,Integer>();
        int posoffset = startRange - startMain;
        
        // Test printout for sanity check and/or compute total depth
        for(int i=0;i<numSamples;i++)
        {
            //System.out.println("Sample " + SampleList.get(i));
            for(int k=0;k<numsites;k++)
            {
                totdepth[k][i] = 0;
                for(int j=0;j<6;j++)
                {
                    totdepth[k][i] += data[i][j][k+posoffset];
                    //System.out.print(data[i][j][k+posoffset] + "\t");
                }
                //System.out.print("\n");
            }
        }
        
        // Print the first line
        if(startRange == 1)
        {
        String firstline = "#CHR\tPOS\tREF\tALT\tINFO";
        for(int i=0;i<SampleList.size();i++)
        {
            firstline += "\t"+SampleList.get(i);
        }
        try
        {
            bw.write(firstline+"\n");
        }
        catch(Exception e)
        {
            System.out.println("Thread could not write first line to file"+e);
        }
        }
        
        // Loop over sites
        for(int pos=0;pos<numsites;pos++)
        {
            // rewrite data for position pos into a new local table - maybe it will be fastr
            /*
            int [][] data1 = new int[numSamples][6];
            for(int i=0;i<numSamples;i++)
            {
                for(int j=0;j<6;j++)
                {
                    data1[i][j] = data[i][j][pos+posoffset];
                }
            }
             * 
             */
            
            String infostr = "";  // will contain various info about filters etc.
            
            String refbase = Character.toString((char)refsequence[pos+posoffset]).toUpperCase();
            int pos_actual = pos + startRange; // Actual positiono on the chromosome
            
            // Skip the site if reference unknown - maybe this is not right
            if(refbase.equalsIgnoreCase("N"))
            {
                filter_count[0]++;
                continue;
            }
            // Find total depth over all samples and number of samples with non-zero coverage
            int totcov = 0;
            int samplesnonzerocov = 0;
            for(int i=0;i<numSamples;i++)
            {
                totcov += totdepth[pos][i];
                if(totdepth[pos][i] > nonzero_cov_thr) { samplesnonzerocov++; }
            }
            // Filter positions with no coverage
            if(totcov == 0) { filter_count[1]++; continue; }
            // Filter positions with coverage in less than threshold% of samples
            if(samplesnonzerocov < minfracsampcov*numSamples) { filter_count[2]++; continue; }
            infostr += "DP="+totcov+";NZ="+samplesnonzerocov+";";
            
            // For each allele, find the number of samples it "occurs" in
            // allele "occurs" in sample if its depth in this sample is greater than nonzero_all_cov_thr
            for(int ia=0;ia<6;ia++)
            {
                sampcount.put(alleles[ia], 0);
            }
            for(int i=0;i<numSamples;i++)
            {
                for(int ia=0;ia<6;ia++)
                {
                    if(data[i][allelepos.get(alleles[ia])][pos+posoffset] > nonzero_all_cov_thr)
                    {
                        sampcount.put(alleles[ia], sampcount.get(alleles[ia]) + 1);
                    }
                }
            }
            
            // Sort aleles according to the number of occurrence accross all samples
            final int [] allele_nums = {sampcount.get("A"), sampcount.get("C"), sampcount.get("G"),sampcount.get("T"),sampcount.get("I"),sampcount.get("D") };
            ArrayList<Integer> index = new ArrayList<Integer>();
            for(int ia=0;ia<6;ia++)
            {
                index.add(ia);
            }
            
            // Construct the comparator to sort in reverse order
            Comparator<Integer> cmp = new Comparator<Integer>(){
                @Override
                public int compare(Integer a, Integer b){
                    return allele_nums[b]-allele_nums[a];
            }};
            
            Collections.sort(index, cmp);
           
            // Save sorted version of alleles and allele_nums
            String [] alleles1 = new String [6];
            int [] allele_nums1 = new int [6];
            for(int ia=0;ia<6;ia++)
            {
                alleles1[ia] = alleles[index.get(ia)];
                allele_nums1[ia] = allele_nums[index.get(ia)];
            }
            
            // Test printout
            /*
            System.out.println("initial alleles: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles[ia] + ":" + allele_nums[ia] +"\t");
            }
            System.out.print("\n");
            System.out.println("sorted alleles: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles1[ia] + ":" + allele_nums1[ia] +"\t");
            }
            System.out.print("\n");
             * 
             */
            
            // Construct the alternative allele string. Retain only up to numaltall_max alternative alleles
            String allele_str = "";
            int numaltall = 0;
            for(int ia=0;ia<6;ia++)
            {
                if(allele_nums1[ia] >= all_retension_thr && numaltall < numaltall_max)
                {
                    if(! alleles1[ia].equals(refbase)) 
                    { 
                        allele_str += alleles1[ia] + ",";
                        numaltall++;
                    }
                }
                else
                {
                    break;
                }
            }
            
            // If no variation detected, proceed to the next position
            // (if no reference base given all positions with non-zero coverage will be written
            if(numaltall == 0)
            {
                filter_count[3]++;
                continue;     // continue the loop over positions
            }
            // This allele_str will not be used if the final, genotype-based sorting is in effect
            allele_str = allele_str.substring(0, allele_str.length()-1);  // remove trailing comma
            
            // Start building the output line for this position
            StringBuilder outstrbld = new StringBuilder();
            String outstr = chrom + "\t" + pos_actual + "\t" + refbase + "\t";
            //outstrbld.append(outstr);
            
            // Generate allele depths and add this info to the output string
            int samples_for_cntg = 0;        // count samples to include in contingency table (now not used)
            int samps_with_ret_all = 0;    // count samples with depth in retained alleles (for Ed's factor)
            int num_of_hets = 0;
            int num_minor_all = 0;
            int depth_alt_all = 0;           // count combined depth of all non-reference alleles
            String depth_alt_all_str = "";
            String genallele_nums_str = "";
            String gntp_nums_str = "";
            int [][] num_mat = new int[numSamples][6];  // sample-wise allele depths - kept only for reference nad retained alt alleles
            int [][] lkhd_mat = new int [numSamples][21];  // hold normalized likelihoods, 21 = 6*(6+1)/2
            String [] genotype_mat = new String [numSamples]; // To hold genotypes for final re-sorting
            int [] depth_alt = {0,0,0,0,0,0};
            int [] genallele_nums = {0,0,0,0,0,0};   // Numbers of alleles in genotypes
            int [][] gnum_mat = new int[6][6];
            zero2Dmat(gnum_mat);
            int max_depth_alt = 0;    // record max depth amog all samples with genotypes containin any alt alleles
            for(int i=0;i<numSamples;i++)
            {
                int matcount = 0;
                // Add depth of reference base as the first element - once reference base is known
                num_mat[i][matcount] = data[i][allelepos.get(refbase)][pos+posoffset];
                int depth_retained_alleles = num_mat[i][matcount];
                depth_alt[matcount] += num_mat[i][matcount];
                String depth_str = String.valueOf(num_mat[i][matcount]) + ",";
                //outstrbld.append(num_mat[i][matcount]).append(",");
                matcount++;
                for(int ia=0;ia<6;ia++)
                {
                    if(matcount < numaltall + 1)
                    {
                        if(! alleles1[ia].equals(refbase))
                        {
                            num_mat[i][matcount] = data[i][allelepos.get(alleles1[ia])][pos+posoffset];
                            depth_retained_alleles += num_mat[i][matcount];
                            depth_alt_all += num_mat[i][matcount];
                            depth_alt[matcount] += num_mat[i][matcount];
                            //outstrbld.append(num_mat[i][matcount]).append(",");
                            depth_str += String.valueOf(num_mat[i][matcount]) + ",";
                            matcount++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //outstrbld.deleteCharAt(outstrbld.length()-1).append("\t");
                
                // If depth is too low over all retained alleles, mark this sample "missing data" and do not bother genotyping it
                // This filter is neded because some samples may have depth only in skipped alleles
                if(depth_retained_alleles < Math.max(depth_retained_alleles_thr, 1))
                {
                    //outstrbld.append("./.\t");
                    genotype_mat[i] = "./.";
                    continue;
                }
                if(num_mat[i][0] + num_mat[i][1] > cntg_thr) samples_for_cntg++; // now defunct
                
                samps_with_ret_all++;              // This sample counts as having retained alleles
                
                // Add genotype likelihood calculations here
                // This should actually be done in a separate loop after contingency table test (if any)
                // Will keep it here for now - relevant for Ed's factor test
                ArrayList<Double> lkhds = new ArrayList<Double>();
                double lkh_max = -9999999;
                String genotype = "0/0";
                int ig1 = 0;
                int ig2 = 0;
                for(int ia1=0;ia1<matcount;ia1++)
                {
                    for(int ia2=ia1;ia2<matcount;ia2++)
                    {
                        double lll = genotype_likelihood(ia1,ia2,num_mat[i], matcount);
                        lkhds.add(lll);
                        if(lll > lkh_max)
                        {
                            lkh_max = lll;
                            ig1 = ia1;
                            ig2 = ia2;
                            genotype = String.valueOf(ia1) + "/" + String.valueOf(ia2);
                            if(ia1 == ia2)
                            {
                                ishet = false;
                            }
                            else
                            {
                                ishet = true;
                            }
                        }
                        
                    }
                }
                genallele_nums[ig1]++;
                genallele_nums[ig2]++;
                gnum_mat[ig1][ig2]++;
                if(ig1 != ig2) { gnum_mat[ig2][ig1]++; } // making this symmetric is important for subsequent resorting
                genotype_mat[i] = genotype;
                
                // Update numbers needed for Ed's factor test
                if(! genotype.equals("0/0"))
                {
                    if(ishet)
                    {
                        num_of_hets++;
                        if(genotype.indexOf("0") > -1)
                        {
                            num_minor_all++; // hets with one alt allele, like 0/1, 0/2, ...
                        }
                        else
                        {
                            num_minor_all += 2;  // hets with two alt alleles, like 1/2, 2/3, ....
                        }
                    }
                    else
                    {
                        num_minor_all += 2;      // homozygotes with alt alleles
                    }
                    // Update mex depth among samples with genotypes containing any alt allele
                    if(totdepth[pos][i] > max_depth_alt)
                    {
                        max_depth_alt = totdepth[pos][i];
                    }
                }
                
                
                // Normalize likelihoods and append a string to display
                String lkhdstr = "";
                for(int ia=0;ia<lkhds.size();ia++)
                {
                    int lll = (int)(-10*(lkhds.get(ia) - lkh_max) + 0.5);
                    lkhd_mat[i][ia] = lll;
                    lkhdstr += String.valueOf(lll) + ",";
                }
                lkhdstr = genotype + ":" + depth_str.substring(0,depth_str.length()-1) + ":" + lkhdstr.substring(0, lkhdstr.length()-1);
                
                //outstrbld.append(lkhdstr).append("\t");
                
            }  // loop over samples (computing depths and genotype likelihoods
            
            // Skip if not enough overall combined depth in alternative alleles
            // Note: depth in alt alleles does not mean a lot of genotypes containing them
            // e.g., if overall depth is high
            if(depth_alt_all < depth_alt_thr) { filter_count[4]++; continue; }
            // Skip if not enough samples with retained alleles
            if(samps_with_ret_all < samps_with_ret_all_thr) { filter_count[5]++; continue; }
            // Skip if not enough of minor alleles found and genotype not well suported by reads in any sample
            if(num_minor_all <= minor_num_thr && max_depth_alt < max_depth_alt_thr) { filter_count[6]++; continue; }
            
            // Sort alternative alleles according to genotyping results (allele frequencies)
            //
            final int [] genallele_nums_f = Arrays.copyOf(genallele_nums, 6);
            int [][] gnum_mat_f = clone2Dmat(gnum_mat);
            index = new ArrayList<Integer>();
            for(int ia=1;ia<6;ia++)
            {
                index.add(ia);
            }
            cmp = new Comparator<Integer>(){
                @Override
                public int compare(Integer a, Integer b){
                    return genallele_nums_f[b]-genallele_nums_f[a];
            }};
            Collections.sort(index, cmp);
            index.add(0, 0); // this way, 0 (ref allele) remains in the beginning.
            int numaltall_old = numaltall;
            numaltall = 0;
            genallele_nums[0] = genallele_nums_f[0];
            for(int ia=1;ia<6;ia++)
            {
                genallele_nums[ia] = genallele_nums_f[index.get(ia)];
                if(genallele_nums[ia]>0) { numaltall++; }
            }
            
            // Skip if no alt allele found after genotyping
            if(numaltall <1) { filter_count[7]++; continue; }
            
            // Limit the num ber of alleles retained after genotyping
            numaltall = Math.min(numaltall, nall_after_genotyping_thr);
            
            // Numbers of samples with various genotypes
            zero2Dmat(gnum_mat);
            //for(int ia1=0;ia1<=numaltall;ia1++)
            //{
            //    for(int ia2=ia1;ia2<=numaltall;ia2++)
            //    {
            //        gnum_mat[ia1][ia2] = gnum_mat_f[index.get(ia1)][index.get(ia2)];
            //    }
            //}
            //
            // Here we go over all samples,
            // remove any alleles that did not survive genotyping, and generate sample strings.
            // Limiting the number of retained alleles may change genotypes, so arrays gnum_mat and
            // genallele_nums need to be regenerated....
            Arrays.fill(genallele_nums, 0);
            String smptmp = "";
            String [] gntmp;
            int im1=0;
            int im2=0;
            for(int i=0;i<numSamples;i++)
            {
                smptmp = makeSampleStr(genotype_mat[i],num_mat[i],lkhd_mat[i],index,numaltall, numaltall_old);
                outstrbld.append(smptmp).append("\t");
                if(smptmp.equals("./.")) { continue; }
                
                gntmp = smptmp.split(":")[0].split("/");
                im1= Integer.valueOf(gntmp[0]);
                im2 = Integer.valueOf(gntmp[1]);
                genallele_nums[im1]++;
                genallele_nums[im2]++;
                gnum_mat[im1][im2]++;
            }
            
            
            //
            // end of the re-sorting part

            // New Allele string
            String [] old_alt_alleles = allele_str.split(",");
            allele_str = "";
            for(int ia=1;ia<=numaltall;ia++)
            {
                allele_str += old_alt_alleles[index.get(ia)-1]+",";
            }
            allele_str = allele_str.substring(0, allele_str.length()-1);
            
            // Generate INFO string with alleles re-arranged after genotyping
            depth_alt_all_str = "";
            String index_str = "";
            for(int i=0;i<=numaltall;i++)
            {
                depth_alt_all_str += depth_alt[i] +",";
                genallele_nums_str += genallele_nums[i] + ",";
                index_str += index.get(i) + ",";
            }
            depth_alt_all_str = depth_alt_all_str.substring(0, depth_alt_all_str.length()-1);
            infostr += "AD="+depth_alt_all_str+";";
            genallele_nums_str = genallele_nums_str.substring(0, genallele_nums_str.length()-1);
            infostr += "AN="+genallele_nums_str+";";
            index_str = index_str.substring(0, index_str.length()-1);
            infostr += "IN="+index_str+";";
            
            for(int i=0;i<=numaltall;i++)
            {
                for(int j=i;j<=numaltall;j++)
                {
                    gntp_nums_str += gnum_mat[i][j]+","; 
                }
            }
            gntp_nums_str = gntp_nums_str.substring(0, gntp_nums_str.length()-1);
            infostr += "GN="+gntp_nums_str+";";
            infostr += "SR="+samps_with_ret_all+";";
            infostr += "HT="+num_of_hets+";";
            
            // Compute overall Ed's factor = Het_freq/(present_freq*minor_allale_freq)
            double edfactor = 0;
            if(num_minor_all > 0)
            {
                edfactor = 2*(double)num_of_hets*numSamples/(samps_with_ret_all*num_minor_all);
            }
            //System.out.println("Ed factor: "+pos_actual+" "+edfactor + " " + num_of_hets + " "+ samps_with_0or1_data + " " + num_minor_all);
            // Compute Ed's factor based on alleles 0 and 1 only
            double edfactor01 = 0;
            if(num_minor_all > 0)
            {
                edfactor01 = 2*(double)gnum_mat[0][1]*numSamples/((gnum_mat[0][0]+gnum_mat[0][1]+gnum_mat[1][1])*genallele_nums[1]);
            }
            
            // Skip if Ed's factor (from alleles 0 and 1 only) is greater than threshold
            if(edfactor01 > edfactor_reject_thr) { filter_count[8]++; continue; }
            infostr += "EF="+(double)Math.round(100*edfactor01)/100;
            if(numaltall >1)
            {
                infostr += ","+(double)Math.round(100*edfactor)/100;
            }
            
            outstr += allele_str + "\t" + infostr+"\t";
            outstrbld.deleteCharAt(outstrbld.length()-1).append("\n");
            outstrbld.insert(0, outstr);
            
            // Completely skip the contingency table stuff for now
            /*
            // Test the contingency table
            // First, construct the table, just for two main alleles
            double [][] conttab = new double[2][samples_for_cntg];
            int samp_counter = 0;
            int rowtot1 = 0;
            int rowtot2 = 0;
            for(int i=0;i<numSamples;i++)
            {
                if(num_mat[i][0] + num_mat[i][1] > cntg_thr)
                {
                    conttab[0][samp_counter] = num_mat[i][0];
                    conttab[1][samp_counter] = num_mat[i][1];
            
                    rowtot1 += conttab[0][samp_counter];
                    rowtot2 += conttab[1][samp_counter];
                    
                    samp_counter++;
                }
            }
            if(rowtot1 == 0 || rowtot2 == 0)
            {
                System.out.println("Rowtot zero for popsition "+pos_actual + ": "+rowtot1 + "," + rowtot2 + "," + allele_str);
                System.out.println("sampcounter: "+samp_counter+", "+samples_for_cntg);
            }
      
            // Now get the p-value of the contingency table
            double pvalue = 0;
            if(rowtot1>0)  // Otherwise kepp pvalue=0 to include the position, which is not segregating, but different from reference
            {
                if(rowtot2>0 && samples_for_cntg>1) // may be zero if we require total ref+major depth>1 to include sample in cont. table
                {
                try
                {
                ContMatPack cntmatobj = new ContMatPack(conttab);
                    //if(ContingencyTables.cochransCriterion(conttab))
                    if(true)    
                    {
                        // pvalue = ContMatPack.pvalue(conttab);
                        pvalue = ContingencyTables.chiSquared(conttab, false);
                        //double pvalsim = ContMatPack.SimulatePvalueHG(conttab, 1000);
                        double pvalsim = cntmatobj.hypergeomLratio(conttab);
                        //pvalue = cntmatobj.pvalueG(conttab);
                        //double pvalsim = cntmatobj.SimulatePvalueG(conttab, 1000);
                        //double hyprgeom = ContingencyTables.log2MultipleHypergeometric(conttab);
                        //hyprgeom = Math.pow(0.5,hyprgeom);
                        System.out.println("NOTsw pos chisq hypgeom sampc rows: " + pos_actual + " " +pvalue + " "+pvalsim + " "+samples_for_cntg + " "+rowtot1+" "+rowtot2);
                    }
                    else
                    {
                        double pvaluechsq = ContingencyTables.chiSquared(conttab, false);
                        pvalue = ContMatPack.SimulatePvalueHG(conttab, 1000);
                        //double pvaluechsq = cntmatobj.pvalueG(conttab);
                        //pvalue = cntmatobj.SimulatePvalueG(conttab, 1000);
                        //double hyprgeom = ContingencyTables.log2MultipleHypergeometric(conttab);
                        //hyprgeom = Math.pow(0.5,hyprgeom)
                        System.out.println("Sw pos chisq sim sampc rows: " + pos_actual + " " +pvaluechsq + " "+pvalue + " "+samples_for_cntg + " "+rowtot1+" "+rowtot2);
                    }
                }
                catch(Exception e)
                {
                    System.out.println("ConotMatPack error at position " + pos_actual);
                    System.out.println("Rowtot: "+rowtot1 + "," + rowtot2 + "," + allele_str +" sampcounter:" + samp_counter);
                    System.out.println(e);
                }
                }
                else
                {
                    // Will hold if all sample included in contigency table have only ref allele
                    // and/or id there is only one sample in the cont. table. We may want to change this after discussion 
                    pvalue = 1;
                }
            }
            conttab = null;
            
            // Skip if pvalue too big
            if(pvalue > pvalthr)
            {
                //System.out.println("Skipping position " + pos_actual + " with pvalue " + pvalue);
                continue;
            }
             * 
             */
            
            //System.out.print(outstr)
            outstr = outstrbld.toString();
            try
            {
                bw.write(outstr);
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write to file"+e);
            }
                
       } // loop over positions
        
    }   // end of genotype_range
    
    private double genotype_likelihood(int ia1, int ia2, int[] num_mat, int matcount)
    {
        double lkh = 0;
        
        // The parameters below are set in the constructor base on e provided in xml parameter file.
        //double e = 0.05;
        //double p_homo = -0.018483406; // log10(1 - 5*e/6);
        //double p_het = -0.315753252;  //  log10(0.5 -e/3);
        //double p_rest = -2.079181246;  // log10(e/6); 
        
        if(ia1 == ia2)
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1)
                {
                    lkh += num_mat[i]*p_homo;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        else
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1 || i==ia2)
                {
                    lkh += num_mat[i]*p_het;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        
        return lkh;
    }
    
    private static void zero2Dmat(int[][] mat)
    {
        for(int i=0;i< mat.length;i++)
        {
            Arrays.fill(mat[i], 0);
        }
    }
    
    private static int [][] clone2Dmat(int[][] mat)
    {
        int [][] res = new int[mat.length][mat[0].length];
        for(int i=0;i<mat.length;i++)
        {
            res[i] = mat[i].clone();
        }
        return res;
    }
    
    // Construct the genotype/depth/likelihood string for a given sample
    // reshuffling the input arrays according to the result of final re-sorting (index) 
    private static String makeSampleStr(String gnt, int[] depths, int[] lkhds, ArrayList<Integer> index, int numaltall, int numaltall_old)
    {
        String result = "";
        String depth_str = "";
        String lkhd_str = "";
        int newi;
        int newj;
        int newloc;
        
        if(gnt.equals("./.")) { return "./."; }
        
        // Allele epths
        for(int i=0;i<= numaltall;i++)
        {
            depth_str += depths[index.get(i)]+",";
        }
        depth_str = depth_str.substring(0, depth_str.length()-1);
        
        // Genotype likelihoods (pthreaded already)
        // In case an existing genotype is ingored (because the number of retained
        // alleles changed after genotyping), we need to regenotype such samples
        // i.e., find the new best genotype and normalize the others to it.
        int [] lkh_new = new int [(numaltall+1)*(numaltall+2)/2];
        int lmin = 9999999;
        int counter = 0;
        int im1=0;
        int im2=0;
        for(int ia1=0;ia1<=numaltall;ia1++)
        {
            for(int ia2=ia1;ia2<=numaltall;ia2++)
            {
                newi = Math.min(index.get(ia1),index.get(ia2))+1;
                newj = Math.max(index.get(ia1),index.get(ia2))+1;
                newloc = (newi -1)*(2*(numaltall_old+1)-newi)/2 + newj -1; // need to use old d
                //lkhd_str += lkhds[newloc] + ",";
                lkh_new[counter] = lkhds[newloc];
                if(lkhds[newloc] < lmin) { lmin = lkhds[newloc]; im1 = ia1; im2 = ia2;}
                counter++;
            }
        }
        
        for(int ia=0;ia<lkh_new.length;ia++)
        {
            lkh_new[ia] -= lmin;
            lkhd_str += lkh_new[ia] + ",";
        }
        lkhd_str = lkhd_str.substring(0, lkhd_str.length()-1);
        result += im1 + "/" + im2 + ":" + depth_str + ":" + lkhd_str; 
        
        return result;
    }
    
    private void print_filter_stats()
    {
       String header = "Thread:" + startRange + "-" + endRange+":";
       System.out.println(header+"Filter Statistics");         
       System.out.println(header+"Reference is N:"+filter_count[0]);
       System.out.println(header+"Zero total coverage:"+filter_count[1]);
       System.out.println(header+"Not enough taxa with coverage:"+filter_count[2]);
       System.out.println(header+"No variation before genotyping:"+filter_count[3]);
       System.out.println(header+"Not enough depth in alternative alleles:"+filter_count[4]);
       System.out.println(header+"Not enough samples with retained alleles:"+filter_count[5]);
       System.out.println(header+"not enough of minor alleles found and genotype not well suported by reads in any sample:"+filter_count[6]);
       System.out.println(header+"No variation after genotyping:"+filter_count[7]);
       System.out.println(header+"Ed's factor too large:"+filter_count[8]);
    }
    
}
