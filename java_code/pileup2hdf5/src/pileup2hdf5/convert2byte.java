/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 *
 * @author bukowski
 */
public class convert2byte {
    
    public static void convert(String inhdf5, String outhdf5, String chrom, String samplename)
    {
        int position=1;
        String allnumbers = "";
        // chrlen should be supplied based on e fai file....
        int chrlen = h5writereference.chromosome_lengths(2).get(chrom);
        int startRange = 1;
        h5reader h5rdr = new h5reader(inhdf5,chrom, 2, chrlen);
        String qcfile = inhdf5.replace(".h5", "_QC.h5");
        h5reader h5rdrqc = new h5reader(qcfile,chrom, 2, chrlen);
        
        h5rdr.get_range(startRange, chrlen, false);  // tnis will cap endRange, if needed
        h5rdrqc.get_range(startRange, chrlen, false);
        
        try
        {
            PipedOutputStream pos = new PipedOutputStream();
        //postreams.add(pos);
            PipedInputStream pis = new PipedInputStream(pos);
        //pistreams.add(pis);
            BufferedWriter bwrt = new BufferedWriter(new OutputStreamWriter(pos));
        //bwriters.add(bwrt);
            //h5writer h5wrt = new h5writer(pis, outhdf5, samplename, chrom, 2);
            h5CompressedWriter h5wrt = new h5CompressedWriter(pis, outhdf5, samplename, chrom, chrlen, 2);
            Thread thr = new Thread(h5wrt);
            thr.start(); // This should now wait for input
            
            // We are now ready to write input into bwrt
            for(int i=0;i<chrlen;i++)
            {
                int sum = 0;
                for(int j=0;j<6;j++)
                {
                    sum += h5rdr.outmat[j][i];   
                }
                if(sum == 0) { continue; }
                
                position = i+1;
                allnumbers = h5rdr.outmat[0][i] + "\t" + h5rdr.outmat[1][i] + "\t" + h5rdr.outmat[2][i];
                allnumbers += "\t" + h5rdr.outmat[3][i] + "\t" + h5rdr.outmat[4][i] + "\t" + h5rdr.outmat[5][i];
                allnumbers += "\t" + h5rdrqc.outmat[0][i] + "\t" + h5rdrqc.outmat[1][i] + "\t" + h5rdrqc.outmat[2][i];
                allnumbers += "\t" + h5rdrqc.outmat[3][i] + "\t" + h5rdrqc.outmat[4][i] + "\t" + h5rdrqc.outmat[5][i];
                //bwrt.write(chrom+"\t"+position+"\tN\t"+allnumbers+"\n");
                bwrt.write(position+"\t"+allnumbers+"\n");
            }
            bwrt.close(); // this will release the writer thread
            
            h5rdr.close();
        }
        catch(Exception e)
        {
            System.out.println("Error writing file: " + e);
        }
    }
    
}
