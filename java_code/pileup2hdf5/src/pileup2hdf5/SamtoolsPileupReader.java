/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author bukowski
 */
public class SamtoolsPileupReader extends GeneralPileupReader {
    
    private ArrayList<Integer> pos2geno;
    private int baseQCthr;
    private int baseQCaverageType;
    private String pileupCmdMain = "samtools mpileup -A -q 1 -Q 1 -d 60000 -f maize.fa ";
    
    public SamtoolsPileupReader(List<String> samplistIn, List<Integer> poslistIn, String chromIn,  
            int chromlenIN, Integer gversionIn, int startRangeIn, int endRangeIn,
            int[][][] dataIn, int[][] qcdataIn, ArrayList<Integer> pos2genoIn,
            String pileupCmdMainIn)
    {
        super(samplistIn, poslistIn, chromIn, gversionIn, 
            chromlenIN, startRangeIn, endRangeIn,
            dataIn, qcdataIn);
        
        pos2geno = pos2genoIn;
        
        //baseQCthr = 20;  // This should be read from input.....
        baseQCthr = 0;  // The actual threshold should be applied as samtools pileup option
        baseQCaverageType = 2; // 1: equal weigths (1/N_samples); 2: weights propportional to fraction of ellele reads coming from this sample
        if(! pileupCmdMainIn.equals(null))
        {
            pileupCmdMain = pileupCmdMainIn;
        }
    }
    
    public void read_pileup()
    {
        int numSamples = samplist.size();
        boolean useQC=true;
        long startTime, endTime;
        double duration;
        String [] aux;
        int position;
        int relpos;
        BufferedReader isr;
        analyze_pileup anp;
        
        for(int i=0;i<numSamples;i++)
        {
            // If positions to genotype are give, we need to supply the to samtools command
            // by writing them to disk first.....
            // Unfortunately, this does not seem to work right, because samtools does not
            // use BAM index while working with he "-l" option, so it is too slow...
            
            String regionstr = " -r "+chrom+":"+startRange+"-"+endRange;
            //if(pos2geno != null)
            if(false)  //  This is very inefficient, so we'll necer do it...
            {
                String fname = "smtposfile_"+String.valueOf(Thread.currentThread().getId());
                try
                {
                     BufferedWriter bw = new BufferedWriter(new FileWriter(fname));
                     int counter = 0;
                     for(int k=0;k<pos2geno.size();k++)
                     {
                         if(pos2geno.get(k)>=startRange && pos2geno.get(k)<=endRange)
                         {
                             bw.write(chrom + " " + pos2geno.get(k) + "\n");
                             counter++;
                         }
                     }
                     bw.close();
                     
                     System.out.println(counter + " sites to pileup in range "+startRange + "-" + endRange);
                     if(counter == 0) 
                     {
                         // Nothing to do
                         System.out.println("Nothing to do in rnage");
                         return;
                     }  
                }
                catch(Exception e)
                {
                    System.out.println("output file could not be open"+e);
                }
                
                regionstr = " -l " + fname + " ";
            }
            
            
            // We ned to get the list of BAM files for taxon i.
            // The name of the sample is a full path to its .h5 file.....
            String [] bamfiles = getBamList(samplist.get(i));
            String smpname = (new File(bamfiles[0])).getName().split("_")[0];
            String bamstr = "";
            for(int k=0;k<bamfiles.length;k++)
            {
                bamstr += " " + bamfiles[k] + " ";
            }
            
            //String cmdmain = "samtools mpileup -A -q 5 -d 60000 -f maize.fa ";
            //String cmdmain = "samtools mpileup -A -q 5 -Q 1 -d 60000 -f maize.fa ";
            String cmdmain = pileupCmdMain;
            String cmd= cmdmain + regionstr + bamstr;
            
            System.out.println("Piling up sample "+smpname+" with command\n"+cmd);
            startTime = System.currentTimeMillis();
            if(data != null)
            {
                // anp will stream the output to ber intercepted by this thread
                anp = new analyze_pileup(null, chrom, baseQCthr, new String [] {smpname,String.valueOf(bamfiles.length)}, gversion, cmd, pos2geno,false);
                isr = new BufferedReader(new InputStreamReader(anp.pistreams.get(0)),1000000); 
                Thread thr = new Thread(anp);
                thr.start();
                try
                {
                    String line = isr.readLine();
                    while(line != null)
                    {
                    // The format is: positions, 6*allele depths, 6*allele_quals
                    //System.out.println(line);
                    
                        aux = line.split("\t");
                        position = Integer.parseInt(aux[0]);
                        relpos = position - startRange;
                        for(int j=0;j<6;j++)
                        {
                            data[poslist.get(i)][j][relpos] = Integer.parseInt(aux[j+1]);
                            synchronized(this)
                            {
                            // !!! Dosn't simultaneous update of qcdata elements in multiple threads cause data inconsistency???
                            if(baseQCaverageType == 1)
                            {
                                qcdata[j][relpos] += Integer.parseInt(aux[j+7]); // To be normalized by 1/N_samples_with_allele_j
                            }
                            else
                            {
                                // To be normalized total depth of allele j
                                qcdata[j][relpos] += Integer.parseInt(aux[j+7])*data[poslist.get(i)][j][relpos];
                            }
                            }
                        }
                        line = isr.readLine();
                    }
                    isr.close();
                }
                catch(Exception e)
                {
                    System.out.println("ERROR reading samtools output: "+e);
                }
            }
            else
            {
                // Output from anp will be intercepted by h5 writer thread created by anp itself
                // We sneak in chromosome length as gversion
                gversion = chrlen;
                anp = new analyze_pileup(null, chrom, baseQCthr, new String [] {smpname,String.valueOf(bamfiles.length)}, 
                        gversion, cmd, pos2geno,true, startRange, endRange);
                Thread thr = new Thread(anp);
                thr.start();
                try
                {
                    thr.join();
                }
                catch(Exception e)
                {
                    System.out.println("ERROR: thread cannot die: "+e);
                }
            }
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime-startTime)/60;
            System.out.println("Sample "+smpname+" piled up in "+duration+" min");
        }
    }
    
    public static String [] getBamList(String h5path)
    {
        String [] result;
        String curname;
        
        File f = new File(h5path);
        String path = f.getParent(); // path in which to look for BAM files
        String fname = f.getName().replace(".h5", ""); // core of the BAM file name....
        
        // List all files in directory and select the ones starting with our taxon name and ending with ".bam"
        File folder = new File(path);
        File [] allfiles = folder.listFiles();
        
        String bamstr = "";
        for(int i=0;i<allfiles.length;i++)
        {
            curname = allfiles[i].getName();
            //System.out.println("allfiles: "+curname);
            if(curname.startsWith(fname+"_") && curname.endsWith(".bam"))
            {
                bamstr += path+ "/" + curname + ",";
            }
        }
        result = bamstr.split(",");
        return result;
    }
    
    @Override
    public void run()
    {
        read_pileup();
    }
}
