/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bukowski
 */
@XmlRootElement(name = "genotyper_params")
public class genotyper_params {
    // Genome version and file
        private int gversion = 2;
        private String ref_h5_file = null;
    // depth-based thresholds
        private int nonzero_cov_thr = 0; // sample considered to have nonzero coverage if its total depth is larger that this
        private int nonzero_all_cov_thr = 0; //allele is considered to occur in a sample if its depth is larger than this
        private int numaltall_max = 1;  // maximum number of retained alternative alleles
        private double minnumsampcov = 0.5; // minimum fraction of samples with coverage to include the site
        private double pvalthr = 0.01;    // allele rejected if pvalue of contingency table larger than this
        private int cntg_thr = 5;        // sample will be part of contingency table test if reference+1st_minor allele depth is greater than this
        private int depth_retained_alleles_thr = 1;  // sample will *not* be used in Ed's factor tests if depth over all retained alleles is smaller that this
        private int depth_alt_thr = 2; // overall combined depth of minor alleles has to be bigger than this to keep the position
        private int max_depth_alt_thr = 2; // total depth must be at least this in at least one sample with alt genotype to keep the position even if freq of alt alleles too low
        private int allele_retension_thr = 1; // see explanation of samp_allele_retension_thr below
        private int samp_allele_retension_thr = 1; // allele is ignored if its combined depth over all samples is less than allele_retension_thr 
        // OR if no individual sample has depth of this allele at least samp_allele_retension_thr
        // genotype-based thresholds
        private int samps_with_ret_all_thr = 1; // site rejected if number of samples with retained alleles smaller than this
        private int nall_after_genotyping_thr = 1; // retain at most this number of alt alleles after genotyping
        private int alt_num_thr = 0; // site kept if number of nono-reference alleles is more than this or if alt allele genotype supported by reads (see: max_depth_alt_thr)
        private double edfactor_reject_thr = 0.1;  // site will be rejected if Ed's factor greater than this (0.1 value is  arbitrary) 
        // Multinomial genotyper parameter
        private double e = 0.01;
        // Other parameters
        private boolean output_all_sites = false; // Whether to output all sites with coverage (including invariant ones)
        private String mpileupmaincmd="samtools mpileup -A -q 1 -Q 1 -d 60000 -f maize.fa"; // pileup command for direct calc
        private String outfmt = "RBfmt"; // output format: RBfmt or VCF
        private int contmatver = 1;
        private boolean output_genotypes = true;
        private boolean summary_depth_only = false;
        private boolean useCochran = false;
        private double cmatMaxThr = -1;
        
        public void setContmatver(int sr)
        {
            this.contmatver = sr;
        }
        public void setRef_h5_file(String sr)
        {
            this.ref_h5_file = sr;
        }
        public void setGversion(Integer sr)
        {
            this.gversion = sr;
        }
        public void setNonzero_cov_thr(Integer sr)
        {
            this.nonzero_cov_thr = sr;
        }
        public void setNonzero_all_cov_thr(Integer sr)
        {
            this.nonzero_all_cov_thr = sr;
        }
        public void setNumaltall_max(Integer sr)
        {
            this.numaltall_max = sr;
        }
        public void setMinnumsampcov(double sr)
        {
            this.minnumsampcov = sr;
        }
        public void setPvalthr(double sr)
        {
            this.pvalthr = sr;
        }
        public void setCntg_thr(Integer sr)
        {
            this.cntg_thr= sr;
        }
        public void setDepth_retained_alleles_thr(Integer sr)
        {
            this.depth_retained_alleles_thr = sr;
        }
        public void setDepth_alt_thr(Integer sr)
        {
            this.depth_alt_thr = sr;
        }
        public void setAllele_retension_thr(Integer sr)
        {
            this.allele_retension_thr = sr;
        }
        public void setSamp_allele_retension_thr(Integer sr)
        {
            this.samp_allele_retension_thr = sr;
        }
        public void setMax_depth_alt_thr(Integer sr)
        {
            this.max_depth_alt_thr = sr;
        }
        public void setSamps_with_ret_all_thr(Integer sr)
        {
            this.samps_with_ret_all_thr = sr;
        }
        public void setAlt_num_thr(Integer sr)
        {
            this.alt_num_thr = sr;
        }
        public void setNall_after_genotyping_thr(Integer sr)
        {
            this.nall_after_genotyping_thr = sr;
        }
        public void setEdfactor_reject_thr(double sr)
        {
            this.edfactor_reject_thr = sr;
        }
        public void setE(double sr)
        {
            this.e = sr;
        }
        public void setOutput_all_sites(boolean sr)
        {
            this.output_all_sites = sr;
        }
        public void setMpileupmaincmd(String sr)
        {
            this.mpileupmaincmd = sr;
        }
        public void setOutfmt(String sr)
        {
            this.outfmt = sr;
        }
        public void setOutput_genotypes(boolean sr)
        {
            this.output_genotypes = sr;
        }
        public void setSummary_depth_only(boolean sr)
        {
            this.summary_depth_only = sr;
        }
        public void setUseCochran(boolean sr)
        {
            this.useCochran = sr;
        }
        public void setCmatMaxThr(double sr)
        {
            this.cmatMaxThr = sr;
        }
        
        public int getContmatver()
        {
            return contmatver;
        }
        public String getRef_h5_file()
        {
            return ref_h5_file;
        }
        public Integer getGversion()
        {
            return gversion;
        }
        public Integer getNonzero_cov_thr()
        {
            return nonzero_cov_thr;
        }
        public Integer getNonzero_all_cov_thr()
        {
            return nonzero_all_cov_thr;
        }
        public Integer getNumaltall_max()
        {
            return numaltall_max;
        }
        public double getMinnumsampcov()
        {
            return minnumsampcov;
        }
        public double getPvalthr()
        {
            return pvalthr;
        }
        public Integer getCntg_thr()
        {
            return cntg_thr;
        }
        public Integer getDepth_retained_alleles_thr()
        {
            return depth_retained_alleles_thr;
        }
        public Integer getDepth_alt_thr()
        {
            return depth_alt_thr;
        }
        public Integer getAllele_retension_thr()
        {
            return allele_retension_thr;
        }
        public Integer getSamp_allele_retension_thr()
        {
            return this.samp_allele_retension_thr;
        }
        public Integer getMax_depth_alt_thr()
        {
            return max_depth_alt_thr;
        }
        public Integer getSamps_with_ret_all_thr()
        {
            return samps_with_ret_all_thr;
        }
        public Integer getAlt_num_thr()
        {
            return alt_num_thr;
        }
        public Integer getNall_after_genotyping_thr()
        {
            return nall_after_genotyping_thr;
        }
        public double getEdfactor_reject_thr()
        {
            return edfactor_reject_thr;
        }
        public double getE()
        {
            return e;
        }
        public boolean getOutput_all_sites()
        {
            return output_all_sites;
        }
        public String getMpileupmaincmd()
        {
            return mpileupmaincmd;
        }
        public String getOutfmt()
        {
            return outfmt;
        }
        public boolean getOutput_genotypes()
        {
            return output_genotypes;
        }
        public boolean getSummary_depth_only()
        {
            return summary_depth_only;
        }
        public boolean getUseCochran()
        {
            return useCochran;
        }
        public double getCmatMaxThr()
        {
            return cmatMaxThr;
        }
}
