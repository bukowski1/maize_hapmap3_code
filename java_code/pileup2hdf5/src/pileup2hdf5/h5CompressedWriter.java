/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.util.Hashtable;
import java.util.regex.Pattern;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;

/**
 *
 * @author bukowski
 */
public class h5CompressedWriter implements Runnable {
    private int fileID;
    private int sampleID;
    private int chrGID;
    Integer write_block_size=100000;  //write write_block_size number of sites at a time. As java limit the writing block size to 2g
    private Hashtable <Integer, Hashtable<String, Integer>>dataset2status = new Hashtable <Integer, Hashtable<String, Integer>>();
    //private byte[][] seq;
    private byte [][] seq;
    private byte [] index;
    private static Pattern WHITESPACE_PATTERN = Pattern.compile("\t");   
    private int indexinblock;
    private int chrlen;
    private BufferedReader isr = null;
    private boolean isbyte = true;
    //private double logbase = 1.0746;  // logbase^128 = 10,000
    //private double logconv;
    private encoder byte_enc;
    private int startchrpos;
    private int endchrpos;
    
    public h5CompressedWriter(PipedInputStream pis, String outfilename, String samplename, 
            String chrom, int chrlenIN, int gversion)
    {
        this(pis, outfilename, samplename, chrom, chrlenIN, 0, 0, gversion);
    }
    
    public h5CompressedWriter(PipedInputStream pis, String outfilename, String samplename, 
            String chrom, int chrlenIN, int startchrposIn, int endchrposIn, int gversion)
    {
        // chrlen = h5writereference.chromosome_lengths(gversion).get(chrom);
        chrlen = chrlenIN;
        startchrpos = startchrposIn;
        endchrpos = endchrposIn;
        
        try {
            java.io.File f = new java.io.File(outfilename);
            if(f.exists())
            {
               IHDF5Reader reader = HDF5Factory.openForReading(outfilename);
               
            //get/confirm sample name
            java.util.List<java.lang.String> groups = reader.getGroupMembers("/");
            String sampleinfile = groups.get(0);
            if(! sampleinfile.equals(samplename))
            {
                System.out.println("WARNING: sample names NOT the same: requested: " + samplename + " in H5 file: " + sampleinfile);
            }
            groups = reader.getGroupMembers("/"+sampleinfile);
            reader.close();
            
            // Now open the file for writing
                fileID = H5.H5Fopen(outfilename, HDF5Constants.H5F_ACC_RDWR,HDF5Constants.H5P_DEFAULT);
                sampleID = H5.H5Gopen(fileID,samplename);
                if(groups.indexOf(chrom) >= 0)
                {
                    System.out.println("WARNING: chromosome group: "+chrom + " already exists - will be overwritten");
                    H5.H5Ldelete(sampleID, chrom, HDF5Constants.H5P_DEFAULT);
                }
                this.create_chrgroup(chrom);
                //chrGID = H5.H5Gopen(sampleID, chrom);
                System.out.println("New GID: " + chrGID);
            }
            else
            {
                System.out.println("Creating a new H5 file "+outfilename + " with sample "+ samplename
                         + " chromosome " + chrom + " of length " + chrlen);
                fileID = H5.H5Fcreate(outfilename, HDF5Constants.H5F_ACC_TRUNC,
                    HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
                this.create_sample_group(samplename, samplename);
                this.create_chrgroup(chrom);
            }
        }
                    catch (HDF5Exception ex) {
        }
        
        // If input stream is null, it will read directly from the file
        // using first overload of loadMpileupFile
        if(pis != null)
        {
            isr = new BufferedReader(new InputStreamReader(pis));
        }
        
        //logconv = 1.0/Math.log(logbase);
        
        // Encode into bytes with default prameters
        byte_enc = new encoder();
    }
    
    @Override
    public void run()
    {
        loadMpileupFile();
        //closeFile();
    }
    
    public void loadMpileupFile(String infileName)
    {
        InputStreamReader isreader = null;
        try
        {
            if(! infileName.equals("-"))
            {
                isr = new BufferedReader(new FileReader(infileName), 1000000);
            }
            else
            {
                isreader = new InputStreamReader(System.in);
                isr = new BufferedReader(isreader);
            }   
        }
        catch(Exception e)
        {
            
        }
        loadMpileupFile();
    }
    
    // This procedure first stores all data in arrays (index and depth data)
    // and then writes everything out to HDF5
    public void loadMpileupFile() 
     {
         // length of the chromosome. Will keep alls ites even if coverage zero.
         int numsites = chrlen;
         if(startchrpos>0 && endchrpos>0)
         {
             numsites = endchrpos -startchrpos + 1;
         }
         int numalleles = 6;
         int curpos = 0;
         int tempdepth=0;
         int tempindex;
         int [] allele_bits = {32,16,8,4,2,1}; // A = 100000, C=010000, G=001000, T=000100, I=000010, D=000001
        
        try
        {
            //int datasetID = this.create_dataset(chrGID, numsites, numalleles);
            
            // Allocate memory to store depths and indices
            seq = new byte[2][numsites*numalleles]; // Only a fraction of this will be eventually stored
            index = new byte[numsites];
            
            System.out.println("waiting for first input");
            String fileInLine = isr.readLine();
            System.out.println("got first input");
            String[] s;
            
            // fill up the matrices, padding missing index positions with zeroes
            indexinblock = 0;
            int depthindex=0;
            int startpos = 1;
            if(startchrpos>0) { startpos = startchrpos; }
            curpos = startpos;
            int endpos = numsites;
            if(endchrpos>0) { endpos = endchrpos; }
            int pos = 0;
            while (fileInLine != null && curpos <= endpos) // changed from numsites to endpos
            {
                s = WHITESPACE_PATTERN.split(fileInLine);
                //pos = Integer.parseInt(s[1]);
                pos = Integer.parseInt(s[0]);
                if(pos < startpos) { continue; }
                if(pos > endpos) { break; }
                if(pos > curpos)
                {
                    for(int j=0;j< pos - curpos;j++)
                    {
                        index[indexinblock]=0;
                        indexinblock++;
                        //flush_matrix(datasetID,numalleles);
                    }
                }
                tempindex=0;
                for (int i = 1; i < numalleles+1; i++) 
                {
                        // Here we need to set the bits in index and nonzero bytes in seq
                        
                        tempdepth = Integer.parseInt(s[i]);
                        if(tempdepth>0)
                        {
                            /*
                            if(tempdepth>127)
                            {
                                tempdepth = (int)(-logconv*Math.log(tempdepth));
                                if(tempdepth < -128) { tempdepth = -127; }
                            }
                           seq[0][depthindex] = (byte)tempdepth; // ddepths
                           seq[1][depthindex] = (byte)Integer.parseInt(s[i+6]); // base quality scores
                           */
                           seq[0][depthindex] = byte_enc.Encode(tempdepth); // ddepths
                           seq[1][depthindex] = byte_enc.Encode(Integer.parseInt(s[i+6])); // base quality scores
                           depthindex++;
                           
                           // Set the bits in index....
                           tempindex = (tempindex | allele_bits[i-1]);
                        }
                }
                index[indexinblock] = (byte)tempindex;
                indexinblock++;
                //flush_matrix(datasetID,numalleles);
                curpos = pos + 1;
                fileInLine = isr.readLine();
            }
            isr.close();
            
            // Pad the rest of the matrix with zeroes, if needed
            //System.out.println("curpos, endpos, indexinblock: " + curpos + " " + endpos + " " + indexinblock);
            if(curpos <= endpos)
            {
                for(int j=0;j<= endpos - curpos;j++)
                {
                   index[indexinblock]=0;
                   indexinblock++;
                   //flush_matrix(datasetID,numalleles);
                   //System.out.println("indexinblock after flush in last loop: " + indexinblock);
                }
            }
            
            // Both arrays read in - time to save gthem in HDF5 datasets
            // NOTE: depthindex is the size of seq matrix to be written...
            // Size of index matrix is equal to chromosme length
            int indexDatasetID = this.create_dataset("Index",chrGID, numsites, 1);
            int depthDatasetID = this.create_dataset("Depth",chrGID, depthindex, 1);
            int qualDatasetID = this.create_dataset("Quals",chrGID, depthindex, 1);
            
            int  DataTypeID, FileSpaceID,  MemorySpaceID;
        
            FileSpaceID = H5.H5Dget_space (indexDatasetID);
            long[] count= new long[] {1, numsites};
            MemorySpaceID = H5.H5Screate_simple(2, count, null);
            H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
            H5.H5Dwrite(indexDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, index);
            
            System.gc();
            index = null;
        
            H5.H5Sclose(MemorySpaceID);
            H5.H5Sclose(FileSpaceID);
            H5.H5Dclose(indexDatasetID);
            
            FileSpaceID = H5.H5Dget_space (depthDatasetID);
            count= new long[] {1, depthindex};
            MemorySpaceID = H5.H5Screate_simple(2, count, null);
            H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
            H5.H5Dwrite(depthDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, seq[0]);
            
            System.gc();
            //seq = null;
            
            H5.H5Sclose(MemorySpaceID);
            H5.H5Sclose(FileSpaceID);
            H5.H5Dclose(depthDatasetID);
            
            FileSpaceID = H5.H5Dget_space (qualDatasetID);
            count= new long[] {1, depthindex};
            MemorySpaceID = H5.H5Screate_simple(2, count, null);
            H5.H5Sselect_hyperslab(FileSpaceID, HDF5Constants.H5S_SELECT_SET,
                    new long[] {0, 0}, null, count, null);
            H5.H5Dwrite(qualDatasetID, HDF5Constants.H5T_NATIVE_SCHAR, MemorySpaceID, FileSpaceID,HDF5Constants.H5P_DEFAULT, seq[1]);
            
            System.gc();
            seq = null;
        
            H5.H5Sclose(MemorySpaceID);
            H5.H5Sclose(FileSpaceID);
            H5.H5Dclose(qualDatasetID);
            
            H5.H5Gclose(chrGID);
            H5.H5Gclose(sampleID);
            H5.H5Fclose(fileID);
            
            
            //this.close_dataset(datasetID);
        }
        catch(Exception e)
        {
            System.err.println("Error in processing mpileup extract file at position "+curpos+": " + e);
            e.printStackTrace();
        }
     }
    
    public int create_dataset (String dsname, int chrGroupID, int numSites, int numAlleles)
    {                   
            //a block cannot be larger than 2g, as java cannot read/write a selection bigger than 2g 
            int maxbytes = (int)2E9;
            if ((numAlleles * write_block_size) > maxbytes)
            {
                write_block_size = maxbytes/numAlleles;
            }

                        
            //create a data set
            int DataTypeID=-1, FileSpaceID=-1, datasetPropertylist=-1, datasetID=-1;
            
            datasetPropertylist = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
            long[] DIMS3=null;
            long[] MAX_DIMS = null;
            
            
            // Should change this to byte once it works...
            if(isbyte)
            {
                DataTypeID = HDF5Constants.H5T_NATIVE_SCHAR;
                System.out.println("Data type is SCHAR" + DataTypeID);
            }
            else
            {
                DataTypeID = HDF5Constants.H5T_NATIVE_INT;
                System.out.println("Data type is INT" + DataTypeID);
            }
            DIMS3 = new long[]{numAlleles, numSites };
            MAX_DIMS = null;
            H5.H5Pset_layout(datasetPropertylist , HDF5Constants.H5D_CONTIGUOUS);
            FileSpaceID = H5.H5Screate_simple(2, DIMS3, MAX_DIMS);
          
            datasetID = H5.H5Dcreate(chrGroupID, dsname, DataTypeID, FileSpaceID, datasetPropertylist);
            Hashtable <String, Integer> status = new Hashtable<String, Integer>();
            status.put("siteCountInDataSet", 0);
            status.put("numRows", numAlleles);
            status.put("numCols", numSites);
            status.put("lastupboundary", 0);
            dataset2status.put(datasetID, status);
            
            if(isbyte)
            {
                //add_attribute(datasetID,"logbase",String.valueOf(logbase));
                //int offsetIn, double adjIn, int max_acc_depthIn
                add_attribute(datasetID,"logbase",String.valueOf(byte_enc.getLogbase()));
                add_attribute(datasetID,"offset",String.valueOf(byte_enc.getOffset()));
                add_attribute(datasetID,"adj",String.valueOf(byte_enc.getAdj()));
                add_attribute(datasetID,"max_acc_depth",String.valueOf(byte_enc.getMaxAccDepth()));
            }
            
            H5.H5Sclose(FileSpaceID);
            return datasetID;
     }
    
    public void create_sample_group(String SampGroupName, String SampleName)
    {
        try {
            sampleID = H5.H5Gcreate(fileID, SampGroupName, SampGroupName.length());
            } 
        catch (HDF5Exception ex) 
        {
        }
        add_attribute (sampleID, "name", SampleName);
          
    }
    
    public void create_chrgroup (String chrname)
    {
        int chrGroupID = -1;
        chrGroupID = H5.H5Gcreate(sampleID, chrname, chrname.length());
        if (chrlen>0)
        {
            add_attribute(chrGroupID, "length", String.valueOf(chrlen));
        }

        add_attribute(chrGroupID, "name", chrname);
        
        chrGID = chrGroupID;
    }
    
    private  void add_attribute (int locid, String name, String value)
    {
        int DataTypeID=-1, attrid=-1, FileSpaceID=-1;
        try {
        DataTypeID = H5.H5Tcopy(HDF5Constants.H5T_C_S1);   
        H5.H5Tset_size(DataTypeID, value.length());
        long[] DIMS1 = {1};
        FileSpaceID = H5.H5Screate_simple(1, DIMS1, null);
            //int datasetPropertylist = H5.H5Pcreate(HDF5Constants.H5P_ATTRIBUTE_CREATE);
        attrid = H5.H5Acreate(locid, name, DataTypeID, FileSpaceID, HDF5Constants.H5P_DEFAULT);
             byte[] projnamebyte = value.getBytes();
             H5.H5Awrite (attrid, DataTypeID,  projnamebyte); 
             H5.H5Aclose(attrid);
             H5.H5Sclose(FileSpaceID);
        }
        catch (HDF5Exception ex) 
        {
        }
    }
    
    public int getChrGID()
    {
        return chrGID;
    }
    
    public void closeFile()
    {
          H5.H5Gclose(chrGID);
          H5.H5Gclose(sampleID);
          H5.H5Fclose(fileID);
    }
}
