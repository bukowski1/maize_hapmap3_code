/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.util.List;

/**
 *
 * @author bukowski
 */
abstract public class GeneralPileupReader implements Runnable {
    
   protected int [][][] data;
   protected int [][] qcdata;
   protected String chrom;
   protected List<String> samplist;
   protected List<Integer> poslist;
   protected Integer gversion; 
   protected int chrlen;
   protected int startRange;
   protected int endRange;
   //protected int blocksize;
   
   public GeneralPileupReader(List<String> samplistIn, List<Integer> poslistIn, String chromIn, Integer gversionIn, 
            int chrlenIN, int startRangeIn, int endRangeIn,
            int[][][] dataIn, int[][] qcdataIn)
    {
        samplist = samplistIn;
        poslist = poslistIn;
        chrom = chromIn;
        gversion = gversionIn;
        chrlen = chrlenIN;
        startRange = startRangeIn;
        endRange = endRangeIn;
        //blocksize = blocksizeIn;
        data = dataIn;
        qcdata = qcdataIn;
        
    }
   
    
}