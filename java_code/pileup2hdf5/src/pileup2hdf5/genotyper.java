/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import java.util.Arrays;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.*;
//import java.util.List;
//import java.util.Hashtable;
//import java.util.Comparator;
import org.apache.commons.lang.ArrayUtils;


/**
 *
 * @author bukowski
 * @deprecated, use {@ling pgenotyper()} instead 
 */
@Deprecated
public class genotyper {
   
    ArrayList<String> SampleList = new ArrayList<String>(); // will keep the names of sample h5 mpileup files
    int numSamples;
    private int[][][] data = null;
    private String [] alleles = {"A", "C", "G", "T", "I", "D"};
    private Hashtable<String, Integer> allelepos = new Hashtable<String,Integer>(); 
    private int chromlen;
    private byte[] refsequence;

    public genotyper(String sampleFile, String chrom, int startRange, int endRange)
    {
        try
        {
            // Load the list of sample h5 files to process
        BufferedReader fileIn = new BufferedReader(new FileReader(sampleFile), 1000000);
        String line=null;
        while((line=fileIn.readLine()) != null)
        {
            SampleList.add(line);
        }
        numSamples = SampleList.size();
        }
        catch(Exception e)
        {
            System.out.println("Could not read the sample file" + e);
        }
        
        // Initialize the allele positions in extracted mpileup file
        allelepos.put("A", 0);
        allelepos.put("C", 1);
        allelepos.put("G", 2);
        allelepos.put("T", 3);
        allelepos.put("I", 4);
        allelepos.put("D", 5);
        
        // Load chromosome length
        // Assume version 2 of the genome
        chromlen = h5writereference.chromosome_lengths(2).get(chrom);
        
        // Load the approproate chunk of reference sequence - assumes the file maizev2.h5 is in current dir
        // First, normalize the end of range
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int blocksize = endRangeNorm - startRange + 1;
        IHDF5Reader rdr = HDF5Factory.openForReading("maizev2.h5");
        int offset = startRange - 1;
        refsequence = rdr.readByteArrayBlockWithOffset("/"+chrom+"/sequence",blocksize,offset);
        rdr.close();
        /*
        for(int i=0;i<seq_tst.length;i++)
        {
            String refbase = Character.toString((char)seq_tst[i]).toUpperCase();
            
            System.out.print(refbase);
        }
        System.out.print("\n");
         * 
         */
    }
    
    public void genotype_range(String chrom, int startRange, int endRange)
    {
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int numsites = endRangeNorm - startRange + 1;
        data = new int[numSamples][6][numsites];
        int [][] totdepth = new int[numSamples][numsites];
        Hashtable<String,Integer> sampcount = new Hashtable<String,Integer>();
        
        
        for(int i=0;i<numSamples;i++)
        {
            h5reader rdr = new h5reader(SampleList.get(i),chrom,2, chromlen);
            rdr.get_range(startRange, endRange, false);  // tnis will cap endRange, if needed
            data[i] = Arrays.copyOf(rdr.outmat, 6);
            rdr.close();
        }
        
        // Test printout for sanity check and/or compute total depth
        for(int i=0;i<numSamples;i++)
        {
            //System.out.println("Sample " + SampleList.get(i));
            for(int k=0;k<numsites;k++)
            {
                totdepth[i][k] = 0;
                for(int j=0;j<6;j++)
                {
                    totdepth[i][k] += data[i][j][k];
                    //System.out.print(data[i][j][k] + "\t");
                }
                //System.out.print("\n");
            }
        }
        
        // Loop over sites
        for(int pos=0;pos<numsites;pos++)
        {
            String refbase = Character.toString((char)refsequence[pos]).toUpperCase();
            
            // Skip the site if reference unknown - maybe this is not right
            if(refbase.equalsIgnoreCase("N"))
            {
                continue;
            }
            // Find total depth over all samples
            int totcov = 0;
            for(int i=0;i<numSamples;i++)
            {
                totcov += totdepth[i][pos];
            }
            if(totcov == 0) { continue; }
            
            // For each allele, find the number of samples it occurs in
            for(int ia=0;ia<6;ia++)
            {
                sampcount.put(alleles[ia], 0);
            }
            for(int i=0;i<numSamples;i++)
            {
                for(int ia=0;ia<6;ia++)
                {
                    sampcount.put(alleles[ia], sampcount.get(alleles[ia]) + Math.min(data[i][allelepos.get(alleles[ia])][pos],1));
                }
            }
            
            // Sort aleles according to their depth accross all samples
            final int [] allele_nums = {sampcount.get("A"), sampcount.get("C"), sampcount.get("G"),sampcount.get("T"),sampcount.get("I"),sampcount.get("D") };
            //int [] index = {0,1,2,3,4,5};
            ArrayList<Integer> index = new ArrayList<Integer>();
            for(int ia=0;ia<6;ia++)
            {
                index.add(ia);
            }
            
            // Construct the comparator to sort in reverse order
            Comparator<Integer> cmp = new Comparator<Integer>(){
                @Override
                public int compare(Integer a, Integer b){
                    return allele_nums[b]-allele_nums[a];
            }};
            
            Collections.sort(index, cmp);
           
            // Save sorted version of alleles and allele_nums
            String [] alleles1 = new String [6];
            int [] allele_nums1 = new int [6];
            for(int ia=0;ia<6;ia++)
            {
                alleles1[ia] = alleles[index.get(ia)];
                allele_nums1[ia] = allele_nums[index.get(ia)];
            }
            
            // Test printout
            /*
            System.out.println("initial alleles: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles[ia] + ":" + allele_nums[ia] +"\t");
            }
            System.out.print("\n");
            System.out.println("sorted alleles: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles1[ia] + ":" + allele_nums1[ia] +"\t");
            }
            System.out.print("\n");
             * 
             */
            
            String allele_str = "";
            for(int ia=0;ia<6;ia++)
            {
                if(allele_nums1[ia] >0)
                {
                    if(! alleles1[ia].equals(refbase)) { allele_str += alleles1[ia] + ",";}
                }
                else
                {
                    break;
                }
            }
            
            // If no variation detected, proceed to the next position
            // (if no reference base given all positions with non-zero coverage will be written
            if(allele_str == "")
            {
                continue;     // continue the loop over positions
            }
            allele_str = allele_str.substring(0, allele_str.length()-1);  // remove trailing comma
            int pos_actual = pos + startRange;
            String outstr = chrom + "\t" + pos_actual + "\t" + refbase + "\t" + allele_str + "\t";
            
            // Generate allele depths
            for(int i=0;i<numSamples;i++)
            {
                int [] num_mat = new int[6];
                int matcount = 0;
                // Add dpeth of reference base as the first element - once reference base is known
                num_mat[matcount] = data[i][allelepos.get(refbase)][pos];
                outstr += num_mat[matcount] + ",";
                matcount++;
                for(int ia=0;ia<6;ia++)
                {
                    if(allele_nums1[ia] > 0)
                    {
                        if(! alleles1[ia].equals(refbase))
                        {
                            num_mat[matcount] = data[i][allelepos.get(alleles1[ia])][pos];
                            outstr += num_mat[matcount] + ",";
                            matcount++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                outstr = outstr.substring(0, outstr.length()-1);
                
                // Add genotype likelihood calculations here
                
                outstr += "\t";
                
                ArrayList<Double> lkhds = new ArrayList<Double>();
                double lkh_max = -9999999;
                int lkh_max_ind = 0;
                String genotype = "0/0";
                for(int ia1=0;ia1<matcount;ia1++)
                {
                    for(int ia2=ia1;ia2<matcount;ia2++)
                    {
                        double lll = genotype_likelihood(ia1,ia2,num_mat, matcount);
                        lkhds.add(lll);
                        if(lll > lkh_max)
                        {
                            lkh_max = lll;
                            genotype = String.valueOf(ia1) + "/" + String.valueOf(ia2);
                        }
                        
                    }
                }
                
                // Normalize likelihoods and produce a strin to display
                String lkhdstr = "";
                for(int ia=0;ia<lkhds.size();ia++)
                {
                    int lll = (int)(-10*(lkhds.get(ia) - lkh_max) + 0.5);
                    lkhdstr += String.valueOf(lll) + ",";
                }
                lkhdstr = genotype + "|" + lkhdstr.substring(0, lkhdstr.length()-1);
                
                outstr += lkhdstr + "\t";
                
            }  // loop over samples (computing depths and genotype likelihoods
            outstr = outstr.substring(0,outstr.length()-1) + "\n";
            
            System.out.print(outstr);
            
       } // loop over positions
        
    }   // end of genotype_range
    
    private static double genotype_likelihood(int ia1, int ia2, int[] num_mat, int matcount)
    {
        double lkh = 0;
        
        double e = 0.05;
        double p_homo = -0.018483406; // log10(1 - 5*e/6);
        double p_het = -0.315753252;  //  log10(0.5 -e/3);
        double p_rest = -2.079181246;  // log10(e/6); 
        
        if(ia1 == ia2)
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1)
                {
                    lkh += num_mat[i]*p_homo;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        else
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1 || i==ia2)
                {
                    lkh += num_mat[i]*p_het;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        
        return lkh;
    }
    
}
