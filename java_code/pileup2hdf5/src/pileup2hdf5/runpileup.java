/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static pileup2hdf5.IOthread.getChromInfoFromFai;
/**
 *
 * @author bukowski
 */
public class runpileup {
    ArrayList<String> SampleList = new ArrayList<String>(); // will keep the names of sample h5 mpileup files
    private ArrayList<Integer> pos2geno = null;
    private Integer gversion = 2;
    
    public runpileup(String paramFile, String sampleFile, String chrom, int startRange, int endRange, int numThreads, String h5Dir, String posFile)
    {
        // Load the list of sample h5 files to process
        try
        {
            BufferedReader fileIn = new BufferedReader(new FileReader(sampleFile), 1000000);
            String line=null;
            while((line=fileIn.readLine()) != null)
            {
                // Names in sample list may or may not contain ".h5" suffix - we can now keep uniform sample lists
                String dfile = line.replace(".h5", "");
                SampleList.add(h5Dir + "/" + dfile);
            }
        }
        catch(Exception e)
        {
            System.out.println("Could not read the sample file" + e);
        }
        
        // If the list of sites to genotype is given, read it in
        // The format of posFIle is position\talt1,alt2,..., although only "position" will be used here
        // Assume positions are sorted in ascending order
        if(posFile != null)
        {
            try
            {
                pos2geno = new ArrayList<Integer>();
                BufferedReader fileIn = new BufferedReader(new FileReader(posFile), 1000000);
                String line=null;
                while((line=fileIn.readLine()) != null)
                {
                    String [] aux = line.split("\t");
                    pos2geno.add(Integer.parseInt(aux[0]));
                }
            }
            catch(Exception e)
            {
                System.out.println("Could not read the positions file" + e);
            }
        }
        
        // Read the genotyping parameters xml file, just to get the mpileup command and genome version, if different from defaul
        genotyper_params gparams = new genotyper_params();
        try
        {
            JAXBContext context = JAXBContext.newInstance(genotyper_params.class);
            Unmarshaller um = context.createUnmarshaller();
            try
            {
                Reader r = new FileReader(paramFile);
                gparams = (genotyper_params) um.unmarshal(r);
                gversion = gparams.getGversion();
            }
            catch(IOException i)
            {
                System.out.println("Could not read the parameter file");
                 i.printStackTrace();
            }
        }
        catch(JAXBException i)
        {
            System.out.println("Could not de-serialize the parameter class");
            i.printStackTrace();
        }
        
        // Normalize the end of range
        // int chromlen = h5writereference.chromosome_lengths(gversion).get(chrom);
        //int chromlen = chrlenIN; // this should be supplied based on a fai file instead of being passed on via parameters list
        int chromlen = IOthread.getChromInfoFromFai(gparams.getRef_h5_file() + ".fai",chrom)[0];
        System.out.println("chromlen in runpileup: " + chromlen);
        
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        if(startRange == 0 && endRangeNorm == 0)
        {
            startRange = 1;
            endRangeNorm = chromlen;
        }
        
        // Distribute the taxa (names/h5 files and positions in original list) over threads
        List<List<String>> taxalists = new ArrayList<List<String>>();
        List<List<Integer>> taxapos = new ArrayList<List<Integer>>();
        int numSamples= SampleList.size();
        //for(int i=0;i<numIOthreads;i++)
        for(int i=0;i<numSamples;i++)
        {
            taxalists.add(new ArrayList<String>());
            taxapos.add(new ArrayList<Integer>());
        }
        for(int i=0;i<numSamples;i++)
        {
            //taxalists.get(i % numIOthreads).add(SampleList.get(i));
            //taxapos.get(i % numIOthreads).add(i);
            taxalists.get(i).add(SampleList.get(i));
            taxapos.get(i).add(i);
        }
        
        java.util.concurrent.ExecutorService IOexecutor = Executors.newFixedThreadPool(numThreads);
        
        for(int i=0;i<numSamples;i++)
        {
            // This eader which directly calls samtools......
            SamtoolsPileupReader preader = new SamtoolsPileupReader(taxalists.get(i), taxapos.get(i),chrom, 
                    chromlen, gversion, startRange, endRangeNorm, null, null, pos2geno,
                    gparams.getMpileupmaincmd());
            IOexecutor.execute(preader);
            
        }
        
        IOexecutor.shutdown();
        try
        {
            IOexecutor.awaitTermination(100, TimeUnit.DAYS);
        }
        catch(Exception e)
        {
            System.out.println("IOexecutor termination problem: "+e);
        }
    }
    
    public static void executeSamtools(String cmd)
    {
        
        // Just test case
        
        String [] bamfiles = SamtoolsPileupReader.getBamList(cmd);
        for(int i=0;i<bamfiles.length;i++)
        {
            System.out.println(bamfiles[i]);
        }
        
        return;
        
        /*
        analyze_pileup anp = new analyze_pileup("AAA", "10", 20, new String [] {"B73","1"}, 2, cmd);
        BufferedReader isr = new BufferedReader(new InputStreamReader(anp.mainpistream),100000000); 
        
        Thread thr = new Thread(anp);
        thr.start(); // This should now write output
        //anp.analyze_and_write();
        
        //InputStream pipedOut = null;
        try
        {
            // Process p = Runtime.getRuntime().exec(cmd);
            // pipedOut = p.getInputStream();    
            // BufferedReader isr = new BufferedReader(new InputStreamReader(pipedOut), 1000000);
            
            String line = isr.readLine();
            while(line != null)
            {
                System.out.println(line);
                line = isr.readLine();
            }
            
            /*
            byte [] buffer = new byte[2048];
            int read = pipedOut.read(buffer);
            
            while(read>=0)
            {
                System.out.write(buffer, 0, read);
                read = pipedOut.read(buffer);
            }
            */
            
            // p.waitFor();
        /*
        }
        catch(Exception e)
        {
            System.out.println("ERROR executing: "+e);
        }
         */
        
    }
    
    
}
