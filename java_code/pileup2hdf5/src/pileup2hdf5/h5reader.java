/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import ch.systemsx.cisd.hdf5.HDF5DataSetInformation;
import java.util.List;

/**
 *
 * @author bukowski
 */
public class h5reader {
    private IHDF5Reader reader;
    private String sampleName;
    private String chromName;
    int chrLen;
    int [][] outmat;
    int [][] outmatqc;
    byte [][] outmat_byte;
    byte [][] out_byte_depth;
    byte [][] out_byte_qc;
    private boolean isbyte = false;
    private double logbase = 1.0746;  // logbase^128 = 10,000
    //private double logconv;
    private boolean iscompressed=false;
    private int [] allele_bits = {32,16,8,4,2,1}; // A = 100000, C=010000, G=001000, T=000100, I=000010, D=000001
    private encoder byte_decoder;
    
    public h5reader(String infileName, String chrom, int gversion, int chrlenIN)
    {
        reader = HDF5Factory.openForReading(infileName);
        //get sample name
        java.util.List<java.lang.String> groups = reader.getGroupMembers("/");
        sampleName = groups.get(0);  
        chromName = chrom;
        //chrLen = Integer.parseInt(reader.getStringAttribute("/" + sampleName + "/" + chrom, "length"));
        //chrLen = h5writereference.chromosome_lengths(gversion).get(chromName);
        chrLen = chrlenIN;
     
        String array_name = reader.getGroupMembers("/" + sampleName + "/" + chrom).get(0);
        HDF5DataSetInformation ddd =  reader.getDataSetInformation("/" + sampleName + "/" + chrom + "/" +array_name);
        int elsize = ddd.getTypeInformation().getElementSize();
        //System.out.println("element size: "+elsize);
        
        isbyte= false;
        if(elsize == 1)
        {
            isbyte = true;
            logbase = Double.parseDouble(reader.getStringAttribute("/" + sampleName + "/" + chrom + "/" +array_name, "logbase"));
            //System.out.println("LOGBASE detected: "+logbase);
            //logconv = Math.log(logbase);
            
            // If other encoding attributes are present, read them and initialize the decoder accordingly.
            List<String> attrs = reader.getAllAttributeNames("/" + sampleName + "/" + chrom + "/" +array_name);
            double adj = 0.5;
            int max_acc_depth = 127;
            int offset = 0;
            if(attrs.indexOf("adj")>-1) { adj = Double.parseDouble((reader.getStringAttribute("/" + sampleName + "/" + chrom + "/" +array_name, "adj"))); }
            if(attrs.indexOf("offset")>-1) { offset = Integer.parseInt((reader.getStringAttribute("/" + sampleName + "/" + chrom + "/" +array_name, "offset"))); }
            if(attrs.indexOf("max_acc_depth")>-1) { max_acc_depth = Integer.parseInt((reader.getStringAttribute("/" + sampleName + "/" + chrom + "/" +array_name, "max_acc_depth"))); }
            byte_decoder = new encoder(logbase,offset,adj,max_acc_depth);
            System.out.println("Encoding detected (logbase,adj, offset, max_acc_depth): "+logbase+" "+adj + " "+ offset + " "+max_acc_depth);
        }
        
        if(array_name.equals("Depth")) 
        { 
            iscompressed = true; 
            System.out.println("Compressed format detected:");
        }
    }
    
    public void get_range(int rangeStart, int rangeEnd, boolean prt)
    {
        int rstrt = rangeStart;
        int rend = rangeEnd;
        
        if(rend > chrLen)
        {
            rend = chrLen;
        }
        
        int numsites_in_range = rend - rstrt + 1;
        //System.out.println("numsites_in_range: " + numsites_in_range);
        
        //outmat = new int[6][numsites_in_range];
        if(isbyte && (! iscompressed))
        {  
            outmat_byte= reader.readByteMatrixBlockWithOffset("/" + sampleName + "/" + chromName +"/data_array", 6, numsites_in_range, 0, rstrt-1);
            // Decode this into integer array here - if it proves too time consuming,
            // we can always access outmat_byte directly from the calling code
            outmat = new int[6][numsites_in_range];
            for(int j=0;j<6;j++)
            {
                for(int i=0; i< numsites_in_range; i++)
                {
                    /*
                    if(outmat_byte[j][i] >= 0)
                        {
                            outmat[j][i] = outmat_byte[j][i];
                        }
                        else
                        {
                            int aux = (int)(Math.exp(-logconv*(outmat_byte[j][i]-0.5)));
                            outmat[j][i] = aux;
                            //outmat[j][i] = -outmat_byte[j][i];
                        }
                    */
                    outmat[j][i] = byte_decoder.decoded[128+outmat_byte[j][i]];
                }
            }
        }
        else if(iscompressed)
        {
            // First, read the index up to upper range limit
            int [] cardlookup = make_byte_cardinalities();
            outmat_byte= reader.readByteMatrixBlockWithOffset("/" + sampleName + "/" + chromName +"/Index", 1, rend, 0, 0);
            System.out.println("bytes of index read in: "+outmat_byte[0].length);
            //for(int i=0;i<outmat_byte[0].length;i++)
            //{
            //    System.out.println(outmat_byte[0][i]);
            //}
            // Count the number of depth bytes up to rstrt -1
            int numbytes_before = 0;
            int numbytes_upto = 0;
            for(int i=0;i<rend;i++)
            {
                numbytes_upto += cardlookup[outmat_byte[0][i]];
                if(i<rstrt-1) {numbytes_before = numbytes_upto; }
            }
            System.out.println("numbytes_before: " + numbytes_before);
            System.out.println("numbytes_upto: " + numbytes_upto);
            int numbytes_in_range = numbytes_upto - numbytes_before;
            if(numbytes_in_range >0)
            {
                out_byte_depth = reader.readByteMatrixBlockWithOffset("/" + sampleName + "/" + chromName +"/Depth", 1, numbytes_in_range, 0, numbytes_before);
                out_byte_qc = reader.readByteMatrixBlockWithOffset("/" + sampleName + "/" + chromName +"/Quals", 1, numbytes_in_range, 0, numbytes_before);
                System.out.println("bytes read in: "+out_byte_depth[0].length);
                //for(int i=0;i<out_byte_depth[0].length;i++)
                //{
                //    System.out.println(out_byte_depth[0][i]);
                //}
            }
            // decode depth bytes into integer outmat array
            outmat = new int[6][numsites_in_range];
            outmatqc = new int[6][numsites_in_range];
            int counter = 0;
            for(int i=0;i<numsites_in_range;i++)
            {
                byte baux = outmat_byte[0][rstrt+i-1];
                //System.out.println("i, baux: "+i+" "+baux);
                if(baux > 0)
                {
                    for(int j=0;j<6;j++)
                    {
                        if((baux & allele_bits[j]) == allele_bits[j])
                        {
                            //System.out.println("found allele "+j + " with "+ allele_bits[j]);
                            /*
                            if(out_byte_depth[0][counter] >=0)
                            {
                                outmat[j][i] = out_byte_depth[0][counter];
                            }
                            else
                            {
                                outmat[j][i] = (int)(Math.exp(-logconv*(out_byte_depth[0][counter]-0.5)));
                            }
                            outmatqc[j][i] = (int)out_byte_qc[0][counter];
                            */
                            outmat[j][i] = byte_decoder.decoded[128+out_byte_depth[0][counter]];
                            outmatqc[j][i] = byte_decoder.decoded[128+out_byte_qc[0][counter]];
                            counter++;
                        }
                        else
                        {
                            outmat[j][i] = 0; outmatqc[j][i] = 0;
                        }
                    }
                }
            }
        }
        else
        {
            outmat = reader.readIntMatrixBlockWithOffset("/" + sampleName + "/" + chromName +"/data_array", 6, numsites_in_range, 0, rstrt-1);
        }
        
        if(prt)
        {
            for(int i=0; i< numsites_in_range; i++)
            {
                int sum = 0;
                for(int j=0;j<6;j++)
                {
                    sum += outmat[j][i];   
                }
                if(sum == 0) { continue; }
                
                int pos = rstrt + i;
                System.out.print("c\t"+pos+"\tN");
                for(int j=0;j<6;j++)
                {
                    System.out.print("\t" + outmat[j][i]);   
                }
                if(iscompressed)
                {
                    for(int j=0;j<6;j++)
                    {
                        System.out.print("\t" + outmatqc[j][i]);   
                    }   
                }
                System.out.print("\n");
            }
        }
    }
    
    public void close()
    {
        reader.close();
        outmat = null;
        System.gc();
    }
    
    // Produce a lookup table of byte cardinalities
    // the bytes are from 0 to 00111111=
    private int [] make_byte_cardinalities()
    {
        int [] result = new int[64];
        
        for(int i=0;i<64;i++)
        {
            int value = i;
            int count = 0;
            while(value >0)
            {
                if((value & 1) == 1) { count++; }
                value >>= 1;
            }
            result[i] = count;
        }
        return result;
    }
    
    
}
