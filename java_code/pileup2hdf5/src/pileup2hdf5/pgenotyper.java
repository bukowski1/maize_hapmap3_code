/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;
import net.sf.javaml.utils.ContingencyTables;
import contmatpack.*;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author bukowski
 */
public class pgenotyper implements Runnable {
    ArrayList<String> SampleList = new ArrayList<String>(); // will keep the names of sample h5 mpileup files
    int numSamples;
    private int[][][] data = null;
    private int [][] qcdata = null;
    private String [] alleles = {"A", "C", "G", "T", "I", "D"};
    private Hashtable<String, Integer> allelepos = new Hashtable<String,Integer>(); 
    private String chrom;
    private int chromlen;
    private int startRange;
    private int endRange;
    private int startMain;
    private byte[] refsequence;
    private BufferedWriter bw;
    private genotyper_params gpars;
    double e = 0.05;
    double p_homo = -0.018483406; // log10(1 - 5*e/6);
    double p_het = -0.315753252;  //  log10(0.5 -e/3);
    double p_rest = -2.079181246;
    private int[] filter_count = new int[10];
    private ArrayList<Integer> pos2geno = null;
    private ArrayList<String> altall2geno = null;
    private HashMap pos2genoH = null;
    
    public pgenotyper(String chromIn, int chrlenIN, int startRangeIn, int endRangeIn, int startMainIn, 
            ArrayList<String> SampleListIn, byte[] refsequenceIn, int[][][] dataIn, int [][] qcdataIn, 
            genotyper_params gparams, ArrayList<Integer> pos2genoIn, ArrayList<String> altall2genoIn)
    {
        chrom = chromIn;
        // Initialize the allele positions in extracted mpileup file
        allelepos.put("A", 0);
        allelepos.put("C", 1);
        allelepos.put("G", 2);
        allelepos.put("T", 3);
        allelepos.put("I", 4);
        allelepos.put("D", 5);
        
        gpars = gparams;
        
        // Load chromosome length
        //chromlen = h5writereference.chromosome_lengths(gpars.getGversion()).get(chrom);
        chromlen = chrlenIN;
        
        SampleList = SampleListIn;
        numSamples = SampleList.size();
        // Remove paths from SampleList entires
        for(int i=0;i<numSamples;i++)
        {
            String [] fullname = SampleList.get(i).split("/");
            SampleList.set(i, fullname[fullname.length-1]);
        }
        
        refsequence = refsequenceIn;
        data = dataIn;
        qcdata = qcdataIn;
        
        startRange = startRangeIn;
        endRange = endRangeIn;
        if(endRange > chromlen)
        {
            endRange = chromlen;
        }
        startMain = startMainIn;
        
        // Set the probability logs for multinomial genotyper
        p_homo = Math.log10(1-5*gpars.getE()/6);
        p_het = Math.log10(0.5 - gpars.getE()/3);
        p_rest = Math.log10(gpars.getE()/6);
        
        // Initialize filter count
        Arrays.fill(filter_count, 0);
        
        // Set the positions/alternative alleles to genotype on
        // Procude "run" will execute a different path if these are not null
        pos2geno = pos2genoIn;
        altall2geno = altall2genoIn;
        
        // Representing selected positions as keys of a hash is faster when checking in loop
        if(pos2genoIn != null)
        {
            pos2genoH = new HashMap();
            for(int i=0;i<pos2genoIn.size();i++)
            {
                if(pos2genoIn.get(i) >= startRange && pos2genoIn.get(i) <= endRange)
                {
                    pos2genoH.put(pos2genoIn.get(i), true);
                }
            }
        }
                
    }
    
    public void run()
    {
        long startTime;
        long endTime;
        // Open output file for thread
        try
        {
            bw = new BufferedWriter(new FileWriter("thr.c"+chrom+"."+startRange+"-"+endRange));
        }
        catch(Exception e)
        {
            System.out.println("output file could not be open"+e);
        }
        System.out.println("Thread " + startRange + "-" + endRange + " starting");
        startTime = System.currentTimeMillis();
        if(pos2geno != null && altall2geno != null && !(gpars.getSummary_depth_only()))
        {
            genotype_on_positions_set();
        }
        else if(gpars.getSummary_depth_only())
        {
            makeDepthSummary();
        }
        else
        {
            // This will run even if pos2geno present if altall2geno absent
            genotype_range();
        }
        endTime = System.currentTimeMillis();
        double duration = 0.001*(endTime - startTime)/60;
        System.out.println("Thread " + startRange + "-" + endRange + " took "+ duration +" minutes");
        try
        {
            bw.close();
        }
        catch(Exception e)
        {
            System.out.println("File could not be closed" + e);
        }
        
        // Print filter stats
        print_filter_stats();
    }
    
    public void makeDepthSummary()
    {
        
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int numsites = endRangeNorm - startRange + 1;
        int posoffset = startRange - startMain;
        int [] alleledepth = new int[6];
        int [] alleleqc = new int[6];
        int depth_tot;
        int allpos = 0;
        
        // Print the first line
        if(startRange == startMain)
        {
            String firstline = "#CHR\tPOS\tREF\tCOUNT_A\tQ_A\tCOUNT_C\tQ_C\tCOUNT_G\tQ_G\tCOUNT_T\tQ_T\t";
            firstline += "COUNT_I\tQ_I\tCOUNT_D\tQ_D";
            try
            {
                bw.write(firstline+"\n");
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write first line to file"+e);
            }
        }
        
        // Loop over sites
        for(int pos=0;pos<numsites;pos++)
        {
            String refbase = Character.toString((char)refsequence[pos+posoffset]).toUpperCase();
            int pos_actual = pos + startRange; // Actual positiono on the chromosome
            int pos_relative = pos + posoffset; // Position relative to startMain
            
            // If position file specified, only process positions present in that file
            if(pos2genoH != null)
            {
                if( ! pos2genoH.containsKey(pos_actual) ) { continue; }
            }
            Arrays.fill(alleledepth,0);
            for(int i=0;i<numSamples;i++)
            {
                // Loop over alleles
                for(int j=0;j<6;j++)
                {
                    alleledepth[j] += data[i][j][pos_relative];
                }
            }
            // qcdata contains qc scores, by allele, summed over all reads carrying this allele
            // Here we will compute the average over all such reads
            depth_tot = 0;
            for(int j=0;j<6;j++)
            {
                depth_tot += alleledepth[j];
                alleleqc[j] = alleledepth[j]>0?qcdata[j][pos_relative]/alleledepth[j]:0;
            }
            
            // Totals collected - can print them out if there is anything to print
            if(depth_tot == 0) { continue;}
            try
            {
                bw.write(chrom + "\t" + pos_actual + "\t" + refbase);
                for(int j=0;j<6;j++)
                {
                    bw.write("\t" + alleledepth[j] + "\t"+alleleqc[j]);
                }
                bw.write("\n");
            }
            catch(Exception e)
            {
                 System.out.print("ERROR writing to file: "+e);
            }
        }
    }
    
    public void genotype_on_positions_set()
    {
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        
        // Print the first line
        if(startRange == startMain)
        {
            String firstline = "#CHR\tPOS\tREF\tALT";
            if(gpars.getOutfmt().equalsIgnoreCase("vcf"))
            {
                firstline = OutputHeader(gpars.getOutfmt());
            }
            for(int i=0;i<SampleList.size();i++)
            {
                firstline += "\t"+SampleList.get(i).replaceAll("\\.h5", "");
            }
            try
            {
                bw.write(firstline+"\n");
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write first line to file"+e);
            }
        }
        
        // Loop over positions in position file, skip the ones outside of my range
        for(int i=0;i<pos2geno.size();i++)
        {
            int pos = pos2geno.get(i);
            if(pos < startRange)
            {
                continue;
            }
            if(pos > endRangeNorm)
            {
                break;
            }
            
            // This position is mine - proceed with geotyping
            String refbase = Character.toString((char)refsequence[pos - startMain]).toUpperCase();
            String [] altalleles = altall2geno.get(i).split(",");
            int matcount = altalleles.length+1;
            int [] num_mat = new int[matcount];
            
            // Start building the output line for this position
            StringBuilder outstrbld = new StringBuilder();
            String outstr; // = chrom + "\t" + pos + "\t" + refbase + "\t" + altall2geno.get(i);
            if(gpars.getOutfmt().equalsIgnoreCase("RBfmt"))
            {
                outstr = chrom + "\t" + pos + "\t" + refbase + "\t" + altall2geno.get(i);
            }
            else
            {
                // we'll set INFO to missing data
                outstr = chrom + "\t" + pos + "\t.\t" + refbase + "\t" + altall2geno.get(i).replaceAll("N", ".").replaceAll("D", "<DEL>").replaceAll("I", "<INS>");
                outstr += "\t.\t.\t.\tGT:AD:GL";
            }
            outstrbld.append(outstr);
                    
            for(int j=0;j<numSamples;j++)
            {
                // Fill up the allele depths - the reference allele comes first
                num_mat[0] = 0;
                if(! refbase.equalsIgnoreCase("N"))
                {
                    num_mat[0] = data[j][allelepos.get(refbase)][pos - startMain];
                }
                String depthstr = String.valueOf(num_mat[0]);
                int depth_tot = num_mat[0];
                for(int k=1;k<matcount;k++)
                {
                    if(! altalleles[k-1].equalsIgnoreCase("N"))
                    {
                        num_mat[k] = data[j][allelepos.get(altalleles[k-1])][pos - startMain];
                    }
                    else
                    {
                        num_mat[k] = 0;
                    }
                    depthstr += "," + String.valueOf(num_mat[k]);
                    depth_tot += num_mat[k];
                }
                
                if(depth_tot == 0)
                {
                    outstrbld.append("\t./.");
                    continue;
                }
                
                // Compute genotype likelihoods
                ArrayList<Double> lkhds = new ArrayList<Double>();
                double lkh_max = -9999999;
                String genotype = "0/0";
                for(int ia1=0;ia1<matcount;ia1++)
                {
                    for(int ia2=ia1;ia2<matcount;ia2++)
                    {
                        double lll = genotype_likelihood(ia1,ia2,num_mat, matcount);
                        lkhds.add(lll);
                        if(lll > lkh_max)
                        {
                            lkh_max = lll;
                            genotype = String.valueOf(ia1) + "/" + String.valueOf(ia2);
                        }
                        
                    }
                }
                // Normalize likelihoods and append a string to display
                String lkhdstr = "";
                for(int ia=0;ia<lkhds.size();ia++)
                {
                    int lll = (int)(-10*(lkhds.get(ia) - lkh_max) + 0.5);
                    lkhdstr += String.valueOf(lll) + ",";
                }
                lkhdstr = "\t" + genotype + ":" + depthstr + ":" + lkhdstr.substring(0, lkhdstr.length()-1);
                
                outstrbld.append(lkhdstr);
                
            } // loop over samples
            
            outstrbld.append("\n");
            outstr = outstrbld.toString();
            try
            {
                bw.write(outstr);
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write to file"+e);
            }
        } // loop over positions
    }
    
    public void genotype_range()
    {
        boolean output_all_sites = gpars.getOutput_all_sites(); // If true, all filters will be disabled (except for no coverage filter)
        
        // depth-based thresholds
        int nonzero_cov_thr = gpars.getNonzero_cov_thr(); // sample considered to have nonzero coverage if its total depth is larger that this
        int nonzero_all_cov_thr = gpars.getNonzero_all_cov_thr(); //allele is considered to occur in a sample if its depth is larger than this
        int numaltall_max = gpars.getNumaltall_max();  // maximum number of retained alternative alleles
        int all_retension_thr = gpars.getAllele_retension_thr(); // see explanation for samp_all_retension_thr below
        int samp_all_retension_thr = gpars.getSamp_allele_retension_thr(); // allele is ignored if its combined depth over all samples is less than allele_retension_thr 
        // OR if no individual sample has depth of this allele at least samp_allele_retension_thr
        double minnumsampcov = gpars.getMinnumsampcov(); // minimum fraction of samples with coverage to include the site
        double pvalthr = gpars.getPvalthr();    // allele rejected if pvalue of contingency table larger than this
        int cntg_thr = gpars.getCntg_thr();        // sample will be part of contingency table test if reference+1st_minor allele depth is greater than this
        int depth_retained_alleles_thr = gpars.getDepth_retained_alleles_thr();    // sample will NOT be used in Ed's factor tests if depth over all retained alleles is smaller than this
        int depth_alt_thr = gpars.getDepth_alt_thr(); // overall depth of minor alleles has to be bigger than this to keep the position
        int max_depth_alt_thr =gpars.getMax_depth_alt_thr(); // total depth must be at least this in at least one sample with alt genotype to keep the position even if number of alt alleles too low
        // genotype-based thresholds
        int samps_with_ret_all_thr = gpars.getSamps_with_ret_all_thr(); // site rejected if number of samples with genotypes 0/0 + 0/1 + 1/1 smaller than this
        int nall_after_genotyping_thr = gpars.getNall_after_genotyping_thr(); // retain at most this many alleles after genotyping
        double alt_num_thr = gpars.getAlt_num_thr(); // site kept if number of minor allele more than this
        double edfactor_reject_thr = gpars.getEdfactor_reject_thr();  // site will be rejected if Ed's factor greater than this (0.1 value is  arbitrary) 
        
        boolean ishet = false;   // to be used in counting hets for Ed's factor
        //int[][] gnum_mat = new int[6][6]; // to hold the number of samples of different genotypes
        
        int baseQCaverageType=2; // 1: equal weigths (1/N_samples); 2: weights propportional to fraction of ellele reads coming from this sample
        
        String outformat = gpars.getOutfmt();
        boolean useCochran = gpars.getUseCochran(); // skip contmat simulation if Cochran's cond. satisfied
        boolean cochransCrit;
        double cmatMaxThr=gpars.getCmatMaxThr(); // in the future, get it from parameters file
        
        // For contingency table simulations
        int seed = 123456789;
        Random generator = new Random(seed);
        int contmatver = gpars.getContmatver();
        FisherExact FEtest=null;
        int FEmax = 10000;
        double cmatsum = 0;
        
        // prepare Fisher Exact test, if applicatble
        if(contmatver >= 6)
        {
            FEtest = new FisherExact(FEmax);
        }
        
        long startTime = System.currentTimeMillis();
        long endTime;
        double duration;
        double auxdouble;
        
        int endRangeNorm = endRange;
        if(endRangeNorm > chromlen)
        {
            endRangeNorm = chromlen;
        }
        int numsites = endRangeNorm - startRange + 1;
        Hashtable<String,Integer> sampcount = new Hashtable<String,Integer>();
        int posoffset = startRange - startMain;
        int [] allele_depths = new int[6];
        int [] max_allele_depths = new int[6];
        //int [] qcdata0 = new int[6];
        int allpos = 0;
        
        // Print the first line
        if(startRange == startMain)
        {
            //String firstline = "#CHR\tPOS\tREF\tALT\tINFO";
            String firstline = OutputHeader(outformat);
            if(gpars.getOutput_genotypes())
            {
                for(int i=0;i<SampleList.size();i++)
                {
                    firstline += "\t"+SampleList.get(i).replaceAll("\\.h5", "");
                }
            }
            try
            {
                bw.write(firstline+"\n");
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write first line to file"+e);
            }
        }
        
        // Loop over sites
        for(int pos=0;pos<numsites;pos++)
        {
            // rewrite data for position pos into a new local table - maybe it will be fastr
            /*
            int [][] data1 = new int[numSamples][6];
            for(int i=0;i<numSamples;i++)
            {
                for(int j=0;j<6;j++)
                {
                    data1[i][j] = data[i][j][pos+posoffset];
                }
            }
             * 
             */
            
            String infostr = "";  // will contain various info about filters etc.
            
            String refbase = Character.toString((char)refsequence[pos+posoffset]).toUpperCase();
            int pos_actual = pos + startRange; // Actual positiono on the chromosome
            int pos_relative = pos + posoffset; // Position relative to startMain
            
            // If position file specified, only process positions present in that file
            if(pos2genoH != null)
            {
                if( ! pos2genoH.containsKey(pos_actual) ) { continue; }
            }
            
            // Skip the site if reference unknown - maybe this is not right
            //if(refbase.equalsIgnoreCase("N"))
            //{
            //    filter_count[0]++;
            //    continue;
            //}
            // Find total depth over all samples and number of samples with non-zero coverage
            int totcov = 0;
            int samplesnonzerocov = 0;
            int [] totdepth = new int[numSamples];
            Arrays.fill(totdepth,0);
            for(int i=0;i<numSamples;i++)
            {
                totdepth[i] += data[i][0][pos_relative]+data[i][1][pos_relative]+data[i][2][pos_relative]
                            +data[i][3][pos_relative]+data[i][4][pos_relative]+data[i][5][pos_relative];
                totcov += totdepth[i];
                if(totdepth[i] > nonzero_cov_thr) { samplesnonzerocov++; }
            }
            // Filter positions with no coverage
            if(totcov == 0) { filter_count[1]++; continue; }
            // Filter positions with coverage in less than threshold of samples
            if(samplesnonzerocov < minnumsampcov) { filter_count[2]++; continue; }
            infostr += "DP="+totcov+";NZ="+samplesnonzerocov+";";
            
            // For each allele, find the number of samples it "occurs" in
            // allele "occurs" in sample if its depth in this sample is greater than nonzero_all_cov_thr
            // Also, find the overall allele depths
            for(int ia=0;ia<6;ia++)
            {
                sampcount.put(alleles[ia], 0);
                allele_depths[ia] = 0;
                max_allele_depths[ia] = 0;
            }
            for(int i=0;i<numSamples;i++)
            {
                for(int ia=0;ia<6;ia++)
                {
                    int samplealldepth = data[i][allelepos.get(alleles[ia])][pos+posoffset];
                    if(samplealldepth > nonzero_all_cov_thr)
                    {
                        sampcount.put(alleles[ia], sampcount.get(alleles[ia]) + 1);
                        allele_depths[ia] += samplealldepth;
                        if(samplealldepth > max_allele_depths[ia]) max_allele_depths[ia] = samplealldepth;               }
                }
            }
            
            // zero out sampcount for alleles where overall allele depth is below threshold
            // After sorting these will end up at the end and will not be considered later
            for(int ia=0;ia<6;ia++)
            {
                if(allele_depths[ia] < all_retension_thr || max_allele_depths[ia] < samp_all_retension_thr) 
                { 
                    sampcount.put(alleles[ia], 0);
                    allele_depths[ia] = 0;
                }
            }
            
            // Sort aleles according to the number of occurrence accross all samples
            final int [] allele_nums = {sampcount.get("A"), sampcount.get("C"), sampcount.get("G"),sampcount.get("T"),sampcount.get("I"),sampcount.get("D") };
            ArrayList<Integer> index = new ArrayList<Integer>();
            for(int ia=0;ia<6;ia++)
            {
                index.add(ia);
            }
            
            // Construct the comparator to sort in reverse order
            Comparator<Integer> cmp = new Comparator<Integer>(){
                @Override
                public int compare(Integer a, Integer b){
                    return allele_nums[b]-allele_nums[a];
            }};
            
            Collections.sort(index, cmp);
           
            // Save sorted version of alleles, allele_nums, and qcdata
            // Normalize qcdata dividing by number of samples the allele occurs in
            String [] alleles1 = new String [6];
            //for(int ia=0;ia<6;ia++)
            //{
            //   qcdata0[ia] = 0;
            //    if(allele_nums[ia]>0) { qcdata0[ia] = qcdata[ia][pos + posoffset]/allele_nums[ia]; }
            // }
            for(int ia=0;ia<6;ia++)
            {
                int ind = index.get(ia);
                alleles1[ia] = alleles[ind];
            }
            
            // Test printout
            /*
            System.out.println("initial allele depths: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles[ia] + ":" + allele_depths[ia] +"\t");
            }
            System.out.print("\n");
            System.out.println("sorted allele depths: ");
            for(int ia=0;ia<6;ia++)
            {
                System.out.print(alleles1[ia] + ":" + allele_depths[index.get(ia)] +"\t");
            }
            System.out.print("\n");
            * 
            */
             
            
            // Construct the alternative allele string. Retain only up to numaltall_max alternative alleles
            // Allele is retained if there is enough reads supporting it across all samples - others
            // have their sampcounts zero'ed out already
            String allele_str = "";
            int numaltall = 0;
            for(int ia=0;ia<6;ia++)
            {
                if(allele_depths[index.get(ia)] >0 && numaltall < numaltall_max)
                {
                    if(! alleles1[ia].equals(refbase)) 
                    { 
                        allele_str += alleles1[ia] + ",";
                        numaltall++;
                    }
                }
                else
                {
                    break;
                }
            }
            
            // If no variation detected, proceed to the next position
            // (if no reference base given all positions with non-zero coverage will be written
            if(numaltall == 0)
            {
                filter_count[3]++;
                if(! output_all_sites) continue;     // continue the loop over positions
            }
            // This allele_str will not be used if the final, genotype-based sorting is in effect
            // allele_str = allele_str.substring(0, allele_str.length()-1);  // remove trailing comma
            
            // Start building the output line for this position
            StringBuilder outstrbld = new StringBuilder();
            String outstr;
            if(outformat.equalsIgnoreCase("RBfmt"))
            {
                outstr = chrom + "\t" + pos_actual + "\t" + refbase + "\t";
            }
            else
            {
                outstr = chrom + "\t" + pos_actual + "\t.\t" + refbase + "\t";
            }
            //outstrbld.append(outstr);
            
            // Generate allele depths and add this info to the output string
            int samples_for_cntg = 0;        // count samples to include in contingency table (now not used)
            int samps_with_ret_all = 0;    // count samples with depth in retained alleles (for Ed's factor)
            int num_of_hets = 0;
            int num_alt_all = 0;
            int depth_alt_all = 0;           // count combined depth of all non-reference alleles
            String depth_alt_all_str = "";
            String genallele_nums_str = "";
            String gntp_nums_str = "";
            int [][] num_mat = new int[numSamples][7];  // sample-wise allele depths - kept only for reference nad retained alt alleles
                                                        // in principle, must account for total of 7 alleles (if reference is N)
            int [][] lkhd_mat = new int [numSamples][28];  // hold normalized likelihoods, 28 = 7*(7+1)/2
            String [] genotype_mat = new String [numSamples]; // To hold genotypes for final re-sorting
            int [] depth_alt = {0,0,0,0,0,0,0};
            int [] genallele_nums = {0,0,0,0,0,0,0};   // Numbers of alleles in genotypes
            int [] qc_mat = {0,0,0,0,0,0,0};
            int [][] gnum_mat = new int[7][7];
            zero2Dmat(gnum_mat);
            int max_depth_alt = 0;    // record max depth amog all samples with genotypes containin any alt alleles
            int [] amax_depth_alt = {0,0,0,0,0,0,0}; //record max genotype depth among all samples with genotypes containing a prticular allele
            for(int i=0;i<numSamples;i++)
            {
                int matcount = 0;
                
                // Add depth of reference base as the first element - once reference base is known
                // If reference is N, keep it as zero
                num_mat[i][matcount] = 0;
                if(! refbase.equalsIgnoreCase("N"))
                {
                    allpos = allelepos.get(refbase);
                    //num_mat[i][matcount] = data[i][allpos][pos+posoffset];
                    num_mat[i][matcount] = data[i][allpos][pos+posoffset]>nonzero_all_cov_thr?data[i][allpos][pos+posoffset]:0;
                    if(i==0) 
                    {
                        if(baseQCaverageType ==1)
                        {
                            qc_mat[matcount] = (allele_nums[allpos]>0)?qcdata[allpos][pos+posoffset]/allele_nums[allpos]:0; 
                        }
                        else
                        {
                            qc_mat[matcount] = (allele_depths[allpos]>0)?qcdata[allpos][pos+posoffset]/allele_depths[allpos]:0; 
                        }
                    }
                }
                
                int depth_retained_alleles = num_mat[i][matcount];
                depth_alt[matcount] += num_mat[i][matcount];
                String depth_str = String.valueOf(num_mat[i][matcount]) + ",";
                //outstrbld.append(num_mat[i][matcount]).append(",");
                matcount++;
                for(int ia=0;ia<6;ia++)
                {
                    if(matcount < numaltall + 1)
                    {
                        if(! alleles1[ia].equals(refbase))
                        {
                            allpos = allelepos.get(alleles1[ia]);
                            //num_mat[i][matcount] = data[i][allpos][pos+posoffset];
                            num_mat[i][matcount] = data[i][allpos][pos+posoffset]>nonzero_all_cov_thr?data[i][allpos][pos+posoffset]:0;
                            depth_retained_alleles += num_mat[i][matcount];
                            depth_alt_all += num_mat[i][matcount];
                            depth_alt[matcount] += num_mat[i][matcount];
                            //outstrbld.append(num_mat[i][matcount]).append(",");
                            depth_str += String.valueOf(num_mat[i][matcount]) + ",";
                            if(i==0) 
                            {
                                if(baseQCaverageType ==1)
                                {
                                    qc_mat[matcount] = (allele_nums[allpos]>0)?qcdata[allpos][pos+posoffset]/allele_nums[allpos]:0; 
                                }
                                else
                                {
                                    qc_mat[matcount] = (allele_depths[allpos]>0)?qcdata[allpos][pos+posoffset]/allele_depths[allpos]:0;
                                }
                            }
                            matcount++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //outstrbld.deleteCharAt(outstrbld.length()-1).append("\t");
                
                // If depth is too low over all retained alleles, mark this sample "missing data" and do not bother genotyping it
                // This filter is neded because some samples may have depth only in skipped alleles
                if(depth_retained_alleles < Math.max(depth_retained_alleles_thr, 1))
                {
                    //outstrbld.append("./.\t");
                    genotype_mat[i] = "./.";
                    continue;
                }
                if(num_mat[i][0] + num_mat[i][1] > cntg_thr) samples_for_cntg++; // now defunct
                
                samps_with_ret_all++;              // This sample counts as having retained alleles
                
                // Add genotype likelihood calculations here
                // This should actually be done in a separate loop after contingency table test (if any)
                // Will keep it here for now - relevant for Ed's factor test
                ArrayList<Double> lkhds = new ArrayList<Double>();
                double lkh_max = -9999999;
                String genotype = "0/0";
                int ig1 = 0;
                int ig2 = 0;
                for(int ia1=0;ia1<matcount;ia1++)
                {
                    for(int ia2=ia1;ia2<matcount;ia2++)
                    {
                        double lll = genotype_likelihood(ia1,ia2,num_mat[i], matcount);
                        lkhds.add(lll);
                        if(lll > lkh_max)
                        {
                            lkh_max = lll;
                            ig1 = ia1;
                            ig2 = ia2;
                            genotype = String.valueOf(ia1) + "/" + String.valueOf(ia2);
                            if(ia1 == ia2)
                            {
                                ishet = false;
                            }
                            else
                            {
                                ishet = true;
                            }
                        }
                        
                    }
                }
                genallele_nums[ig1]++;
                genallele_nums[ig2]++;
                gnum_mat[ig1][ig2]++;
                if(ig1 != ig2) { gnum_mat[ig2][ig1]++; } // making this symmetric is important for subsequent resorting
                genotype_mat[i] = genotype;
                
                int geno_depth = num_mat[i][ig1] + num_mat[i][ig2];
                if(geno_depth > amax_depth_alt[ig1]) { amax_depth_alt[ig1] = geno_depth; }
                if(geno_depth > amax_depth_alt[ig2]) { amax_depth_alt[ig2] = geno_depth; }
                
                // Update numbers needed for Ed's factor test - this really needs to be done after re-sorting
                if(! genotype.equals("0/0"))
                {
                    if(ishet)
                    {
                        if(genotype.indexOf("0") > -1)
                        {
                            num_alt_all++; // hets with one alt allele, like 0/1, 0/2, ...
                        }
                        else
                        {
                            num_alt_all += 2;  // hets with two alt alleles, like 1/2, 2/3, ....
                        }
                    }
                    else
                    {
                        num_alt_all += 2;      // homozygotes with alt alleles
                    }
                    // Update max depth among samples with genotypes containing any alt allele
                    //if(totdepth[pos][i] > max_depth_alt)
                    //{num_mat
                    //    max_depth_alt = totdepth[pos][i];
                    //}
                    if(totdepth[i] > max_depth_alt)
                    {
                        max_depth_alt = totdepth[i];
                    }
                }
                
                
                // Normalize likelihoods and append a string to display
                String lkhdstr = "";
                for(int ia=0;ia<lkhds.size();ia++)
                {
                    int lll = (int)(-10*(lkhds.get(ia) - lkh_max) + 0.5);
                    lkhd_mat[i][ia] = lll;
                    lkhdstr += String.valueOf(lll) + ",";
                }
                lkhdstr = genotype + ":" + depth_str.substring(0,depth_str.length()-1) + ":" + lkhdstr.substring(0, lkhdstr.length()-1);
                
                //outstrbld.append(lkhdstr).append("\t");
                
            }  // loop over samples (computing depths and genotype likelihoods
            
            // Skip if not enough overall combined depth in alternative alleles
            // Note: depth in alt alleles does not mean a lot of genotypes containing them
            // e.g., if overall depth is high
            if(depth_alt_all < depth_alt_thr) { filter_count[4]++; continue; }
            // Skip if not enough samples with retained alleles
            if(samps_with_ret_all < samps_with_ret_all_thr) { filter_count[5]++; continue; }
            // Skip if not enough of minor (really: non-reference) alleles found and genotype not well suported by reads in any sample
            //if(num_alt_all <= alt_num_thr && max_depth_alt < max_depth_alt_thr) { filter_count[6]++; continue; }
            
            // Remove any alternative alleles that where MAF is too small AND total max (over samples) depth too small
            // We do this by zeroeing out all arrays, so that sorting will put such offending alleles at the end
            for(int ia=1;ia<7;ia++)
            {
                if(genallele_nums[ia] <= alt_num_thr && amax_depth_alt[ia] < max_depth_alt_thr)
                {
                    genallele_nums[ia] = 0;
                    depth_alt[ia] = 0;
                    qc_mat[ia] = 0;
                    filter_count[6]++;
                }
            }
            
            // Do contingency test on all pairs of surviving alleles
            double [][] PVmat = new double[7][7];
            for(int ia=0;ia<6;ia++)
            {
                for(int ib=ia+1;ib<7;ib++)
                {
                    PVmat[ia][ib] = 1.0;
                    PVmat[ib][ia] = 1.0;
                    if(genallele_nums[ia]>0 && genallele_nums[ib]>0)
                    {
                        // First, count samples with data in the alleles considered
                        int nonzersamp = 0;
                        double [][] cmat;
                        double cmatMax = 0.0;
                        int numhets = 0, numhoma=0, numhomb=0; 
                        
                        // Version with columns=taxa
                        if(contmatver == 1)
                        {
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] + num_mat[i][ib] > 0) { nonzersamp++; }
                        }
                        // Create contingency table
                        cmat = new double[2][nonzersamp];
                        int counter = 0;
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] + num_mat[i][ib] > 0)
                            {
                                cmat[0][counter] = num_mat[i][ia];
                                cmat[1][counter] = num_mat[i][ib];
                                if(cmat[0][counter]>cmatMax){ cmatMax = cmat[0][counter]; }
                                if(cmat[1][counter]>cmatMax){ cmatMax = cmat[1][counter]; }
                                counter++;
                            }
                        }
                        }
                        else if(contmatver==2)
                        {
                        // version with squished contingency table,
                        // i.e., columns = (homa, homb, hets), whichever is present
                        // First, count samples with data in the alleles considered
                        boolean arehoma = false;
                        boolean arehomb = false;
                        boolean arehets = false;                    
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { arehets=true; } else if(num_mat[i][ia] > 0 && num_mat[i][ib] == 0)
                            { arehoma=true; } else if(num_mat[i][ia] == 0 && num_mat[i][ib] > 0)
                            { arehomb=true; }
                        }
                        if(arehets) { nonzersamp++; }
                        if(arehoma) { nonzersamp++; }
                        if(arehomb) { nonzersamp++; }
                        // Create contingency table
                        cmat = new double[2][nonzersamp];
                        Arrays.fill(cmat[0],0.0);
                        Arrays.fill(cmat[1],0.0);
                        // Which column corresponds to what
                        int ixhoma = -1;
                        int ixhomb = -1;
                        int ixhets = -1;
                        if(nonzersamp == 3)
                        {
                            ixhoma = 0;
                            ixhomb = 1;
                            ixhets = 2;
                        }
                        else if(nonzersamp == 2)
                        {
                            if(! arehoma)
                            {
                                ixhomb = 0;
                                ixhets = 1;
                            }
                            else if(! arehomb)
                            {
                                ixhoma = 0;
                                ixhets = 1;
                            }
                            else
                            {
                                ixhoma = 0;
                                ixhomb = 1;
                            }
                        }
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { cmat[0][ixhets] += num_mat[i][ia]; cmat[1][ixhets] += num_mat[i][ib]; } else if(num_mat[i][ia] > 0 && num_mat[i][ib] == 0)
                            { cmat[0][ixhoma] += num_mat[i][ia]; cmat[1][ixhoma] += num_mat[i][ib]; } else if(num_mat[i][ia] == 0 && num_mat[i][ib] > 0)
                            { cmat[0][ixhomb] += num_mat[i][ia]; cmat[1][ixhomb] += num_mat[i][ib];}
                        }
                        }
                        else if(contmatver == 3 || contmatver == 4) 
                        {
                        // another version with squished contingency table,
                        // i.e., columns = (homa, homb, hets1, het2, ...), whichever is present
                        // i.e., all taxa heterozygous in reads are separated
                        // First, count samples with data in the alleles considered
                        boolean arehoma = false;
                        boolean arehomb = false;
                        numhets = 0; numhoma=0; numhomb=0;                    
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { numhets++; } else if(num_mat[i][ia] > 0 && num_mat[i][ib] == 0)
                            { arehoma=true; numhoma++;} else if(num_mat[i][ia] == 0 && num_mat[i][ib] > 0)
                            { arehomb=true; numhomb++;}
                        }
                        nonzersamp += numhets;
                        if(arehoma) { nonzersamp++; }
                        if(arehomb) { nonzersamp++; }
                        System.out.println("Position: "+pos_actual+" columns: "+nonzersamp);
                        // Create contingency table
                        cmat = new double[2][nonzersamp];
                        Arrays.fill(cmat[0],0.0);
                        Arrays.fill(cmat[1],0.0);
                        
                        // Which column corresponds to what
                        int ixhoma = -1;
                        int ixhomb = -1;
                        int ixhets = -1;
                        if(arehoma && arehomb)
                        {
                            ixhoma = 0;
                            ixhomb = 1;
                            ixhets = 2;   // Hets will start in this column
                        }
                        else if(arehoma && (! arehomb))
                        {
                            ixhoma = 0;
                            ixhets = 1;   // Hets will start in this column
                        }
                        else if((! arehoma) && arehomb)
                        {
                            ixhomb = 0;
                            ixhets = 1;   // Hets will start in this column
                        }
                        else // no homs at all
                        {
                            ixhets = 0;
                        }
                        int hetcount = 0;
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { cmat[0][ixhets+hetcount] += num_mat[i][ia]; cmat[1][ixhets+hetcount] += num_mat[i][ib]; hetcount++;} 
                            else if(num_mat[i][ia] > 0 && num_mat[i][ib] == 0)
                            { cmat[0][ixhoma] += num_mat[i][ia]; } 
                            else if(num_mat[i][ia] == 0 && num_mat[i][ib] > 0)
                            { cmat[1][ixhomb] += num_mat[i][ib];}
                        }
                        if(contmatver == 4)
                        {
                            if(numhoma>0) {cmat[0][ixhoma] = (int)(cmat[0][ixhoma]/numhoma) + 1; }
                            if(numhomb>0) {cmat[1][ixhomb] = (int)(cmat[1][ixhomb]/numhomb) + 1; }
                        }
                        }
                        else if(contmatver == 5)// contmatver == 5
                        {
                        // yet another version with squished contingency table,
                        // i.e., columns = (homa1, homa2,... homb1,homb2,... hets1, het2, ...), whichever is present
                        // i.e., all taxa heterozygous in reads are separated
                        // all homozygotes of a given allele deph are represented by one column
                        numhets = 0; numhoma=0; numhomb=0;
                        HashMap used_depths_a = new HashMap();
                        HashMap used_depths_b = new HashMap();
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { numhets++; } 
                            else if(num_mat[i][ia] > 0 && num_mat[i][ib] == 0)
                            { 
                                if(! used_depths_a.containsKey(num_mat[i][ia]))
                                {
                                    used_depths_a.put(num_mat[i][ia], true);
                                    numhoma++;
                                }
                            } 
                            else if(num_mat[i][ia] == 0 && num_mat[i][ib] > 0)
                            {
                                if(! used_depths_b.containsKey(num_mat[i][ib]))
                                {
                                    used_depths_b.put(num_mat[i][ib], true);   
                                    numhomb++;
                                }
                            }
                        }
                        nonzersamp += numhets + numhoma + numhomb;
                        //System.out.println("Position: "+pos_actual+" columns: "+nonzersamp + " hets,homa,homb:" + numhets + " "+numhoma + " "+numhomb);
                        // Create contingency table
                        cmat = new double[2][nonzersamp];
                        Arrays.fill(cmat[0],0.0);
                        Arrays.fill(cmat[1],0.0);
                        int ixcount = 0;
                        // Columns for a homozygotes
                        Object [] uda = used_depths_a.keySet().toArray();
                        for(int i=0;i<uda.length;i++)
                        {
                            cmat[0][ixcount] = (Integer)uda[i];
                            ixcount++;
                        }
                        // Columns for b homozygotes
                        Object [] udb = used_depths_b.keySet().toArray();
                        for(int i=0;i<udb.length;i++)
                        {
                            cmat[1][ixcount] = (Integer)udb[i];
                            ixcount++;
                        }
                        for(int i=0;i<numSamples;i++)
                        {
                            if(num_mat[i][ia] > 0 && num_mat[i][ib] > 0) 
                            { 
                                cmat[0][ixcount] += num_mat[i][ia]; 
                                cmat[1][ixcount] += num_mat[i][ib];
                                ixcount++;
                            } 
                        }
                        }
                        else if(contmatver == 6 || contmatver == 7) // Ed's 2 by 2 array suggestion
                        {
                            cmat = new double[2][2];
                            // Compute first column (all data sums), check wich one is larger
                            for(int i=0;i<numSamples;i++)
                            {
                                cmat[0][0] += num_mat[i][ia];
                                cmat[1][0] += num_mat[i][ib];
                            }
                            int ia0 = ia;
                            int ib0 = ib;
                            if(cmat[1][0]>cmat[0][0])
                            {
                                ia0 = ib; ib0 = ia;
                                auxdouble = cmat[0][0]; cmat[0][0] = cmat[1][0]; cmat[1][0] = auxdouble; 
                            }
                            // Compute the second column (sums conditional on less abundant allele)
                            for(int i=0;i<numSamples;i++)
                            {
                                if(num_mat[i][ib0]>0)
                                {
                                    cmat[0][1] += num_mat[i][ia0];
                                    cmat[1][1] += num_mat[i][ib0]-1;
                                }
                            }
                            if(contmatver == 7)
                            {
                                cmat[1][1] = cmat[1][0];
                                cmat[1][0] = 0;
                                cmat[0][0] -= cmat[0][1];
                            }
                            cmatsum = cmat[0][0] + cmat[0][1] + cmat[1][0] + cmat[1][1];
                        }
                        else   //for all other values of contmatver, including the negative ones
                        {
                            cmat = null;
                        }
                        // Calculate/simulate P-value - check Cochran's criterion first....
                        double chisqpvalue=1.0;
                        if(contmatver >= 6) // Do Fisher exact test
                        {
                            // If not enough factorials pre-computed for FET, re-initialize
                            if(cmatsum > FEmax)
                            {
                                FEmax = (int)cmatsum + 10;
                                FEtest = new FisherExact(FEmax);
                            }
                            //chisqpvalue=FEtest.getTwoTailedP((int)cmat[0][0], (int)cmat[1][0], (int)cmat[0][1], (int)cmat[1][1]);
                            chisqpvalue=FEtest.getTwoTailedP((int)cmat[0][0], (int)cmat[0][1], (int)cmat[1][0], (int)cmat[1][1]);
                        }
                        else if(contmatver >0) // For larger tables, Chi-square + simulation combo
                        {
                            cochransCrit = false;
                            if(useCochran)
                            {
                                cochransCrit = ContingencyTables.cochransCriterion(cmat);
                            }
                            //if(cochransCrit)
                            //{
                                chisqpvalue = ContingencyTables.chiSquared(cmat, true);
                            //}
                            //else
                            if(chisqpvalue < 0.2 && ! cochransCrit )
                            {
                                if(cmatMax > cmatMaxThr && cmatMaxThr > 0)
                                {
                                    // Scaling cmat does not work too well. It seems to remove too many sites
                                    //double scfact = cmatMaxThr/cmatMax;
                                    //scaleCmat(cmat,scfact);
                                    // Cap all cmat elements at some maximum value
                                    capCmat(cmat,cmatMaxThr);
                                }
                                ContMatPack ooo = new ContMatPack(cmat);
                                seed = generator.nextInt(1000000000) + 12345;
                                chisqpvalue = ooo.SimulatePvalueAsa159(1000, seed); // 100 (number of iterations) should be input as parameter
                            }
                        }
                        // No calculation will be done if contmatver <= 0 - this is the way to skip this
                        // expensive test
                        chisqpvalue = Math.min(1.0, chisqpvalue);
                        PVmat[ia][ib] = chisqpvalue;
                        PVmat[ib][ia] = chisqpvalue;
                    }
                }
            }
            // For each alternative allele, check if at least one P-value is acceptable; if not - zero
            // out this allele'depth to exclude from sorting
            // First, we need to know What the major allele (by frequency) is?
            int almajor = 0;
            int ntmp = 0;
            for(int ia=0;ia<7;ia++)
            {
                if(genallele_nums[ia] > ntmp) { ntmp = genallele_nums[ia]; almajor = ia; }
            }
            // almajor is the index of the major allele
            double [] aminpv = new double[7];
            double globmin= 100;
            int iaglobmin = 100;
            int ibglobmin = 100;
            double minpv = 100.0;
            for(int ia=1;ia<7;ia++)
            {
                if(genallele_nums[ia] == 0) { continue; }
                minpv = 100.0;
                int ibmin = 100;
                for(int ib=0;ib<7;ib++)
                {
                    if(ib == ia) { continue; }
                    if(ia != almajor && ib != almajor) { continue; }
                    if(PVmat[ia][ib] < minpv) { minpv = PVmat[ia][ib]; ibmin = ib; }
                }
                aminpv[ia] = minpv;
                if(minpv < globmin) { globmin = minpv; iaglobmin = ia; ibglobmin = ibmin;} 
            }
            for(int ia=1;ia<7;ia++)
            {
                // eliminate allele if p-value too large (larger than threshold or larger than minimum
                // pvalue by more than 1.0 of threshold),
                // but always keep the
                // allele with the smallest p-value if all sites output requested
                // This way, pvalthr can always be kept reasonably small
                boolean skip_this_allele = ! ((ia == iaglobmin || ia == ibglobmin) && output_all_sites);
                skip_this_allele = skip_this_allele && (aminpv[ia] > pvalthr || aminpv[ia] - minpv > 1.0*pvalthr );
                if(skip_this_allele)
                {
                    genallele_nums[ia] = 0;
                    depth_alt[ia] = 0;
                    qc_mat[ia] = 0;
                    filter_count[9]++;
                }
            }
            // End of contingency table stuff
            //endTime = System.currentTimeMillis();
            //duration = 0.001*(endTime - startTime)/60;
            //startTime = endTime;
            //System.out.println("Thread " + startRange + "-" + endRange + " after conntab site " + pos_actual +" "+ duration +" minutes");
            
            int filtstat = processAfterGenotyping(genallele_nums, depth_alt, qc_mat, gnum_mat,
            numaltall, output_all_sites, nall_after_genotyping_thr, edfactor_reject_thr, numSamples,
            genotype_mat, num_mat, lkhd_mat, outstrbld,
            allele_str, PVmat, infostr, outstr, gpars.getOutput_genotypes());
            
            if(filtstat >0)
            {
                filter_count[filtstat]++;
                continue;
            }
            
            outstr = outstrbld.toString();
            try
            {
                bw.write(outstr);
            }
            catch(Exception e)
            {
                System.out.println("Thread could not write to file"+e);
            }
                
       } // loop over positions
        
    }   // end of genotype_range
    
    public static int processAfterGenotyping(int [] genallele_nums, int [] depth_alt, int [] qc_mat, int [][]gnum_mat,
            int numaltall, boolean output_all_sites, int nall_after_genotyping_thr, double edfactor_reject_thr, int numsamples,
            String [] genotype_mat, int [][] num_mat, int [][] lkhd_mat, StringBuilder outstrbld,
            String allele_strIn, double [][] PVmat, String infostrIn, String outstrIn, boolean output_genotypes)
    {
        double [] HWresult;
        // Sort alternative alleles according to genotyping results (allele frequencies)
            //
            final int [] genallele_nums_f = Arrays.copyOf(genallele_nums, 7);
            final int [] depth_alt_f = Arrays.copyOf(depth_alt, 7);
            final int [] qc_mat_f = Arrays.copyOf(qc_mat, 7);
            final int [][] gnum_mat_f = clone2Dmat(gnum_mat);
            ArrayList<Integer> index = new ArrayList<Integer>();
            for(int ia=1;ia<7;ia++)
            {
                index.add(ia);
            }
            Comparator cmp = new Comparator<Integer>(){
                @Override
                public int compare(Integer a, Integer b){
                    // Resolve on allele numbers
                    if(genallele_nums_f[b] != genallele_nums_f[a])
                    {
                        return genallele_nums_f[b]-genallele_nums_f[a];
                    }
                    // try tie-breaking on numbers of allele depths
                    if(depth_alt_f[b] != depth_alt_f[a])
                    {
                        return depth_alt_f[b] - depth_alt_f[a];
                    }
                    // Try tie--breaking on average base qualities
                    if(qc_mat_f[b] != qc_mat_f[a])
                    {
                        return qc_mat_f[b] - qc_mat_f[a];
                    }
                    // Finally, if everything else aquares up, try tie-breaking on the number of homozygotes
                    if(gnum_mat_f[b][b] != gnum_mat_f[a][a])
                    {
                        return gnum_mat_f[b][b] - gnum_mat_f[a][a];
                    }
                    return 0;
            }};
            Collections.sort(index, cmp);
            index.add(0, 0); // this way, 0 (ref allele) remains in the beginning.
            
            // Count number of non-reference alleles 
            int numaltall_old = numaltall;
            int num_aux = 0;
            numaltall = 0;
            for(int ia=1;ia<7;ia++)
            {
                num_aux = genallele_nums_f[index.get(ia)];
                if(num_aux >0 ) { numaltall++; }
                //System.out.println("ia, index(ia), num_aux: "+ia + "," + index.get(ia) + "," + num_aux);
            }
            
            // Skip if no alt allele found after genotyping and *not* output_all_sites
            if(numaltall <1) 
            { 
                //filter_count[7]++; 
                if(output_all_sites)
                {
                    numaltall=1;
                }
                else
                {
                    return 7;
                }
            }
            
            // Limit the number of alleles retained after genotyping
            numaltall = Math.min(numaltall, nall_after_genotyping_thr);
            
            // Numbers of samples with various genotypes
            zero2Dmat(gnum_mat);
            //
            // Here we go over all samples,
            // remove any alleles that did not survive genotyping (but reference will always be kept), 
            // and generate sample strings.
            // Limiting the number of retained alleles may change genotypes, so arrays gnum_mat and
            // genallele_nums need to be regenerated....
            Arrays.fill(genallele_nums, 0);
            String smptmp = "";
            String [] gntmp;
            int im1=0;
            int im2=0;
            int num_of_hets = 0;
            int samps_with_ret_all = 0;
            for(int i=0;i<numsamples;i++)
            {
                smptmp = makeSampleStr(genotype_mat[i],num_mat[i],lkhd_mat[i],index,numaltall, numaltall_old);
                if(output_genotypes) { outstrbld.append(smptmp).append("\t"); }
                if(smptmp.equals("./.")) { continue; }
                
                gntmp = smptmp.split(":")[0].split("/");
                im1= Integer.valueOf(gntmp[0]);
                im2 = Integer.valueOf(gntmp[1]);

                samps_with_ret_all++;
                genallele_nums[im1]++;
                genallele_nums[im2]++;
                gnum_mat[im1][im2]++;
                if(im1 != im2) 
                { 
                    num_of_hets++; 
                    gnum_mat[im2][im1]++;
                }
            }
            //
            // end of the re-sorting part
            
            // Find index of the major allele (by allele frequency)
            // (which does not have to be 0, i.e., it is not necessarily reference allele)
            int ind_major_all = 0;
            int num_major_all = genallele_nums[0];
            num_aux = 0;
            for(int ia=1;ia<=numaltall;ia++)
            {
                num_aux = genallele_nums[ia];
                if(num_aux > num_major_all) { ind_major_all = ia; num_major_all = num_aux; }
            }
            // Find index of the first minor allele (by allele frequency)
            // and the overall number of minor alleles
            int num_first_minor_all = 0;
            int ind_first_minor_all = 0;
            int num_minor_all = 0;
            for(int ia=0;ia<=numaltall;ia++)
            {
                if(ia == ind_major_all) { continue; }
                num_aux = genallele_nums[ia];
                if(num_aux > num_first_minor_all) { ind_first_minor_all = ia; num_first_minor_all = num_aux; }
                num_minor_all += num_aux;
            }
          
            // New Allele string
            String [] old_alt_alleles = allele_strIn.split(",");
            String allele_str = "";
            for(int ia=1;ia<=numaltall;ia++)
            {
                allele_str += old_alt_alleles[index.get(ia)-1]+",";
            }
            allele_str = allele_str.substring(0, allele_str.length()-1);
            if(allele_str.isEmpty() || allele_str.equals(null)) { allele_str = "N"; }
            
            // Generate INFO string with alleles re-arranged after genotyping
            String depth_alt_all_str = "";
            String genallele_nums_str = "";
            String index_str = "";
            String allele_qc_str = "";
            String pv_str = "";
            for(int i=0;i<=numaltall;i++)
            {
                int indi = index.get(i);
                // in the following line, we should probably have index.get(i) instead of i....
                //depth_alt_all_str += depth_alt[i] +",";
                depth_alt_all_str += depth_alt[indi] +",";
                genallele_nums_str += genallele_nums[i] + ",";
                index_str += indi + ",";
                allele_qc_str += qc_mat[indi] + ",";
                for(int j=i+1;j<=numaltall;j++)
                {
                    int indj = index.get(j);
                    pv_str += (double)Math.round(100000*PVmat[indi][indj])/100000 + ",";
                }
            }
            String infostr = infostrIn;
            depth_alt_all_str = depth_alt_all_str.substring(0, depth_alt_all_str.length()-1);
            infostr += "AD="+depth_alt_all_str+";";
            genallele_nums_str = genallele_nums_str.substring(0, genallele_nums_str.length()-1);
            infostr += "AN="+genallele_nums_str+";";
            index_str = index_str.substring(0, index_str.length()-1);
            infostr += "IN="+index_str+";";
            allele_qc_str = allele_qc_str.substring(0, allele_qc_str.length()-1);
            infostr += "AQ="+allele_qc_str+";";
            
            String gntp_nums_str = "";
            for(int i=0;i<=numaltall;i++)
            {
                for(int j=i;j<=numaltall;j++)
                {
                    gntp_nums_str += gnum_mat[i][j]+","; 
                }
            }
            gntp_nums_str = gntp_nums_str.substring(0, gntp_nums_str.length()-1);
            infostr += "GN="+gntp_nums_str+";";
            infostr += "SR="+samps_with_ret_all+";";
            infostr += "HT="+num_of_hets+";";
            
            // Compute overall Ed's factor = Het_freq/(present_freq*minor_allale_freq)
            double edfactor = 0;
            if(num_minor_all > 0)
            {
                edfactor = 2*(double)num_of_hets*numsamples/(samps_with_ret_all*num_minor_all);
            }
            //System.out.println("Ed factor: "+pos_actual+" "+edfactor + " " + num_of_hets + " "+ samps_with_0or1_data + " " + num_minor_all);
            // Compute Ed's factor based on alleles 0 and 1 only
            double edfactor01 = 0;
            if(num_minor_all > 0)
            {
                // Debug:
                /*
                if(pos_actual == 40000000)
                {
                    System.out.println("ind_major_all, ind_first_minor_all:"+ind_major_all+","+ind_first_minor_all);
                    System.out.println("gnum_mat:"+gnum_mat[ind_major_all][ind_first_minor_all]);
                }
                // end debug
                */
                edfactor01 = 2*(double)gnum_mat[ind_major_all][ind_first_minor_all]*numsamples;
                edfactor01 = edfactor01/((gnum_mat[ind_major_all][ind_major_all]+gnum_mat[ind_major_all][ind_first_minor_all]+gnum_mat[ind_first_minor_all][ind_first_minor_all])*genallele_nums[ind_first_minor_all]);
            }
            
            // Skip if Ed's factor (from alleles 0 and 1 only) is greater than threshold
            //if(edfactor01 > edfactor_reject_thr) { filter_count[8]++; continue; }
            if(edfactor01 > edfactor_reject_thr) { return 8; }
            infostr += "EF="+(double)Math.round(100*edfactor01)/100;
            if(numaltall >1)
            {
                infostr += ","+(double)Math.round(100*edfactor)/100;
            }
            
            double maf = 0.5*num_minor_all/samps_with_ret_all;
            infostr += ";MAF="+(double)Math.round(1000*maf)/1000;
            
            pv_str = pv_str.substring(0, pv_str.length()-1);
            infostr += ";PV="+pv_str;
            
            // Add HW departure p-value...
            HWresult = HWtest.getChisqPvalue(gnum_mat[ind_major_all][ind_major_all], gnum_mat[ind_major_all][ind_first_minor_all], gnum_mat[ind_first_minor_all][ind_first_minor_all]);
            infostr += ";HW="+(double)Math.round(1000*HWresult[1])/1000;
            infostr += ";CH2="+(double)Math.round(1000*HWresult[0])/1000;
            
            String outstr;
            if (! outstrIn.contains(".")) // This is RBfmt format
            {
                outstr = outstrIn + allele_str + "\t" + infostr+"\t";
            }
            else // This must be VCF format
            {
                allele_str = allele_str.replaceAll("N", ".").replaceAll("D", "<DEL>").replaceAll("I", "<INS>");
                outstr = outstrIn + allele_str + "\t.\t.\t" + infostr+"\tGT:AD:GL\t";
            }
            if(output_genotypes)
            {
                outstrbld.deleteCharAt(outstrbld.length()-1).append("\n");
            }
            else
            {
                outstrbld.append("\n");
            }
            outstrbld.insert(0, outstr);
            
            return 0;
    }
    
    private double genotype_likelihood(int ia1, int ia2, int[] num_mat, int matcount)
    {
        double lkh = 0;
        
        // The parameters below are set in the constructor base on e provided in xml parameter file.
        //double e = 0.05;
        //double p_homo = -0.018483406; // log10(1 - 5*e/6);
        //double p_het = -0.315753252;  //  log10(0.5 -e/3);
        //double p_rest = -2.079181246;  // log10(e/6); 
        
        if(ia1 == ia2)
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1)
                {
                    lkh += num_mat[i]*p_homo;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        else
        {
            for(int i=0;i<matcount;i++)
            {
                if(i==ia1 || i==ia2)
                {
                    lkh += num_mat[i]*p_het;
                }
                else
                {
                    lkh += num_mat[i]*p_rest;
                }
            }
        }
        
        return lkh;
    }
    
    public static void zero2Dmat(int[][] mat)
    {
        for(int i=0;i< mat.length;i++)
        {
            Arrays.fill(mat[i], 0);
        }
    }
    
    public static void zero3Dmat(int[][][] mat)
    {
        for(int i=0;i< mat.length;i++)
        {
            zero2Dmat(mat[i]);
        }
    }
    
    private static int [][] clone2Dmat(int[][] mat)
    {
        int [][] res = new int[mat.length][mat[0].length];
        for(int i=0;i<mat.length;i++)
        {
            res[i] = mat[i].clone();
        }
        return res;
    }
    
    // Construct the genotype/depth/likelihood string for a given sample
    // reshuffling the input arrays according to the result of final re-sorting (index) 
    private static String makeSampleStr(String gnt, int[] depths, int[] lkhds, ArrayList<Integer> index, int numaltall, int numaltall_old)
    {
        String result = "";
        String depth_str = "";
        int depth_int = 0;
        String lkhd_str = "";
        int newi;
        int newj;
        int newloc;
        
        if(gnt.equals("./.")) { return "./."; }
        
        // Allele epths
        for(int i=0;i<= numaltall;i++)
        {
            depth_str += depths[index.get(i)]+",";
            depth_int += depths[index.get(i)];
        }
        if(depth_int == 0) { return "./."; }
        depth_str = depth_str.substring(0, depth_str.length()-1);
        
        // Genotype likelihoods (pthreaded already)
        // In case an existing genotype is ingored (because the number of retained
        // alleles changed after genotyping), we need to regenotype such samples
        // i.e., find the new best genotype and normalize the others to it.
        int [] lkh_new = new int [(numaltall+1)*(numaltall+2)/2];
        int lmin = 9999999;
        int counter = 0;
        int im1=0;
        int im2=0;
        for(int ia1=0;ia1<=numaltall;ia1++)
        {
            for(int ia2=ia1;ia2<=numaltall;ia2++)
            {
                newi = Math.min(index.get(ia1),index.get(ia2))+1;
                newj = Math.max(index.get(ia1),index.get(ia2))+1;
                newloc = (newi -1)*(2*(numaltall_old+1)-newi)/2 + newj -1; // need to use old d
                //lkhd_str += lkhds[newloc] + ",";
                lkh_new[counter] = lkhds[newloc];
                if(lkhds[newloc] < lmin) { lmin = lkhds[newloc]; im1 = ia1; im2 = ia2;}
                counter++;
            }
        }
        
        for(int ia=0;ia<lkh_new.length;ia++)
        {
            lkh_new[ia] -= lmin;
            lkhd_str += lkh_new[ia] + ",";
        }
        lkhd_str = lkhd_str.substring(0, lkhd_str.length()-1);
        result += im1 + "/" + im2 + ":" + depth_str + ":" + lkhd_str; 
        
        return result;
    }
    
    private void print_filter_stats()
    {
       String header = "Thread:" + startRange + "-" + endRange+":";
       System.out.println(header+"Filter Statistics");         
       System.out.println(header+"Reference is N:"+filter_count[0]);
       System.out.println(header+"Zero total coverage:"+filter_count[1]);
       System.out.println(header+"Not enough taxa with coverage:"+filter_count[2]);
       System.out.println(header+"No variation before genotyping:"+filter_count[3]);
       System.out.println(header+"Not enough depth in alternative alleles:"+filter_count[4]);
       System.out.println(header+"Not enough samples with retained alleles:"+filter_count[5]);
       System.out.println(header+"Number of alleles removed b/c of too small allele number AND genotype not well suported by reads in any sample:"+filter_count[6]);
       System.out.println(header+"Number of alleles removed b/c of too large allele p-value of segregation test:"+filter_count[9]);
       System.out.println(header+"No variation after genotyping:"+filter_count[7]);
       System.out.println(header+"Ed's factor too large:"+filter_count[8]);
    }
    
    private static String OutputHeader(String outfmt)
    {
        String VCFfileName = "VCFheader"; // Let's just fix the header name...
        String header = "";
        String line;
        
        if(outfmt.equalsIgnoreCase("RBfmt"))
        {
            header = "#CHR\tPOS\tREF\tALT\tINFO";
        }
        else if(outfmt.equalsIgnoreCase("vcf"))
        {
            try
            {
                BufferedReader headerfile = new BufferedReader(new FileReader(VCFfileName), 1000000);
                while((line=headerfile.readLine()) != null)
                {
                    header += line + "\n";
                }
                headerfile.close();
                header += "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
            }
            catch(Exception e)
            {
                System.out.println("Error reading VCF header file "+VCFfileName+":" + e);
            }
        }
        else
        {
            System.out.println("WARNING: Output format "+outfmt+" not known");
        }
        
        return header;
    }
    
    // Scale all elements of the matrix by a factor; round off to nearest integer
    private void scaleCmat(double [][] cmat, double sfact)
    {
        for(int i=0;i<cmat.length;i++)
            for(int j=0;j<cmat[i].length;j++)
            {
                cmat[i][j] = (int)(sfact*cmat[i][j]);
            }
    }
    
    private void capCmat(double [][] cmat, double cap)
    {
        for(int i=0;i<cmat.length;i++)
            for(int j=0;j<cmat[i].length;j++)
            {
                if(cmat[i][j] > cap) { cmat[i][j] = (int)cap; }
            }
    }
    
}
