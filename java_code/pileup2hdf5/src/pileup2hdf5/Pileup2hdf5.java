/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.util.Arrays;


/**
 *
 * @author rb299
 */
public class Pileup2hdf5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String action = args[0];
        
        /*
        This path is obsolete
        
        if(action.equals("write"))
        {
            String infile = args[1];  // can be "-" for stdin
            String outfile = args[2];
            String samplename = args[3];
            String chrom = args[4];
            int gversion = Integer.parseInt(args[5]);
        
            //h5writer my_writer = new h5writer(null, outfile, samplename, chrom, gversion);
            //my_writer.loadMpileupFile(infile);
            //my_writer.closeFile();
            
            h5CompressedWriter my_writer = new h5CompressedWriter(null, outfile, samplename, chrom, gversion);
            my_writer.loadMpileupFile(infile);
        }
        */
        if(action.equals("pwrite"))
        {
            // Look for a "|" on command liine
            int barind = args.length;
            for(int i=0;i<args.length;i++)
            {
                if(args[i].endsWith("|")) barind = i;
            }
            // Everythng before barind are ptions to java progran
            // Everything after barind should be concatenated into samtools comand "cmd"
            String infile = args[1];  // can be "-" for stdin
            String chrom = args[2];
            Integer baseqcthr = Integer.valueOf(args[3]);
            int gversion = Integer.valueOf(args[4]);
            // The rest of arguments has to have sample name/BAM number data
            String [] sampdata = Arrays.copyOfRange(args, 5, barind);
            System.out.println("Lenght of sampdata array: " + sampdata.length);
            String cmd = null;
            for(int i=barind+1;i<args.length;i++)
            {
                if(cmd==null) cmd="";
                cmd += args[i] + " ";
            }
            System.out.println("Command: "+cmd);
            //cmd ="samtools mpileup -f /data/HapMap3/maize.fa -r 10 /data/HapMap3/c10_bams/B73_FC42G13AAXX_1_c10_AS100.bam ";
            //analyze_pileup anmp = new analyze_pileup(infile, chrom, baseqcthr, sampdata, gversion, null);
            analyze_pileup anmp = new analyze_pileup(infile, chrom, baseqcthr, sampdata, gversion, cmd, null, true);
            anmp.analyze_and_write();
        }
        else if(action.equals("gpwrite"))
        {
            String parfile = args[1];
            String sampfile = args[2]; 
            String chrom = args[3];
            int start = Integer.parseInt(args[4]);
            int end = Integer.parseInt(args[5]);
            int numThreads = Integer.parseInt(args[6]);
            String h5Dir = args[7];
            String posFile = null;
            if(args.length > 8) { posFile=args[8]; }
            runpileup rnplp = new runpileup(parfile, sampfile, chrom, start, end, numThreads, h5Dir, posFile);
        }
        /*
        this path is obsolete
        else if(action.equals("read"))
        {
            String infile = args[1]; 
            String chrom = args[2];
            int start = Integer.parseInt(args[3]);
            int end = Integer.parseInt(args[4]);
            
            h5reader my_reader = new h5reader(infile,chrom,2); // assume version 2
            my_reader.get_range(start, end, true);
            my_reader.close();
        }
        */
        else if(action.equals("genotype"))
        {
            String sampfile = args[1]; 
            String chrom = args[2];
            int start = Integer.parseInt(args[3]);
            int end = Integer.parseInt(args[4]);
            genotyper gntp = new genotyper(sampfile,chrom, start, end); // will assume version 2 of the genome
            gntp.genotype_range(chrom, start, end);
        }
        else if(action.equals("pgenotype"))
        {
            String sampfile = args[1]; 
            String parfile = args[2];
            String chrom = args[3];
            int start = Integer.parseInt(args[4]);
            int end = Integer.parseInt(args[5]);
            int numThreads = Integer.parseInt(args[6]);
            int threadPiece = Integer.parseInt(args[7]);
            String h5Dir = args[8];
            int numIOthreads = Integer.parseInt(args[9]);
            String posFile = null;
            if(args.length > 10) { posFile=args[10]; }
            IOthread iothr = new IOthread(sampfile, parfile, chrom, start, end, numThreads, threadPiece, numIOthreads, h5Dir, posFile);
            System.out.println("IO thread exited");
        }
        else if(action.equalsIgnoreCase("convertfasta"))
        {
            String fastafile = args[1]; 
            String outhdf5 = args[2];
            int gversion = Integer.parseInt(args[3]);
            h5writereference wrt = new h5writereference(fastafile,outhdf5,gversion);
            wrt.covert_fasta();
            wrt.close();
        }
        else if(action.equals("convert2byte"))
        {
            String inhdf5 = args[1]; 
            String outhdf5 = args[2];
            String chrom= args[3];
            String sampname = args[4];
            convert2byte.convert(inhdf5, outhdf5, chrom, sampname);
        }
        else if(action.equals("filtergeno"))
        {
            String infile = args[1]; 
            String outfile = args[2];
            double pvthr = Double.parseDouble(args[3]);
            boolean output_all_sites = Boolean.parseBoolean(args[4]);
            int startpos = Integer.parseInt(args[5]);
            filter flt = new filter(infile,outfile,pvthr,output_all_sites,startpos);
            flt.filterOnPv();
        }
        else if(action.equals("mergeh5"))
        {
            String infile1 = args[1]; 
            String infile2 = args[2];
            String outfile= args[3];
            mergeh5.merge_compr_h5(infile1, infile2, outfile);
        }
        else if(action.equals("testanpileup"))
        {
            //String [] ttt = analyze_pileup.analyze_pileup_strs("SSSS", "SSSS", "SSSS", 0);
            //runpileup.executeSamtools("samtools mpileup -f /data/HapMap3/maize.fa -r 10 /data/HapMap3/c10_bams/B73_FC42G13AAXX_1_c10_AS100.bam ");
            runpileup.executeSamtools("/data/HapMap3/c10_bams/B73");
        }
        else
        {
            System.out.println("Unknown action");
        }
    }
}
