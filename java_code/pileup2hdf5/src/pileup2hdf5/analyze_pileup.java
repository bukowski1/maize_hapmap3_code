/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pileup2hdf5;

import java.util.regex.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author bukowski
 */
public class analyze_pileup implements Runnable {
    
    private static String [] alleles = {"A", "C", "G", "T", "I", "D", "N"};
    private String infileName;
    private int nSamples;
    private String chrom;
    private int chrlen;
    private String [] sampNames;
    private int [] sampBamNumbers;
    private int baseQcthr;
    private boolean saveQc;
    private int gversion;
    private String cmd=null;
    
    //public PipedInputStream mainpistream;
    //private PipedOutputStream mainpostream;
    
    private ArrayList<PipedOutputStream> postreams = new ArrayList<PipedOutputStream>();
    public ArrayList<PipedInputStream> pistreams = new ArrayList<PipedInputStream>();
    
    private HashMap pos2genoH = null;
    private boolean writeh5 = true;
    
    private int startchrpos;
    private int endchrpos;
    
    public analyze_pileup(String infilename, String Chrom, Integer baseqcthr, String [] sampdata, int gversionIn, String cmdIn,
            ArrayList<Integer> pos2genoIn, boolean writeh5In)
    {
        this(infilename, Chrom, baseqcthr, sampdata, gversionIn, cmdIn,
            pos2genoIn, writeh5In, 0, 0);
    }
    
    public analyze_pileup(String infilename, String Chrom, Integer baseqcthr, String [] sampdata, int gversionIn, String cmdIn,
            ArrayList<Integer> pos2genoIn, boolean writeh5In, int startchrposIn, int endchrposIn)
    {
        infileName = infilename;
        
        // sampdata contains (sampl_name, bam_number) pairs
        // to be translated into sampBames, sampBamNumbers, and nSamples
        
        nSamples = sampdata.length/2;
        System.out.println("Found "+nSamples+" samples");
        sampNames = new String [nSamples];
        sampBamNumbers = new int [nSamples];
        System.out.println("Sample names and BAM numbers:");
        for(int i=0;i<nSamples;i++)
        {
            sampNames[i] = sampdata[2*i];
            sampBamNumbers[i] = Integer.valueOf(sampdata[2*i+1]);
            System.out.println(sampNames[i]+":"+sampBamNumbers[i]);
        }
        
        chrom = Chrom;
        // chrlen is only needed for the path with h5 depth file being written.
        // Preferably, it shhould be known from the fai file. However, since
        // the h5 writing patrhy is unlikely to be used for systems otyher than already
        // hardcoded in h5writereference (maize v2, v3, and cassava), we will
        // take a shortcut here and use chromosome_lengths hash....
        gversion = gversionIn;
        // OK, we will use a *very* dirty trick here: pass chromosome length as gversion.
        // This allows to avoid extensive changes to the program
        // This assums that the calling program sets gversion equal to chromsome length
        if(gversion <=5)
        {
            chrlen = h5writereference.chromosome_lengths(gversion).get(chrom);
        }
        else
        {
            chrlen = gversion;
        }
        baseQcthr = baseqcthr;
        
        saveQc = true;
        
        cmd = cmdIn;
        if(pos2genoIn != null)
        {
            pos2genoH = new HashMap();
            for(int i=0;i<pos2genoIn.size();i++)
            {
                pos2genoH.put(String.valueOf(pos2genoIn.get(i)), true);
            }
        }
        
         writeh5 = writeh5In;
         startchrpos = startchrposIn;
         endchrpos = endchrposIn;
        
         /*
        try
        {
            mainpostream = new PipedOutputStream();
            mainpistream = new PipedInputStream(mainpostream);
        }
        catch(Exception e)
        {
            System.out.println("Error connecting pipes: "+e);
        }
        */
        
        // Connect pipes for all samples (in practice, there will only be one sample in most cases)
        try
        {
            for(int i=0;i< nSamples;i++)
            {
                postreams.add(new PipedOutputStream());
                pistreams.add(new PipedInputStream(postreams.get(i)));
            }
        }
        catch(Exception e)
        {
            System.out.println("Error connecting pipes: "+e);
        }
    }
    
    @Override
    public void run()
    {
        analyze_and_write();
    }
    
    public void analyze_and_write()
    {
        InputStreamReader isreader = null;
        BufferedReader isr = null;
        PipedOutputStream pos = null;
        PipedInputStream pis = null;
        BufferedWriter bwrt = null;
        String samplename = null;
        String position="";
        String refbase;
        String pstr;
        String qstr;
        String len;
        String [] allnumbers;
        boolean debug = false;
        Process p = null;
        try
        {
            // open the appropriate stream to read from
            if(cmd != null)
            {
                p = Runtime.getRuntime().exec(cmd);
                InputStream pipedOut = p.getInputStream();
                isr = new BufferedReader(new InputStreamReader(pipedOut), 1000000);
            }
            else if(! infileName.equals("-"))
            {
                // This is reading from stdin
                isr = new BufferedReader(new FileReader(infileName), 1000000);
            }
            else
            {
                isreader = new InputStreamReader(System.in);
                isr = new BufferedReader(isreader);
            }   
            
            
            // Launch h5writer thread for each sample with its own pipe connection
            //ArrayList<PipedOutputStream> postreams = new ArrayList<PipedOutputStream>();
            //ArrayList<PipedInputStream> pistreams = new ArrayList<PipedInputStream>();
            ArrayList<BufferedWriter> bwriters = new ArrayList<BufferedWriter>();
            
            // No separate quality writers any more
            //ArrayList<PipedOutputStream> qpostreams = new ArrayList<PipedOutputStream>();
            //ArrayList<PipedInputStream> qpistreams = new ArrayList<PipedInputStream>();
            //ArrayList<BufferedWriter> qbwriters = new ArrayList<BufferedWriter>();
            
            for(int i=0;i<nSamples;i++)
            {
                // Initialize H5 writer for alelle depths
                
                //pos = new PipedOutputStream();
                //postreams.add(pos);
                //pis = new PipedInputStream(postreams.get(i));
                //pistreams.add(pis);
                //bwrt = new BufferedWriter(new OutputStreamWriter(pos));
                //bwrt = new BufferedWriter(new OutputStreamWriter(mainpostream)); // just for test
                
                bwrt = new BufferedWriter(new OutputStreamWriter(postreams.get(i)));
                bwriters.add(bwrt);
                samplename = sampNames[i];
                // If requested, start the writer threads - otherwise the output written to bwrt will be intercepted elsewhere...
                if(writeh5)
                {
                    h5CompressedWriter h5wrt = new h5CompressedWriter(pistreams.get(i), samplename + ".h5", samplename, chrom, chrlen, startchrpos, endchrpos, gversion);
                    Thread thr = new Thread(h5wrt);
                    thr.start(); // This will now wait for input
                }
            }
            
            // Read the mpileup input line by line, extract numbers, and send to 
            // respective h5writers. There may be multiple BAM files per sample
            
            // The best way to select a subset of positions (such as GBS sites) is to
            // run samtools mpileup on the whole range (startRane-endRange) and then skip
            // processing pileup string for positions not in the key set of pos2genoH hash.
            // Counterintuitively, samtool mpileup with "-l" option (specifying subset of sites)
            // is slower...
            
            String lineIn = isr.readLine();
            while(lineIn !=null)
            {
                String [] aux = lineIn.split("\t");
                position = aux[1];
                
                if(pos2genoH != null)
                {
                    // ???? This seems to be veeeery sloooow id pos2geno is a List
                    // Conversion to HashMap solves this problem...
                    if( ! pos2genoH.containsKey(position)) 
                    {
                        lineIn = isr.readLine();
                        continue;
                    }
                }
                
                refbase = aux[2];
                
                //System.out.println("Read line from pileup: "+lineIn);
                int samp_offset = 3; // where the data starts for current sample
                for(int i=0;i<nSamples;i++)
                {
                    pstr = "";
                    qstr = "";
                    for(int j=0;j<sampBamNumbers[i];j++)
                    {
                        len = aux[3*j + samp_offset];
                        //System.out.println("Length for sample "+i+ ", BAM "+j+" : "+len);
                        if(! len.equals("0"))
                        {
                            pstr = pstr.concat(aux[3*j + samp_offset + 1]);
                            qstr = qstr.concat(aux[3*j + samp_offset + 2]);
                        }
                    }
                 
                    if(! pstr.equals(""))
                    {
                        allnumbers = analyze_pileup_strs(pstr, qstr, refbase, baseQcthr);
                        bwriters.get(i).write(position+"\t"+allnumbers[0]+"\t"+allnumbers[1]+"\n");   
                    }
                    
                    samp_offset += 3*sampBamNumbers[i];
                }
                lineIn = isr.readLine();
            }
            isr.close();
            
            if(p != null) { p.waitFor(); }
            
            // Close all the writers to release the h5 writer threads, if any
            for(int i=0;i<nSamples;i++)
            {
                bwriters.get(i).close();
            }
        }
        catch(Exception e)
        {
            System.out.println("ERROR in analyze_pileup at pos "+position+": "+e);
        }
    }
    
    public static String [] analyze_pileup_strs(String pstr, String qstr, String refbase, Integer baseQcthr)
    {
        //int [] result = new int[12];
        String base;
        //System.out.println("function eneterd "+pstr+" "+qstr+" "+refbase);
        //String pstr0 = ".$..^F....+2AG.-2AG.+3AGGG";
        //refbase ="C";
        
        // remove read start/end markers
        // remove $ character
        pstr = pstr.replace("$", "");
        pstr = pstr.replaceAll("\\^[\\x00-\\x7F]", "");

        // Process insertions
        Pattern ptrn = Pattern.compile("(\\+[0-9]+[ACGTNacgtn=]+)"); // "=" added to acommodate HapMap2 bams
        Pattern ptrn1 = Pattern.compile("\\+([0-9]+)");
        Matcher mchr = ptrn.matcher(pstr);
        Matcher mchr1;
        while(mchr.find())
        {
            mchr1 = ptrn1.matcher(mchr.group());
            mchr1.find();
            int len = Integer.valueOf(mchr1.group());
            int numeral_len = mchr1.group().length();
            //System.out.println("len: "+len+", numeral_len: "+numeral_len+", other"+mchr.group().length());
            //String torepl = mchr1.group() + mchr.group().substring(2, 2+len);
            String torepl = mchr1.group() + mchr.group().substring(numeral_len, numeral_len+len);
            //System.out.println("torepl: "+torepl);
            pstr = pstr.replaceFirst(".\\"+torepl, "I");
            //System.out.println(pstr);
            pstr = pstr.replaceFirst(",\\"+torepl, "I"); // Not needed, because "." in regexp stands for anything already
            //System.out.println(pstr);
        }
        //System.out.println("After while 1");
        // Process deletions (just remove)
        // the actual deleted bases will show up as "*" in subsequent positions
        ptrn = Pattern.compile("(\\-[0-9]+[ACGTNacgtn]+)");
        ptrn1 = Pattern.compile("\\-([0-9]+)");
        mchr = ptrn.matcher(pstr);
        while(mchr.find())
        {
            mchr1 = ptrn1.matcher(mchr.group());
            mchr1.find();
            int len = Integer.valueOf(mchr1.group());
            int numeral_len = mchr1.group().length();
            //System.out.println("len: "+len);
            //String torepl = mchr1.group() + mchr.group().substring(2, 2-len);
            String torepl = mchr1.group() + mchr.group().substring(numeral_len, numeral_len-len);
            //System.out.println("torepl: "+torepl);
            pstr = pstr.replaceAll("\\"+torepl, "");
        }
        //System.out.println("After while 2");
        pstr = pstr.replace("*", "D");
        // We keep the (upper-cased) N bases and collect quality stats on them
        // to avoid removing N positions from quality string (in the future)
        pstr = pstr.replace("n", "N");
        
        // Count the numbers of bases
        Hashtable<String,Integer> basecount = new Hashtable<String,Integer>();
        Hashtable<String,Integer> qccount = new Hashtable<String,Integer>();
        for(int ia=0;ia<7;ia++)
        {
           basecount.put(alleles[ia], 0);
           qccount.put(alleles[ia], 0);
        }
        //System.out.println("After for 1");
        char [] apstr = pstr.toCharArray();
        char [] aqstr = qstr.toCharArray();
        int ichar = 0;
        for(int i=0;i<apstr.length;i++)
        {
            ichar = (int)aqstr[i] - 33;
            if(ichar >= baseQcthr)
            {
                if(apstr[i] == '.' || apstr[i] == ',')
                {
                    base = refbase.toUpperCase();
                }
                else
                {
                    base = String.valueOf(apstr[i]).toUpperCase();
                }
                basecount.put(base, basecount.get(base)+1);
                qccount.put(base, qccount.get(base)+ichar);
            }
        }
        //System.out.println("After for 2");
        // Calculate QC averages
        for(int ia=0;ia<7;ia++)
        {
           int avqc = 0;
           if(basecount.get(alleles[ia]) > 0)
           {
               avqc = qccount.get(alleles[ia])/basecount.get(alleles[ia]);
               qccount.put(alleles[ia], avqc);
           }
        }
        //System.out.println("After for 3");
        //int [] allele_nums = {basecount.get("A"), basecount.get("C"), basecount.get("G"),basecount.get("T"),basecount.get("I"),basecount.get("D") };

        //System.out.println(pstr);
        String [] allnums = new String[2]; 
        allnums[0] = basecount.get("A") + "\t" + basecount.get("C") + "\t" +  basecount.get("G") + "\t" + basecount.get("T") + "\t" + basecount.get("I") + "\t" + basecount.get("D");
        allnums[1] = qccount.get("A") + "\t" + qccount.get("C") + "\t" +  qccount.get("G") + "\t" + qccount.get("T") + "\t" + qccount.get("I") + "\t" + qccount.get("D");
        //System.out.println("After allnum assignment"+allnums[0]+" "+allnums[1]);    
        return allnums;
    }
    
}
