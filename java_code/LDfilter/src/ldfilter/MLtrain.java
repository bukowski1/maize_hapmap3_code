/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.Instance;
import net.sf.javaml.tools.data.FileHandler;
import net.sf.javaml.tools.Serial;
import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.tree.RandomForest;

import java.io.File;

/**
 *
 * @author bukowski
 */
public class MLtrain {
    
    private String trainFile;
    
    public MLtrain(String trainFileIn)
    {
        trainFile = trainFileIn;
    }
    
    public void trainModel()
    {
        Dataset data = null;
        try
        {
            // This is just for the test file loading into dataset
            data = FileHandler.loadDataset(new File(trainFile), 3, ",");
            System.out.println("Data file read in - dataset created");
            Object [] my_classes = data.classes().toArray();
            System.out.println("Classes detected:");
            for(Object str : my_classes)
            {
                System.out.println((String)str);
            }
        }
        catch(Exception e)
        {
            System.out.println("Error loading training file: "+e);
        }
        
        // Make a copy of input dataset before it is used in classifier constructor
        Dataset data_saved = data.copy();
        System.out.println("Copy of dataset saved");
        
        Classifier rf = new RandomForest(5);
        System.out.println("Classifier initialized");
        rf.buildClassifier(data);
        System.out.println("Model trained");
        
        // Serialize and save the trained model
        //Serial.store(rf, trainFile + ".xml");
        //RandomForest rf1 = (RandomForest)Serial.load(trainFile + ".xml");
        
        // Model now trained - check first on the input dat
        int right = 0, wrong = 0;
        for(Instance inst : data_saved)
        {
            Object class_pred = rf.classify(inst);
            if(class_pred.equals(inst.classValue()))
            {
                right++;
            }
            else
            {
                wrong++;
            }
        }
        
        System.out.println("Right: " + right + " wrong: " + wrong);
                
    }
    
}
