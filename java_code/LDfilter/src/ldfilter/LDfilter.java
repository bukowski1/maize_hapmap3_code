/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

/**
 *
 * @author bukowski
 */
public class LDfilter {

    /**
     * @param args the command line arguments
     * Launch with first argument makeld (mltrain path is not functional and should be removed)
     */
    public static void main(String[] args) {
        
        String action = args[0];
        if(action.equals("makeld"))
        {
            // Name of the genotype file
            String hmpname = args[1];
            // Name of the xml file with run options
            String paramfile = args[2];
            // Number of threads to use
            String numthreads_str = args[3];
            boolean doglobal = Boolean.valueOf(args[4]);
            int numThreads = Integer.parseInt(numthreads_str);
            pLDannotator plda = new pLDannotator(hmpname, paramfile, numThreads, doglobal);
        }
        else if(action.equals("mltrain"))
        {
            String trainFile = args[1];
            MLtrain mlt = new MLtrain(trainFile);
            mlt.trainModel();
        }
        else if(action.equals("IBDscan"))
        {
            String genoFile = args[1]; 
            int numThreads = Integer.parseInt(args[2]);
            int minComp = Integer.parseInt(args[3]);
            double maxIBDDist = Double.parseDouble(args[4]);
            int windowSize = Integer.parseInt(args[5]);
            
            makeIBD mm = new makeIBD();
            mm.scanForIBD(genoFile, numThreads, minComp, maxIBDDist, windowSize);
            
        }
        else
        {
            System.out.println("Action " + action + " not implemented");
        }
        
    }
}
