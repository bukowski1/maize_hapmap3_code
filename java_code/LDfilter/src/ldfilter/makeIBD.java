/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import net.maizegenetics.util.Utils;
import net.maizegenetics.util.ExceptionUtils;
import net.maizegenetics.pal.alignment.Alignment;
import net.maizegenetics.pal.alignment.BitAlignment;
import net.maizegenetics.pal.alignment.FilterAlignment;
import net.maizegenetics.pal.alignment.Locus;
import net.maizegenetics.pal.alignment.ImportUtils;
import net.maizegenetics.pal.distance.*;
import net.maizegenetics.util.OpenBitSet;
import net.maizegenetics.prefs.TasselPrefs;
import net.maizegenetics.pal.ids.IdGroup;
import net.maizegenetics.pal.ids.SimpleIdGroup;
import net.maizegenetics.util.ProgressListener;

/**
 *
 * @author bukowski
 */
public class makeIBD implements ProgressListener {
    
    public makeIBD()
    {
        
    }
    
    public void scanForIBD(String genoFile, int numThreads, int minComp, double maxIBDDist, int windowSize)
    {
        double distij=0;
        int physstart, physend;
        
        Alignment hm3 = pLDannotator.readFromRBfmt(genoFile, true, numThreads, false, this);
        
        for(int start=0;start+windowSize<hm3.getSiteCount();start+=windowSize)
        {
            Alignment a = FilterAlignment.getInstance(hm3, start, start+windowSize);
            DistanceMatrix dm = new IBSDistanceMatrix(a,minComp,null);
            int countIBD=0, taxaIBD=0, count=0;
            double mean = 0;
            System.out.println("Row count: "+dm.getRowCount()+" Column count: "+dm.getColumnCount());
            for(int i=0;i<dm.getRowCount();i++)
            {
                //System.out.println("Doing taxon "+i+" for window starting at "+start);
                int ibd = 0;
                for(int j=0;j<dm.getColumnCount()-1;j++)
                {
                    if(i == j) continue;
                    distij = dm.getDistance(i, j);
                    if(Double.isNaN(distij)) continue;
                  
                    mean += distij;
                    count++;
                    
                    if(distij < maxIBDDist) 
                    {
                        ibd++;
                        physstart = a.getPositionInLocus(start);
                        physend = a.getPositionInLocus(start+windowSize);
                        System.out.println(physstart + "\t" + physend + "\t" + i + "\t" + j);
                    }
                }
                
                if(ibd >0) taxaIBD++;
                countIBD += ibd;
            }
            mean = mean/count;
            
            System.out.printf("Window:%d StartSite:%d Taxa:%d maxIBDDist:%g IBDTaxa:%d IBDContrasts:%d MeanDist:%g%n",
                        windowSize, start,hm3.getSequenceCount(),maxIBDDist,taxaIBD,countIBD,mean);
        }
    }
    
    @Override
    public void progress(int i, Object obj)
    {
        System.out.printf("%d%% of file read in %.3f minutes %n",i,(Double)obj);
    }
    
}
