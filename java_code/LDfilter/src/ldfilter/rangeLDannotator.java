/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import net.maizegenetics.pal.alignment.Alignment;
import net.maizegenetics.pal.statistics.FisherExact;
import net.maizegenetics.util.BitSet;
import net.maizegenetics.util.OpenBitSet;
import net.maizegenetics.gbs.maps.SiteMappingInfo;
import net.maizegenetics.pal.popgen.LinkageDisequilibrium;
import net.maizegenetics.pal.alignment.Locus;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Compute LD for a range of sites. Write output to a file
 * called LDthr.start-end
 *
 * @author bukowski
 */
public class rangeLDannotator implements Runnable {
    
    private Alignment a;
    private Alignment aa;
    private int startRange;
    private int endRange;
    private int minPhysDist;
    private int maxPhysDist;
    private int minMinorCnt; 
    private float minR2;
    private float maxPvalue;
    private BufferedWriter bw;
    private boolean doglobal;
    
    /**
     * 
     * @param aIn: alignment object with genotyping results
     * @param aaIn: alignment object with anchor map - can be pointing to the actual alignment
     * @param startRangeIn: index (zero-based) of the first site to process
     * @param endRangeIn: index (zero-based) of the last site to process
     * @param minPhysDistIn: radius of the inner window
     * @param maxPhysDistIn: radius of the outer window
     * @param minMinorCntIn: number of taxa with alternative allele must be more than this
     *                          in each of the two sites for this site pair of sites to be considered
     * @param minR2In: minimum R2 threshold for skipping Fisher Exact Test
     * @param maxPvalueIn : p-value threshold
     */
    public rangeLDannotator(Alignment aIn, Alignment aaIn, int startRangeIn, int endRangeIn, int minPhysDistIn, int maxPhysDistIn, int minMinorCntIn, 
        float minR2In, float maxPvalueIn, boolean doglobalIn)
    {
        startRange = startRangeIn;
        endRange = endRangeIn;
        minPhysDist = minPhysDistIn;
        maxPhysDist = maxPhysDistIn;
        minMinorCnt = minMinorCntIn;
        minR2 = minR2In;
        maxPvalue = maxPvalueIn;
        a = aIn;
        a.optimizeForSites(null);
        aa = aaIn;
        doglobal = doglobalIn;
    }
    
    @Override
    public void run()
    {
        long startTime;
        long endTime;
        // Open output file for thread
        try
        {
            bw = new BufferedWriter(new FileWriter("LDthr."+startRange+"-"+endRange));
        }
        catch(Exception e)
        {
            System.out.println("output file could not be open"+e);
        }
        System.out.println("LDThread " + startRange + "-" + endRange + " starting");
        startTime = System.currentTimeMillis();
        if(doglobal)
        {
            annotateLD_range_global();
        }
        else
        {
            annotateLD_range();
        }
        endTime = System.currentTimeMillis();
        double duration = 0.001*(endTime - startTime)/60;
        System.out.printf("LDThread %d-%d took %.3f minutes %n", startRange, endRange, duration);
        try
        {
            bw.close();
        }
        catch(Exception e)
        {
            System.out.println("File could not be closed" + e);
        }
        
    }
    
    /**
     * Methods to evaluate the LD of a site.
     * Taken from AnnotateGBSHDF5Plugin.java and adapted for tests by RB
     * Currently it evaluates everything by r2 and p-value, collecting best
     * results within three windows: below minPhysDist, between minPhysDist and
     * maxPhysDist/2, and between maxPhysDist/2 and maxPhysDist.
     */
    private void annotateLD_range() 
    {
        FisherExact myFisherExact=new FisherExact(a.getSequenceCount() + 10);
        // a.optimizeForSites(null); - done in constructor
        int minCnt=40;
        int sites=aa.getSiteCount();
        int mysites = endRange - startRange + 1;
        long startTime;
        long endTime;
        double duration;
        int [] auxind = new int[2];
        boolean sortOnR2 = false; // What to sort sites on - in the future, make it an input parameter
        double logPvalue;
        
        OpenBitSet opBS = new OpenBitSet(a.getSequenceCount());
        
        startTime = System.currentTimeMillis();
        
        // First dimension is the distance interval - we will have three
        float[][] maxR2=new float[3][mysites];  
        float[][] minPOfMaxR2=new float[3][mysites];  
        int[][] minSigDistance=new int[3][mysites]; 
        int[][] nLDsites = new int[3][mysites]; 
        int[][] distToBest = new int[3][mysites];
        for(int ivl=0;ivl<3;ivl++)
        {
            Arrays.fill(maxR2[ivl], 0.0F);
            //Arrays.fill(minPOfMaxR2[ivl], Float.NaN);
            // p-value =-1 will mean that LD could not be computed for the site
            // because of too low MAF. If any suitable site is found, this will
            // be changed to 1 or less...
            Arrays.fill(minPOfMaxR2[ivl], -1.0F);
            Arrays.fill(minSigDistance[ivl], Integer.MAX_VALUE);
            Arrays.fill(nLDsites[ivl], 0);
            Arrays.fill(distToBest[ivl], 0);
        }
        
        for (int i = startRange; i <= endRange; i++) {
            int myi = i - startRange;
            int[][] contig = new int[2][2];
            
            //System.out.println("DEBUG: position of index "+i);
            
            // Locus myLocus=a.getLocus(i);
            BitSet rMj = a.getAllelePresenceForAllTaxa(i, 0);
            BitSet rMn = a.getAllelePresenceForAllTaxa(i, 1); 
            if(rMn.cardinality()<minMinorCnt) {/*System.out.println("Not enough minor in site:"+rMn.cardinality());*/continue;}
            //TreeMap<Double,SiteMappingInfo> bestLDSites=new TreeMap<Double,SiteMappingInfo>(Collections.reverseOrder());
            List<TreeMap<Double,SiteMappingInfo>> bestLDSites = new ArrayList<TreeMap<Double,SiteMappingInfo>>(3);
            for(int interval=0;interval<3;interval++)
            {
                bestLDSites.add(interval,new TreeMap<Double,SiteMappingInfo>(Collections.reverseOrder()));
            }
            double [] minLD=new double[3]; Arrays.fill(minLD, 0.0);
            int attemptTests=0, completedTests=0, sigTests=0;
            int position=a.getPositionInLocus(i), dist=-1;
            int leftSite, rightSite, j;
            if(a.equals(aa))
            {
                leftSite=i; rightSite=i; j=-1;
            }
            else
            {
                auxind = findFlankingIndexes(a,aa,i);
                leftSite = auxind[0] + 1;
                rightSite = auxind[1] - 1;
                //System.out.println("DEBUG0: position "+position + " left,rightSite: "+ leftSite + " " + rightSite);
            }
            
            //System.out.println("DEBUG: position "+position + " left,rightSite: "+ leftSite + " " + rightSite);
            
            while(((leftSite>=0)||(rightSite+1<sites))&&(dist<maxPhysDist)) {
                int rightDistance=(rightSite+1<sites)?aa.getPositionInLocus(rightSite+1)-position:Integer.MAX_VALUE;
                int leftDistance=(leftSite>0)?position-aa.getPositionInLocus(leftSite-1):Integer.MAX_VALUE;
                if(rightDistance<leftDistance) {rightSite++; j=rightSite; dist=rightDistance;}
                else {leftSite--; j=leftSite; dist=leftDistance;}
                // if(dist<minPhysDist) continue;
                if(dist > maxPhysDist) continue;
                attemptTests++;
               // System.out.printf("Dist: %d %d bin: %d %n",dist, Integer.highestOneBit(dist), bin);
                //System.out.println("DEBUG: testing j "+ j + " dist: "+ dist);
                BitSet cMj = aa.getAllelePresenceForAllTaxa(j, 0);  //major alleles
                BitSet cMn = aa.getAllelePresenceForAllTaxa(j, 1);  //minor alleles
                int n = 0;
                n += contig[1][1] = (int) OpenBitSet.intersectionCount(rMn, cMn);
                n += contig[1][0] = (int) OpenBitSet.intersectionCount(rMn, cMj);
                if(contig[1][0]+contig[1][1]<minMinorCnt) {/*System.out.println("Not enough minor in raw:"+n);*/continue;}
                n += contig[0][1] = (int) OpenBitSet.intersectionCount(rMj, cMn);
                int naux = contig[0][1]+contig[1][1];
                if(contig[0][1]+contig[1][1]<minMinorCnt) {/*System.out.println("Not enough minor in col:"+naux);*/continue;}
                n += contig[0][0] = (int) OpenBitSet.intersectionCount(rMj, cMj);
                if(n<minCnt) {/*System.out.println("Not enough n:"+n);*/continue;}
                double rValue = LinkageDisequilibrium.calculateRSqr(contig[0][0], contig[1][0], contig[0][1], contig[1][1], minCnt);
                if (Double.isNaN(rValue)) {/*System.out.println("R2 is NaN:");*/continue;}
                completedTests++;
                // Determine the distance interval to record the values in
                int interval;
                if(dist < minPhysDist)
                {
                    interval = 0;
                }
                else if(dist >= minPhysDist && dist < maxPhysDist/2)
                {
                    interval = 1;
                }
                else   // dist >=maxPhysDist/2 && dist < maxPhysDist
                {
                    interval = 2;
                }
                // So, we found a site to compute LD with - change min p-value from -1 to 1.
                // Sites with p-value=-1 will be deemed unclassifiable, sites with pvalue=1
                // will mean bad LD (if not improved on later).
                minPOfMaxR2[interval][myi] = 1.0F;
                if(rValue<minR2) {/*System.out.println("rValue too small:"+rValue);*/continue;}
                double pValue=myFisherExact.getTwoTailedP(contig[0][0], contig[1][0], contig[0][1], contig[1][1]);
                if(pValue>maxPvalue) {/*System.out.println("pValue too large:"+pValue);*/continue;}
                
                sigTests++;
                      
                if(dist<minSigDistance[interval][myi]) minSigDistance[interval][myi]=dist;
                
                if(sortOnR2)
                {
                    if(rValue>minLD[interval]) {
                        //float[] result={j, (float)rValue, (float)pValue, dist};
                        SiteMappingInfo smi=new SiteMappingInfo(Integer.parseInt(aa.getLocusName(j)),
                            (byte)1,aa.getPositionInLocus(j),(float)rValue, (float)pValue,j);
                        bestLDSites.get(interval).put(rValue, smi);
                    }
                }
                else
                {
                    logPvalue = -Math.log10(pValue);
                    if(logPvalue>minLD[interval]) {
                        SiteMappingInfo smi=new SiteMappingInfo(Integer.parseInt(aa.getLocusName(j)),
                            (byte)1,aa.getPositionInLocus(j),(float)rValue, (float)pValue,j);
                        bestLDSites.get(interval).put(logPvalue, smi);
                    }
                }
                
                if(bestLDSites.get(interval).size()>20) {
                    bestLDSites.get(interval).remove(bestLDSites.get(interval).lastKey());
                    minLD[interval]=bestLDSites.get(interval).lastKey();
                }
            }
            
            for(int interval=0;interval<3;interval++)
            {
                if(bestLDSites.get(interval).size()>0) {
                    SiteMappingInfo smi=(SiteMappingInfo)bestLDSites.get(interval).firstEntry().getValue();
                    maxR2[interval][myi]=smi.r2;
                    minPOfMaxR2[interval][myi]=smi.mapP;
                    nLDsites[interval][myi]=bestLDSites.get(interval).size();
                    distToBest[interval][myi] = smi.position - position;
                }
            }
            
            // Report incremental timing
            if((myi+1)%10000 == 0)
            {
                endTime = System.currentTimeMillis();
                duration = 0.001*(endTime - startTime)/60;
                System.out.printf("LDthread %d-%d progress: %d%% in %.3f minutes %n", startRange, endRange, 100*(myi+1)/mysites, duration);
            }
        }
        
        // write out the results (file open and closed in method "run"
        // Actually, wrinitng could be done inside the i loop, saving some array memory
        try
        {
            if(startRange == 0)
            {
                bw.write("pos\tmaxR2_0\tmaxR2_1\tmaxR2_2\tpv_0\tpv_1\tpv_2\tmindist_0\tmindist_1\tmindist_2\t");
                bw.write("nsites_0\tnsites_1\tnsites_3\tDtobest_0\tDtobest_1\tDtobest_2\n");
            }
            for(int i=0;i<mysites;i++)
            {
                bw.write(String.valueOf(a.getPositionInLocus(i+startRange)));
                bw.write("\t" + maxR2[0][i] + "\t" + maxR2[1][i] + "\t" + maxR2[2][i]);
                bw.write("\t" + minPOfMaxR2[0][i] + "\t" + minPOfMaxR2[1][i] + "\t" + minPOfMaxR2[2][i]);
                bw.write("\t" + minSigDistance[0][i] + "\t" + minSigDistance[1][i] + "\t" +minSigDistance[2][i]);
                bw.write("\t" + nLDsites[0][i] + "\t" + nLDsites[1][i] + "\t" + nLDsites[2][i]);
                bw.write("\t" + distToBest[0][i] + "\t" + distToBest[1][i] + "\t" + distToBest[2][i] + "\n");
                
            }   
        }
        catch(Exception e)
        {
            System.out.println("output could not be written"+e);
        }
        
        /*
        IHDF5WriterConfigurator config = HDF5Factory.configure(hdf5File);
        myLogger.info("Annotating HDF5 file with LD: " + hdf5File);
//        config.overwrite();
//        config.dontUseExtendableDataTypes();
        IHDF5Writer h5w = config.writer();
        if(!h5w.exists(HapMapHDF5Constants.LD_DESC)) h5w.createGroup(HapMapHDF5Constants.LD_DESC);
        if(!h5w.exists(HapMapHDF5Constants.LDR2_DESC)) h5w.createFloatArray(HapMapHDF5Constants.LDR2_DESC, maxR2.length);
        h5w.writeFloatArray(HapMapHDF5Constants.LDR2_DESC, maxR2);
        if(!h5w.exists(HapMapHDF5Constants.LDP_DESC)) h5w.createFloatArray(HapMapHDF5Constants.LDP_DESC, minPOfMaxR2.length);
        h5w.writeFloatArray(HapMapHDF5Constants.LDP_DESC, minPOfMaxR2);
        if(!h5w.exists(HapMapHDF5Constants.LDPropLD_DESC)) h5w.createFloatArray(HapMapHDF5Constants.LDPropLD_DESC, propSigTests.length);
        h5w.writeFloatArray(HapMapHDF5Constants.LDPropLD_DESC, propSigTests);
        if(!h5w.exists(HapMapHDF5Constants.LDMinDist_DESC)) h5w.createIntArray(HapMapHDF5Constants.LDMinDist_DESC, minSigDistance.length);
        h5w.writeIntArray(HapMapHDF5Constants.LDMinDist_DESC, minSigDistance);
        */
    }
    
    private void annotateLD_range_global() 
    {
        FisherExact myFisherExact=new FisherExact(a.getSequenceCount() + 10);
        // a.optimizeForSites(null); - done in constructor
        int minCnt=40;
        int sites=aa.getSiteCount();
        int mysites = endRange - startRange + 1;
        long startTime;
        long endTime;
        double duration;
        int [] auxind = new int[2];
        boolean sortOnR2 = false; // What to sort sites on - in the future, make it an input parameter
        double logPvalue;
        int interval;
        int dist;
        
        int nintervals = 1; // originally there was 3 - probably do not need it here, at least for now
        
        startTime = System.currentTimeMillis();
        
        // First dimension is the distance interval - we will have three
        float[][] maxR2=new float[nintervals][mysites];  
        float[][] minPOfMaxR2=new float[nintervals][mysites];  
        int[][] minSigDistance=new int[nintervals][mysites]; 
        int[][] nLDsites = new int[nintervals][mysites]; 
        int[][] coordOfBest = new int[nintervals][mysites];
        String [][] chrOfBest = new String[nintervals][mysites];
        String [][] POfAllBest = new String[nintervals][mysites];
        String [][] chrOfAllBest = new String[nintervals][mysites];
        String [][] coordOfAllBest = new String[nintervals][mysites];
        
        // R2 and p-value histograms
        int dimR2hist = 50;
        double incrR2hist = 1.0/dimR2hist;
        int [] R2hist = new int[dimR2hist+1];
        int [] pVhist = new int[100];
        //
        
        for(int ivl=0;ivl<nintervals;ivl++)
        {
            Arrays.fill(maxR2[ivl], 0.0F);
            //Arrays.fill(minPOfMaxR2[ivl], Float.NaN);
            // p-value =-1 will mean that LD could not be computed for the site
            // because of too low MAF. If any suitable site is found, this will
            // be changed to 1 or less...
            Arrays.fill(minPOfMaxR2[ivl], -1.0F);
            Arrays.fill(minSigDistance[ivl], Integer.MAX_VALUE);
            Arrays.fill(nLDsites[ivl], 0);
            Arrays.fill(coordOfBest[ivl], 0);
            Arrays.fill(chrOfBest[ivl],"NA");
            Arrays.fill(POfAllBest[ivl],"");
            Arrays.fill(coordOfAllBest[ivl],"");
        }
        
        for (int i = startRange; i <= endRange; i++) {
            int myi = i - startRange;
            int[][] contig = new int[2][2];
            
            //System.out.println("DEBUG: position of index "+i);
            
            Locus myLocus=a.getLocus(i);
            int position=a.getPositionInLocus(i);
            
            BitSet rMj = a.getAllelePresenceForAllTaxa(i, 0);
            BitSet rMn = a.getAllelePresenceForAllTaxa(i, 1); 
            if(rMn.cardinality()<minMinorCnt) {/*System.out.println("Not enough minor in site:"+rMn.cardinality());*/continue;}
            //TreeMap<Double,SiteMappingInfo> bestLDSites=new TreeMap<Double,SiteMappingInfo>(Collections.reverseOrder());
            List<TreeMap<Double,SiteMappingInfo>> bestLDSites = new ArrayList<TreeMap<Double,SiteMappingInfo>>(3);
            for(interval=0;interval<nintervals;interval++)
            {
                bestLDSites.add(interval,new TreeMap<Double,SiteMappingInfo>(Collections.reverseOrder()));
            }
            double [] minLD=new double[nintervals]; Arrays.fill(minLD, 0.0);
            int attemptTests=0, completedTests=0, sigTests=0;
            
            //System.out.println("DEBUG: position "+position + " left,rightSite: "+ leftSite + " " + rightSite);
            // This needs to be replaced by loop over all sites of the anchor map, possibly with some restrictions
            // while(((leftSite>=0)||(rightSite+1<sites))&&(dist<maxPhysDist)) 
            for(int j=0;j<sites;j++)
            {
                dist = Integer.MAX_VALUE;
                if(a.getLocusName(i).equals(aa.getLocusName(j)))
                {
                    dist = Math.abs(a.getPositionInLocus(i) - aa.getPositionInLocus(j));
                }
                if(dist <= minPhysDist){ continue; }
                
                attemptTests++;
               // System.out.printf("Dist: %d %d bin: %d %n",dist, Integer.highestOneBit(dist), bin);
                //System.out.println("DEBUG: testing j "+ j + " dist: "+ dist);
                BitSet cMj = aa.getAllelePresenceForAllTaxa(j, 0);  //major alleles
                BitSet cMn = aa.getAllelePresenceForAllTaxa(j, 1);  //minor alleles
                int n = 0;
                n += contig[1][1] = (int) OpenBitSet.intersectionCount(rMn, cMn);
                n += contig[1][0] = (int) OpenBitSet.intersectionCount(rMn, cMj);
                if(contig[1][0]+contig[1][1]<minMinorCnt) {/*System.out.println("Not enough minor in raw:"+n);*/continue;}
                n += contig[0][1] = (int) OpenBitSet.intersectionCount(rMj, cMn);
                int naux = contig[0][1]+contig[1][1];
                if(contig[0][1]+contig[1][1]<minMinorCnt) {/*System.out.println("Not enough minor in col:"+naux);*/continue;}
                n += contig[0][0] = (int) OpenBitSet.intersectionCount(rMj, cMj);
                if(n<minCnt) {/*System.out.println("Not enough n:"+n);*/continue;}
                double rValue = LinkageDisequilibrium.calculateRSqr(contig[0][0], contig[1][0], contig[0][1], contig[1][1], minCnt);
                if (Double.isNaN(rValue)) {/*System.out.println("R2 is NaN:");*/continue;}
                completedTests++;
                
                // Determine the distance interval to record the values in
                // For now, we will just have a single "interval"
                interval = 0;
                
                // So, we found a site to compute LD with - change min p-value from -1 to 1.
                // Sites with p-value=-1 will be deemed unclassifiable, sites with pvalue=1
                // will mean bad LD (if not improved on later).
                minPOfMaxR2[interval][myi] = 1.0F;
                if(rValue<minR2) {/*System.out.println("rValue too small:"+rValue);*/continue;}
                double pValue=myFisherExact.getTwoTailedP(contig[0][0], contig[1][0], contig[0][1], contig[1][1]);
                if(pValue>maxPvalue) {/*System.out.println("pValue too large:"+pValue);*/continue;}
                
                sigTests++;
                
                // Update the R2 and p-value histograms
                int histpos = (int)(rValue/incrR2hist);
                R2hist[histpos]++;
                histpos = (int)(-Math.log10(pValue));
                if(histpos > 99) histpos = 99;
                pVhist[histpos]++;
                // End of histogram update
                      
                // Update minimum distance to significant hit if it is on the same locus (chromosome)
                if(dist<minSigDistance[interval][myi]) minSigDistance[interval][myi]=dist;
                
                if(sortOnR2)
                {
                    if(rValue>minLD[interval]) {
                        //float[] result={j, (float)rValue, (float)pValue, dist};
                        SiteMappingInfo smi=new SiteMappingInfo(Integer.parseInt(aa.getLocusName(j)),
                            (byte)1,aa.getPositionInLocus(j),(float)rValue, (float)pValue,j);
                        bestLDSites.get(interval).put(rValue, smi);
                    }
                }
                else
                {
                    logPvalue = -Math.log10(pValue);
                    if(logPvalue>minLD[interval]) {
                        SiteMappingInfo smi=new SiteMappingInfo(Integer.parseInt(aa.getLocusName(j)),
                            (byte)1,aa.getPositionInLocus(j),(float)rValue, (float)pValue,j);
                        bestLDSites.get(interval).put(logPvalue, smi);
                    }
                }
                
                if(bestLDSites.get(interval).size()>20) {
                    bestLDSites.get(interval).remove(bestLDSites.get(interval).lastKey());
                    minLD[interval]=bestLDSites.get(interval).lastKey();
                }
            
            } // loop over sites in anchor map
            
            for(interval=0;interval<nintervals;interval++)
            {
                POfAllBest[interval][myi] = "";
                coordOfAllBest[interval][myi] = "";
                if(bestLDSites.get(interval).size()>0) {
                    SiteMappingInfo smi=(SiteMappingInfo)bestLDSites.get(interval).firstEntry().getValue();
                    maxR2[interval][myi]=smi.r2;
                    minPOfMaxR2[interval][myi]=smi.mapP;
                    nLDsites[interval][myi]=bestLDSites.get(interval).size();
                    chrOfBest[interval][myi]=String.valueOf(smi.chromosome);
                    coordOfBest[interval][myi] = smi.position;
                    
                    for(int k=0;k<bestLDSites.get(interval).size();k++)
                    {
                        //smi=(SiteMappingInfo)bestLDSites.get(interval).get(bestLDSites.get(interval).keySet().toArray()[k]);
                        smi=(SiteMappingInfo)bestLDSites.get(interval).values().toArray()[k];
                        //smi=(SiteMappingInfo)bestLDSites.get(interval).values().iterator().next();
                        POfAllBest[interval][myi] += String.valueOf(smi.mapP) + ",";
                        chrOfAllBest[interval][myi] += String.valueOf(smi.chromosome) + ",";
                        coordOfAllBest[interval][myi] += String.valueOf(smi.position) + ",";
                    }
                }
            }
            
            // Report incremental timing
            if((myi+1)%10000 == 0)
            {
                endTime = System.currentTimeMillis();
                duration = 0.001*(endTime - startTime)/60;
                System.out.printf("LDthread %d-%d progress: %d%% in %.3f minutes %n", startRange, endRange, 100*(myi+1)/mysites, duration);
            }
        }
        
        // write out the results (file open and closed in method "run"
        // Actually, wrinitng could be done inside the i loop, saving some array memory
        try
        {
            if(startRange == 0)
            {
                bw.write("pos\tmaxR2_0\tpv_0\tmindist_0\t");
                bw.write("nsites_0\tChrom_of_best\tCoord_of_best\tpv_all_best\tccord_all_best\n");
            }
            for(int i=0;i<mysites;i++)
            {
                bw.write(String.valueOf(a.getPositionInLocus(i+startRange)));
                bw.write("\t" + maxR2[0][i]);
                bw.write("\t" + minPOfMaxR2[0][i]);
                bw.write("\t" + minSigDistance[0][i]);
                bw.write("\t" + nLDsites[0][i]);
                bw.write("\t" + chrOfBest[0][i]);
                bw.write("\t" + coordOfBest[0][i]);
                bw.write("\t" + POfAllBest[0][i]);
                bw.write("\t" + chrOfAllBest[0][i]);
                bw.write("\t" + coordOfAllBest[0][i] + "\n");
            }   
            bw.write("R2 histogram:\n");
            for(int i=0;i<=dimR2hist;i++)
            {
                double pos_tmp = i*incrR2hist;
                bw.write(pos_tmp + "\t" + R2hist[i] + "\n");
            }
            bw.write("pV histogram:\n");
            for(int i=0;i<100;i++)
            {
                bw.write(i + "\t" + pVhist[i] + "\n");
            }
        }
        catch(Exception e)
        {
            System.out.println("output could not be written"+e);
        }
    }
    
    /**
     * For position with index ii in alignment a, find the indexes of left and right
     * closest positions in alignment aa. Use the bisection method
     * @param a
     * @param aa
     * @param ii
     * @return 
     */
    private int [] findFlankingIndexes(Alignment a, Alignment aa, int ii)
    {
        int [] pos = new int[2];
        int iipos = a.getPositionInLocus(ii);
        int newpos=0;
        int is = 0;
        int aacount = aa.getSiteCount();
        int ie = aacount;
        int i0 = -1;
        int i0_new = (is+ie)/2;
        while(i0 != i0_new)
        {
            i0 = i0_new;
            newpos = aa.getPositionInLocus(i0);
            //System.out.println("DEBUG: is,ie,i0,newpos:"+is+" "+ie+" "+i0+" "+newpos);
            if(newpos < iipos)
            {
                is = i0;
            }
            else if(newpos > iipos)
            {
                ie = i0;
            }
            else
            {
                pos[0] = i0 - 1;
                pos[1] = i0 + 1;
                return pos;
            }
            i0_new = (is+ie)/2;
        }
        pos[0] = i0;
        pos[1] = i0 + 1;
        if(newpos > iipos)
        {
            pos[0]=-1;
            pos[1] = 0;
        }
        if(iipos > aa.getPositionInLocus(aacount-1))
        {
            pos[0] = aacount-1;
            pos[1] = aacount;
        }
        
        return pos;
    }
    
}
