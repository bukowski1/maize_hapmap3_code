/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import net.maizegenetics.util.Utils;
import net.maizegenetics.util.ExceptionUtils;
import net.maizegenetics.pal.alignment.Alignment;
import net.maizegenetics.pal.alignment.BitAlignment;
import net.maizegenetics.pal.alignment.Locus;
import net.maizegenetics.pal.alignment.ImportUtils;
import net.maizegenetics.util.OpenBitSet;
import net.maizegenetics.prefs.TasselPrefs;
import net.maizegenetics.pal.ids.IdGroup;
import net.maizegenetics.pal.ids.SimpleIdGroup;
import net.maizegenetics.util.ProgressListener;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


/**
 *
 * @author bukowski
 * 
 * Read in the genotyping data into alignment object,
 * then launch LD calculations on the requested number of threads.
 * Each thread deals with its own subset of sites (i.e., computes LD
 * between each site in this subset and other sites within window around it).
 * Each thread has access to the whole alignment object.
 */
public class pLDannotator implements ProgressListener {
    private Alignment a;
    private Alignment aa;  // Anchor alignment object - good SNPs to calculate LD with
    
    /**
     * @param genoFile: path to the genotype file
     * @param paramFile: path to the xml file with run options
     * @param numThreads: number of threads to launch
     */
    public pLDannotator(String genoFile, String paramFile, int numThreads, boolean doglobalIn)
    {
        long startTime, endTime;
        double duration;
        
        // Load the parameters from xml file
        ld_params params = new ld_params();
        try
        {
            JAXBContext context = JAXBContext.newInstance(ld_params.class);
            Unmarshaller um = context.createUnmarshaller();
            try
            {
                Reader r = new FileReader(paramFile);
                params = (ld_params) um.unmarshal(r);
                print_ld_params(params);
            }
            catch(IOException i)
            {
                System.out.println("Could not read the parameter file");
                 i.printStackTrace();
            }
        }
        catch(JAXBException i)
        {
            System.out.println("Could not de-serialize the parameter class");
            i.printStackTrace();
        }
        
        // Read the genotype file
        startTime = System.currentTimeMillis();
        if(params.getFormat().equals("RBfmt"))
        {
            a = readFromRBfmt(genoFile, true, numThreads, params.getConvhet(), this);
        } 
        else if (params.getFormat().equals("HapMap"))
        {
            a = ImportUtils.readFromHapmap(genoFile, true, null);
        }
        
        endTime = System.currentTimeMillis();
        duration = 0.001*(endTime-startTime)/60;
        startTime = endTime;
        System.out.printf("Alignment loaded in %.3f minutes %n", duration);
        
        System.out.println("Genotype input file: " + genoFile);
        int numsites_all = a.getSiteCount();
        System.out.println("Number of sites in genotype input file: " + numsites_all);
        
        // Read the anchor file, if present, and load it into aa alignment object
        if(params.getAnchorFile() != null)
        {
            if(params.getAnchorFormat().equals("RBfmt"))
            {
                aa = readFromRBfmt(params.getAnchorFile(), true, numThreads, params.getConvhet(), this);
            }
            else if(params.getAnchorFormat().equals("HapMap"))
            {
                aa = ImportUtils.readFromHapmap(params.getAnchorFile(), true, null);
            }
            endTime = System.currentTimeMillis();
            duration = 0.001*(endTime-startTime)/60;
            startTime = endTime;
            System.out.printf("Anchor alignment loaded in %.3f minutes %n", duration);
        
            System.out.println("Anchor input file: " + params.getAnchorFile());
            System.out.println("Number of sites in anchor input file: " + aa.getSiteCount());
        }
        else
        {
            // make anchor alignent just point to the actual alignment
            aa = a;
        }
        
        java.util.concurrent.ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        
        // Distribute the work among threads
        ArrayList<Thread> my_threads = new ArrayList<Thread>();
        int sites_per_thread = numsites_all/numThreads;
        int start_thr = 0;
        int end_thr;
        while(start_thr < numsites_all)
        {
            end_thr = Math.min(start_thr + sites_per_thread - 1, numsites_all - 1);
            //System.out.println("Main thread submitting subrange " + start_thr + "-" + end_thr);
            rangeLDannotator pgnt = new rangeLDannotator(a, aa, start_thr, end_thr, 
                    params.getMinPhysDist(), 
                    params.getMaxPhysDist(), 
                    params.getMinMinorCnt(), 
                    params.getMinR2(), 
                    params.getMaxPvalue(),
                    doglobalIn);
            //Thread thr = new Thread(pgnt);
            //thr.start();
            executor.execute(pgnt);
            //my_threads.add(thr);
            start_thr = end_thr + 1;
        }
        
        executor.shutdown();
        try
        {
            executor.awaitTermination(100, TimeUnit.DAYS);
        }
        catch(Exception e)
        {
            System.out.println("executor termination problem: "+e);
        }
    }
    
    
    /**
     * Read from RB's genotype format, which is similar to VCF, but not identical
     * Adapted from ImportUtils.readFromHapMap 
     * @param filename: path to the genotype file
     * @param isSBit
     * @param convhet: if true, treat het sites as homozygous in minor allele
     * @param listener
     * @return 
     */
        public static Alignment readFromRBfmt(final String filename, boolean isSBit, int numThreads, boolean convhet, ProgressListener listener) {

        final Pattern WHITESPACE_PATTERN = Pattern.compile("\\s");
        final int NUM_NON_TAXA_HEADERS=5;
        final int POSITION_COLUMN_INDEX=1;
        final int CHROMOSOME_COLUMN_INDEX=0;
            
        int minPosition = Integer.MAX_VALUE;
        String currLocus = null;
        List<Locus> loci = new ArrayList<Locus>();
        List<Integer> lociOffsets = new ArrayList<Integer>();
        double duration;

        long currentTime = System.currentTimeMillis();
        int numSites = Utils.getNumberLines(filename) - 1;
        //myLogger.info("readFromHapmap: Number of Sites: " + numSites);
        System.out.println("readFromRBfmt: Number of Sites: " + numSites);

        long prevTime = currentTime;
        currentTime = System.currentTimeMillis();
        //myLogger.info("readFromHapmap: Time to count lines: " + ((currentTime - prevTime) / 1000));
        duration = 0.001*(currentTime - prevTime)/60;
        prevTime = currentTime;
        System.out.printf("readFromRBfmt: Time to count lines: %.3f minutes %n",duration);


        BufferedReader fileIn = null;
        try {
            // int numThreads = Runtime.getRuntime().availableProcessors();
            ExecutorService pool = Executors.newFixedThreadPool(numThreads);

            fileIn = Utils.getBufferedReader(filename, 1000000);
            String[] header = WHITESPACE_PATTERN.split(fileIn.readLine());
            int lineInFile = 1;
            int numTaxa = header.length - NUM_NON_TAXA_HEADERS;
            String[] snpIDs = new String[numSites];
            int prevPosition = -1;

            OpenBitSet[][] theData;
            byte[][] alleles = new byte[numSites][TasselPrefs.getAlignmentMaxAllelesToRetain()];
            int numDataRows = TasselPrefs.getAlignmentMaxAllelesToRetain();
            if (TasselPrefs.getAlignmentRetainRareAlleles()) {
                numDataRows++;
            }
            int numSitesToProcess = 1;
            if (isSBit) {
                theData = new OpenBitSet[numDataRows][numSites];
                numSitesToProcess = 1;
            } else {
                theData = new OpenBitSet[numDataRows][numTaxa];
                for (int al = 0; al < numDataRows; al++) {
                    for (int t = 0; t < numTaxa; t++) {
                        theData[al][t] = new OpenBitSet(numSites);
                    }
                }
                numSitesToProcess = 64;
            }

            int[] physicalPositions = new int[numSites];
            int count = 0;
            String[][] tokens = new String[numSitesToProcess][];
            int currentSite = 0;
            for (int site = 0; site < numSites; site++) {

                lineInFile++;

                String input = fileIn.readLine();
                tokens[count] = WHITESPACE_PATTERN.split(input);

                snpIDs[site] = tokens[count][0] + "_" + tokens[count][1];
                int position = Integer.parseInt(tokens[count][POSITION_COLUMN_INDEX]);
                String temp = new String(tokens[count][CHROMOSOME_COLUMN_INDEX]);
                if (currLocus == null) {
                    lociOffsets.add(site);
                    currLocus = temp;
                    minPosition = position;
                    prevPosition = -1;
                } else if (!temp.equals(currLocus)) {
                    loci.add(new Locus(currLocus, currLocus, minPosition, prevPosition, null, null));
                    lociOffsets.add(site);
                    currLocus = temp;
                    minPosition = position;
                    prevPosition = -1;
                }

                if (position < prevPosition) {
                    throw new IllegalStateException("readFromRBfmt: Sites are not properly sorted for chromosome: " + currLocus + " at " + position + " and " + prevPosition);
                }

                count++;

                if (count == numSitesToProcess) {
                    pool.execute(ProcessLineOfRBfmt.getInstance(alleles, theData, TasselPrefs.getAlignmentRetainRareAlleles(), tokens, count, currentSite, numTaxa, lineInFile, isSBit, convhet));
                    count = 0;
                    currentSite += numSitesToProcess;
                    tokens = new String[numSitesToProcess][];
                }

                physicalPositions[site] = position;
                prevPosition = position;

                if((site+1)%100000 == 0)
                {
                    if (listener != null) {
                        currentTime = System.currentTimeMillis();
                        duration = 0.001*(currentTime - prevTime)/60;
                        listener.progress((int) (((double) (site + 1) / (double) numSites) * 100.0), duration);
                    }
                }
                
            }

            if (count != 0) {
                pool.execute(ProcessLineOfRBfmt.getInstance(alleles, theData, TasselPrefs.getAlignmentRetainRareAlleles(), tokens, count, currentSite, numTaxa, lineInFile, isSBit, convhet));
            }


            pool.shutdown();
            if (!pool.awaitTermination(6000, TimeUnit.SECONDS)) {
                throw new IllegalStateException("ImportUtils: readFromHapmap: processing threads timed out.");
            }

            if (currLocus != null) {
                loci.add(new Locus(currLocus, currLocus, minPosition, prevPosition, null, null));
            }

            currentTime = System.currentTimeMillis();
            duration = 0.001*(currentTime - prevTime)/60;
            listener.progress(100, duration);
            prevTime = currentTime;
            //myLogger.info("readFromHapmap: Time to read file: " + ((currentTime - prevTime) / 1000));
            System.out.printf("readFromRBfmt: Time to read file: %.3f minutes %n", duration);

            String[] taxaNames = new String[numTaxa];
            System.arraycopy(header, NUM_NON_TAXA_HEADERS, taxaNames, 0, numTaxa);
            IdGroup idGroup = new SimpleIdGroup(taxaNames);

            Locus[] lociFinal = new Locus[loci.size()];
            loci.toArray(lociFinal);
            int[] offsetsFinal = new int[lociOffsets.size()];
            for (int i = 0; i < lociOffsets.size(); i++) {
                offsetsFinal[i] = ((Integer) lociOffsets.get(i)).intValue();
            }

            Alignment result = BitAlignment.getNucleotideInstance(idGroup, alleles, theData, null, null, physicalPositions, TasselPrefs.getAlignmentMaxAllelesToRetain(), lociFinal, offsetsFinal, snpIDs, TasselPrefs.getAlignmentRetainRareAlleles(), isSBit);

            currentTime = System.currentTimeMillis();
            duration = 0.001*(currentTime - prevTime)/60;
            prevTime = currentTime;
            //myLogger.info("readFreromHapmap: Time to create Alignment: " + ((currentTime - prevTime) / 1000));
            System.out.printf("readFreromRBfmt: Time to create Alignment: %.3f minutes %n", duration);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("readFromRBfmt: Problem creating Alignment: " + filename + ": " + ExceptionUtils.getExceptionCauses(e));
        } finally {
            try {
                fileIn.close();
            } catch (Exception ex) {
                // do nothing
            }
        }

    }
        /**
         * Echo the run parameters 
         * @param params: object with run parameters 
         */
    private void print_ld_params(ld_params params)
    {
        System.out.println("LD annotator paramneters");
        
        System.out.println("Input format: " +params.getFormat());
        System.out.println("Minimum physical distance: " +params.getMinPhysDist()); 
        System.out.println("Maximum physcial distance: " +params.getMaxPhysDist());
        System.out.println("Minimum minor allele count: " +params.getMinMinorCnt());
        System.out.println("Minimum R2: " +params.getMinR2());
        System.out.println("Max allowed p-value: " +params.getMaxPvalue());
        System.out.println("Hets converted to minor homs: " +params.getConvhet());
        if(params.getAnchorFile() != null)
        {
            System.out.println("Anchor file: " + params.getAnchorFile());
            System.out.println("Anchor file format: " + params.getAnchorFormat());
        }
    }
    
    @Override
    public void progress(int i, Object obj)
    {
        System.out.printf("%d%% of file read in %.3f minutes %n",i,(Double)obj);
    }
}
