/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bukowski
 */
@XmlRootElement(name = "ld_params")
public class ld_params {
    private int minPhysDist=250;
    private int maxPhysDist=50000;
    private int minMinorCnt=4; 
    private float minR2=0.05F;
    private float maxPvalue=0.01F;
    private String format="RBfmt";   // can be this or "HapMap"
    private boolean convhet=false;
    private String anchorFile=null;
    private String anchorFormat="HapMap";
    
    public void setMinPhysDist(Integer sr)
    {
            this.minPhysDist = sr;
    }
    public void setMaxPhysDist(Integer sr)
    {
            this.maxPhysDist = sr;
    }
    public void setMinMinorCnt(Integer sr)
    {
            this.minMinorCnt = sr;
    }
    public void setMaxPvalue(float sr)
    {
            this.maxPvalue = sr;
    }
    public void setMinR2(float sr)
    {
            this.minR2 = sr;
    }
    public void setFormat(String sr)
    {
            this.format = sr;
    }
    public void setConvhet(boolean sr)
    {
        this.convhet = sr;
    }
    public void setAnchorFile(String sr)
    {
        this.anchorFile = sr;
    }
    public void setAnchorFormat(String sr)
    {
        this.anchorFormat = sr;
    }
    
    public Integer getMinPhysDist()
    {
            return minPhysDist;
    }
    public Integer getMaxPhysDist()
    {
            return maxPhysDist;
    }
    public Integer getMinMinorCnt()
    {
            return minMinorCnt;
    }
    public float getMaxPvalue()
    {
            return maxPvalue;
    }
    public float getMinR2()
    {
            return minR2;
    }
    public String getFormat()
    {
            return format;
    }
    public boolean getConvhet()
    {
        return convhet;
    }
    public String getAnchorFile()
    {
        return anchorFile; 
    }
    public String getAnchorFormat()
    {
        return anchorFormat;
    }
}
