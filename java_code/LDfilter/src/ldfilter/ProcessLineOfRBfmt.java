/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldfilter;

import net.maizegenetics.util.BitUtil;
import net.maizegenetics.util.OpenBitSet;
import net.maizegenetics.pal.alignment.NucleotideAlignmentConstants;
import net.maizegenetics.pal.alignment.AlignmentUtils;
import net.maizegenetics.pal.alignment.Alignment;

/**
 *
 * @author bukowski
 */
public class ProcessLineOfRBfmt implements Runnable {
    
    private String[][] myTokens;
    private int mySite;
    private int myNumTaxa;
    private int myLineInFile;
    private boolean myComplete = false;
    private OpenBitSet[][] myData;
    private byte[][] myAlleles;
    private int[][] myAlleleMappings;
    private int myNumAlleles;
    private int myNumDataRows;
    private boolean myRetainRareAlleles;
    private boolean myIsSBit;
    private int myNumSitesToProcess;
    private int NUM_NON_TAXA_HEADERS=5;
    private boolean myConvhet;

    private ProcessLineOfRBfmt(byte[][] alleles, OpenBitSet[][] data, boolean retainRareAlleles, String[][] tokens, int numSitesToProcess, int site, int numTaxa, int lineInFile, boolean isSBit, boolean convhet) {
        setVariables(alleles, data, retainRareAlleles, tokens, numSitesToProcess, site, numTaxa, lineInFile, isSBit, convhet);
    }

    public static ProcessLineOfRBfmt getInstance(byte[][] alleles, OpenBitSet[][] data, boolean retainRareAlleles, String[][] tokens, int numSitesToProcess, int site, int numTaxa, int lineInFile, boolean isSBit, boolean convhet) {
        return new ProcessLineOfRBfmt(alleles, data, retainRareAlleles, tokens, numSitesToProcess, site, numTaxa, lineInFile, isSBit, convhet);
    }

    private void setVariables(byte[][] alleles, OpenBitSet[][] data, boolean retainRareAlleles, String[][] tokens, int numSitesToProcess, int site, int numTaxa, int lineInFile, boolean isSBit, boolean convhet) {

        myData = data;
        myTokens = tokens;
        myNumSitesToProcess = numSitesToProcess;
        if (myNumSitesToProcess > 64) {
            throw new IllegalStateException("ProcessLineOfRBfmt: setVariables: Can't process more than 64 sites: " + myNumSitesToProcess);
        }
        mySite = site;
        myNumTaxa = numTaxa;
        myLineInFile = lineInFile;
        myComplete = false;
        myAlleles = alleles;
        myNumAlleles = myAlleles[0].length;
        myAlleleMappings = new int[myNumSitesToProcess][16];
        for (int s = 0; s < myNumSitesToProcess; s++) {
            for (int i = 0; i < 16; i++) {
                myAlleleMappings[s][i] = -1;
            }
        }
        myRetainRareAlleles = retainRareAlleles;
        myNumDataRows = myNumAlleles;
        if (myRetainRareAlleles) {
            myNumDataRows++;
        }

        if (myNumDataRows != myData.length) {
            throw new IllegalStateException("ProcessLineOfRBfmt: setVariables: number of data rows: " + myNumDataRows + " should equal first dimension of data array: " + myData.length);
        }

        myIsSBit = isSBit;
        myConvhet = convhet;
    }

    @Override
    public void run() {
        try {
            if (myComplete) {
                throw new IllegalStateException("ProcessLineOfRBfmt: run: trying to run completed instance.");
            }
            myComplete = true;

            byte[][] data = new byte[myNumSitesToProcess][myNumTaxa];
            String aux [];
            String refall=null;
            String [] allall;
            String [] haplotypes = new String[2];
            String genotype = null;
            int [] alnums = new int[2];
            String [] salnums = new String[2];
            for (int s = 0; s < myNumSitesToProcess; s++) {
                refall = myTokens[s][2] + "," + myTokens[s][3];
                allall = refall.split(",");
                for (int i = 0; i < myNumTaxa; i++) {
                    try {
                        // Need to process the genotypes here....
                        if(myTokens[s][NUM_NON_TAXA_HEADERS + i].equals("./."))
                        {
                            genotype = "NN";
                        }
                        else
                        {
                            aux = myTokens[s][NUM_NON_TAXA_HEADERS + i].split(":");
                            haplotypes = aux[0].split("/");
                            if(myConvhet)
                            {
                                // hets will be treated as homs of the minor (not alternative!) allele
                                // use the info string to see which allele is minor
                                salnums = myTokens[s][4].split(";")[3].split("=")[1].split(",");
                                alnums[0] = Integer.valueOf((salnums[0]));
                                alnums[1] = Integer.valueOf((salnums[1]));
                                if(alnums[0] > alnums[1])
                                {
                                    genotype =  allall[Integer.parseInt(haplotypes[1])];
                                    genotype += genotype;
                                }
                                else
                                {
                                    genotype =  allall[Integer.parseInt(haplotypes[0])];
                                    genotype += genotype;
                                }
                            
                            }
                            else
                            {
                                // hets will be treated as hets
                                genotype =  allall[Integer.parseInt(haplotypes[0])];
                                genotype += allall[Integer.parseInt(haplotypes[1])];
                            }
                        }
                        genotype = genotype.replaceAll("D", "-").replaceAll("I", "+");
                        data[s][i] = NucleotideAlignmentConstants.getNucleotideDiploidByte(genotype);
                    } catch (IndexOutOfBoundsException ex) {
                        throw new IllegalStateException("Number of Taxa: " + myNumTaxa + " does not match number of values at line in file: " + (myLineInFile + s) + " site: " + (mySite + s) + " Taxon: " +i);
                    } catch (Exception e) {
                        throw new IllegalStateException("Problem with line in file: " + (myLineInFile + s), e);
                    }
                }
            }

            setAlleles(data);
            if (myIsSBit) {
                setSBits(data);
            } else {
                setTBits(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAlleles(byte[][] data) {
        for (int s = 0; s < myNumSitesToProcess; s++) {
            int[][] alleles = AlignmentUtils.getAllelesSortedByFrequency(data[s]);
            int resultSize = alleles[0].length;
            for (int i = 0; i < myNumAlleles; i++) {
                if (i < resultSize) {
                    myAlleles[mySite + s][i] = (byte) alleles[0][i];
                    myAlleleMappings[s][myAlleles[mySite + s][i]] = i;
                } else {
                    myAlleles[mySite + s][i] = Alignment.UNKNOWN_ALLELE;
                }
            }
        }
    }

    private void setSBits(byte[][] data) {

        int numLongs = BitUtil.bits2words(myNumTaxa);
        for (int s = 0; s < myNumSitesToProcess; s++) {

            long[][] bits = new long[myNumDataRows][numLongs];
            byte[] cb = new byte[2];
            for (int l = 0; l < numLongs - 1; l++) {
                long bitmask = 0x1L;
                for (int t = l * 64, n = l * 64 + 64; t < n; t++) {
                    cb[0] = (byte) ((data[s][t] >>> 4) & 0xf);
                    cb[1] = (byte) (data[s][t] & 0xf);
                    for (int i = 0; i < 2; i++) {
                        if (cb[i] != Alignment.UNKNOWN_ALLELE) {
                            int index = myAlleleMappings[s][cb[i]];
                            if (index == -1) {
                                if (myRetainRareAlleles) {
                                    bits[myNumAlleles][l] |= bitmask;
                                }
                            } else {
                                bits[index][l] |= bitmask;
                            }
                        }
                    }
                    bitmask = bitmask << 1;
                }
            }

            int lastLong = numLongs - 1;
            int numRemaining = myNumTaxa % 64;
            if (numRemaining == 0) {
                numRemaining = 64;
            }
            long bitmask = 0x1L;
            for (int t = lastLong * 64, n = lastLong * 64 + numRemaining; t < n; t++) {
                cb[0] = (byte) ((data[s][t] >>> 4) & 0xf);
                cb[1] = (byte) (data[s][t] & 0xf);
                for (int i = 0; i < 2; i++) {
                    if (cb[i] != Alignment.UNKNOWN_ALLELE) {
                        int index = myAlleleMappings[s][cb[i]];
                        if (index == -1) {
                            if (myRetainRareAlleles) {
                                bits[myNumAlleles][lastLong] |= bitmask;
                            }
                        } else {
                            bits[index][lastLong] |= bitmask;
                        }
                    }
                }
                bitmask = bitmask << 1;
            }

            for (int i = 0; i < myNumDataRows; i++) {
                myData[i][mySite + s] = new OpenBitSet(bits[i], numLongs);
            }

        }

    }

    private void setTBits(byte[][] data) {

        if (mySite % 64 != 0) {
            throw new IllegalStateException("ProcessLineOfHapmap: setTBits: starting site must begin a word: " + mySite);
        }
        int wordNum = mySite / 64;

        long[] bits = new long[myNumDataRows];

        for (int t = 0; t < myNumTaxa; t++) {
            for (int j = 0; j < myNumDataRows; j++) {
                bits[j] = 0;
            }

            byte[] cb = new byte[2];
            long bitmask = 0x1L;
            for (int s = 0; s < myNumSitesToProcess; s++) {
                cb[0] = (byte) ((data[s][t] >>> 4) & 0xf);
                cb[1] = (byte) (data[s][t] & 0xf);
                for (int i = 0; i < 2; i++) {
                    if (cb[i] != Alignment.UNKNOWN_ALLELE) {
                        int index = myAlleleMappings[s][cb[i]];
                        if (index == -1) {
                            if (myRetainRareAlleles) {
                                bits[myNumAlleles] |= bitmask;
                            }
                        } else {
                            bits[index] |= bitmask;
                        }
                    }
                }
                bitmask = bitmask << 1;
            }

            for (int i = 0; i < myNumDataRows; i++) {
                myData[i][t].setLong(wordNum, bits[i]);
            }

        }

    }
    
}
