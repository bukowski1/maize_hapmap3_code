/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package contmatpack;

import jsc.distributions.ChiSquared;

/**
 *
 * @author bukowski
 */
public class HWtest {
    
    public static double [] getChisqPvalue(int n11, int n12, int n22)
    {
        double [] result = new double[2];
        result[0] = 0; result[1] = 1.;
        int n = n11 + n12 + n22;
        double p = 0.5*(n12 + 2*n11)/n;
        
        if(p == 1 || p == 0)
        {
            return result;
        }
        
        double exp11 = p*p*n;
        double exp22 = (1-p)*(1-p)*n;
        double exp12 = 2*p*(1-p)*n;
        
        double chisq = (n11-exp11)*(n11-exp11)/exp11;
        chisq += (n22-exp22)*(n22-exp22)/exp22;
        chisq += (n12-exp12)*(n12-exp12)/exp12;
        
//        System.out.println("Ns: " + n11 + " "+ n12 + " "+ n22);
//        System.out.println("exps: " + exp11 + " "+ exp12 + " "+ exp22);
//        System.out.println("p: " + p);
//        System.out.println("chisq: " + chisq);
        
        result[0] = chisq;
        result[1] = ChiSquared.upperTailProb(chisq, 1.0);
        
        return result;
    }
    
}
