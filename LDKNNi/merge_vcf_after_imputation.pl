#!/usr/bin/env perl

# Merge overlapping VCF files obtained from piece-wise imputation

$chr = $ARGV[0];

# XXX will be replaced by chromosme number, and YYY - by chunk number
$fnametempl = "hmp321_chrXXX_chunkYYY_imputed.vcf.gz";

# File name (update after test):
# merged_flt_c9.vcf_chunk1.vcf.gz
# merged_flt_c3.vcf_chunk6_imputed.vcf.gz

$taxaindex=9;

# Calculate the number of output (im puted) files and the position boundaries of each
# This should be exactly the same as in splitting program

$nchunk = 30000000;
$overlap = 500000;
$overlap2 = $overlap/2;

# How many chunk files we have for this chromosome?
# Naming convention: hmp321_agpv4_282only_chr10_chunk0_imputed.vcf.gz

$str = $fnametempl;
$str =~ s/XXX/$chr/;
$str =~ s/YYY/\*/;
print "Template to look for: $str\n";

$nchunks = `ls -1 $str | wc -l`;
chomp $nchunks;
print STDERR "$nchunks chunks will be merged for chr $chr\n";

$endpos = 0;
$fcounter = 0;
@sboundary = ();
@eboundary = ();
while($fcounter < $nchunks)
{
	$beg = $endpos + 1;
	$end = ($fcounter + 1)*$nchunk - (2*$fcounter +1)*$overlap2;
	$sboundary[$fcounter] = $beg;
	$eboundary[$fcounter] = $end; 
	$endpos = $end;
	$fcounter++;
}

print STDERR "$fcounter files will be merged with the following boundaries\n";
for($i=0;$i<=$#sboundary;$i++)
{
	print STDERR "$sboundary[$i]\t$eboundary[$i]\n";
}

# Open chunk files, read and save requested records
# First, retrieve the header

$str = $fnametempl;
$str =~ s/XXX/$chr/;
$str =~ s/YYY/0/;

$fname = $str;
$str =~ s/chunk0_//;
$str =~ s/\.gz//;
$ofname = $str;

system("gzip -d -c $fname | head -100 | grep \"#\" > $ofname");
open(out,">>$ofname");
for($i=0;$i<$nchunks;$i++)
{
	$str = $fnametempl;
	$str =~ s/XXX/$chr/;
	$str =~ s/YYY/$i/;
	$fname = $str;
	print STDERR "Processing chunk file $fname\n";
	open(in,"gzip -d -c $fname |");
	while($line=<in>)
	{
		if($line =~ /#/) { next; }
		chomp $line;
		@aux = split /\t/, $line;
		if($aux[1] >= $sboundary[$i] && $aux[1] <= $eboundary[$i]) 
		{
			print out "$line\n";
		}
	}
	close in;
}
close out;

