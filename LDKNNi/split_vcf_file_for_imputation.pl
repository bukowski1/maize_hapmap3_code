#!/usr/bin/env perl

$infile=$ARGV[0];
$chrsize=$ARGV[1];
$stripdepth=$ARGV[2]; # if 1, then remove depth info from the output VCF file

$taxaindex=9;

$nchunk = 30000000;
$overlap = 500000;

# Calculate the number of output files

$endpos = 1;
$fcounter = 0;
while($endpos < $chrsize)
{
	$beg = $endpos;
	$end = $endpos + $nchunk - 1;
	$sboundary[$fcounter] = $beg;
	$eboundary[$fcounter] = $end;
	$endpos = $endpos + $nchunk - $overlap;
	$fcounter++;
}

print "$fcounter files will be open with the following boundaries\n";
for($i=0;$i<=$#sboundary;$i++)
{
	print "$sboundary[$i]\t$eboundary[$i]\n";
}

# Open output files, save file handles in an array
# Also, initialize the files to VCF headers from the main file
my @filehandles;
for($i=0; $i<$fcounter; $i++)
{
  $fname = "${infile}_chunk${i}.vcf";
  `head -100 $infile | grep "#" > $fname`;
  local *FILE;
  open(FILE, ">>$fname") || die;
  push(@filehandles, *FILE);
}

open(in,$infile);
while($line=<in>)
{
	if($line =~ /^#/) { next; }  # handle header separately

	chomp $line;
	@aux = split /\s+/, $line;

	if($stripdepth)
	{
		for($i=$taxaindex-1;$i<=$#aux;$i++)
		{
			@aux1 = split /:/, $aux[$i];
			$aux[$i] = $aux1[0];
		}
		$line = join("\t",@aux);
	}

	# Go over all files and decide whether to write the line there\
	for($i=0;$i<$fcounter;$i++)
	{
		if($aux[1] >= $sboundary[$i] && $aux[1] <= $eboundary[$i])
		{
			print { $filehandles[$i] } "$line\n";
		}
	}
}
close in;

for($i=0; $i<$fcounter; $i++)
{
  close $filehandles[$i] ;
}
