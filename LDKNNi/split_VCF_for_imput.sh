#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

SCR=$BINDIR/LDKNNi/split_vcf_file_for_imputation.pl

for i in {1..10}
do
	CHR=$i
	VCFIN=hmp321_chr${i}.vcf
	$SCR $VCFIN 310000000 1
	mv ${VCFIN}_chunk*.vcf vcf_chunks_no_depth
done
