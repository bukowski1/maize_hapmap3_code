#!/bin/bash

BINDIR=/home/bukowski/HapMap3/maize_hapmap3_code

# Java 8 is needed - uncomment and update, if necessary
#export JAVA_HOME=/usr/local/jdk1.8.0_45
#export PATH=$JAVA_HOME/bin:$PATH

NCORES=16
CMD="java -Xmx100g -cp $BINDIR/java_code/tassel5-active/dist/tassel5-active.jar net.maizegenetics.ed.t5.Imputation "

for k in {1..10}
do
	CHR=$k
	echo Chromosome $k started
	date
	FCORE=hmp321_chr${CHR}.vcf
	LIST=`ls -1 ${FCORE}_chunk*.vcf*`
	for i in $LIST 
	do
		INFILE=$i
		OUTFILE=`echo $INFILE | awk '{gsub(/.vcf/,""); print}'`
		OUTFILE=${OUTFILE}_imputed.vcf
		echo Running  $INFILE   $OUTFILE
		date
		$CMD $INFILE $OUTFILE $NCORES >& log.$OUTFILE
		gzip -1 $OUTFILE
	done
done
